CREATE TABLE IF NOT EXISTS `arena_history` (
  `arenaType` int(2) NOT NULL DEFAULT '0',
  `winnerTeamId` int(14) NOT NULL DEFAULT '0',
  `winnerRating` int(14) NOT NULL DEFAULT '0',
  `winnerRatingChange` int(14) NOT NULL DEFAULT '0',
  `loserTeamId` int(14) NOT NULL DEFAULT '0',
  `loserRating` int(14) NOT NULL DEFAULT '0',
  `loserRatingChange` int(14) NOT NULL DEFAULT '0',
  `mapId` int(14) NOT NULL DEFAULT '0',
  `startTime` bigint(51) unsigned NOT NULL,
  `endTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
