/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Opcodes.h"
#include "Log.h"
#include "Corpse.h"
#include "Player.h"
#include "SpellAuras.h"
#include "MapManager.h"
#include "Transport.h"
#include "Battleground.h"
#include "WaypointMovementGenerator.h"
#include "InstanceSaveMgr.h"
#include "ObjectMgr.h"
#include "World.h"
#include "Vehicle.h"

// Movement anticheat defines
//#define ANTICHEAT_DEBUG
#define ANTICHEAT_EXCEPTION_INFO
// End Movement anticheat defines

void WorldSession::HandleMoveWorldportAckOpcode(WorldPacket & /*recvData*/)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "WORLD: got MSG_MOVE_WORLDPORT_ACK.");
    HandleMoveWorldportAckOpcode();
}

void WorldSession::HandleMoveWorldportAckOpcode()
{
    // ignore unexpected far teleports
    if (!GetPlayer()->IsBeingTeleportedFar())
        return;

    GetPlayer()->SetSemaphoreTeleportFar(false);

    // get the teleport destination
    WorldLocation const& loc = GetPlayer()->GetTeleportDest();

    // possible errors in the coordinate validity check
    if (!MapManager::IsValidMapCoord(loc))
    {
        LogoutPlayer(false);
        return;
    }

    // get the destination map entry, not the current one, this will fix homebind and reset greeting
    MapEntry const* mEntry = sMapStore.LookupEntry(loc.GetMapId());
    InstanceTemplate const* mInstance = sObjectMgr->GetInstanceTemplate(loc.GetMapId());

    // reset instance validity, except if going to an instance inside an instance
    if (GetPlayer()->m_InstanceValid == false && !mInstance)
        GetPlayer()->m_InstanceValid = true;

    Map* oldMap = GetPlayer()->GetMap();
    Map* newMap = sMapMgr->CreateMap(loc.GetMapId(), GetPlayer());

    if (GetPlayer()->IsInWorld())
    {
        TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "Player %s (GUID: %u) is still in world when teleported from map %s (%u) to new map %s (%u)", GetPlayer()->GetName().c_str(), GUID_LOPART(GetPlayer()->GetGUID()), oldMap->GetMapName(), oldMap->GetId(), newMap ? newMap->GetMapName() : "Unknown", loc.GetMapId());
        oldMap->RemovePlayerFromMap(GetPlayer(), false);
    }

    // relocate the player to the teleport destination
    // the CanEnter checks are done in TeleporTo but conditions may change
    // while the player is in transit, for example the map may get full
    if (!newMap || !newMap->CanEnter(GetPlayer()))
    {
        TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "Map %d (%s) could not be created for player %d (%s), porting player to homebind", loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown", GetPlayer()->GetGUIDLow(), GetPlayer()->GetName().c_str());
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }
    else
        GetPlayer()->Relocate(&loc);

    GetPlayer()->ResetMap();
    GetPlayer()->SetMap(newMap);

    GetPlayer()->SendInitialPacketsBeforeAddToMap();
    if (!GetPlayer()->GetMap()->AddPlayerToMap(GetPlayer()))
    {
        TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "WORLD: failed to teleport player %s (%d) to map %d (%s) because of unknown reason!",
            GetPlayer()->GetName().c_str(), GetPlayer()->GetGUIDLow(), loc.GetMapId(), newMap ? newMap->GetMapName() : "Unknown");
        GetPlayer()->ResetMap();
        GetPlayer()->SetMap(oldMap);
        GetPlayer()->TeleportTo(GetPlayer()->m_homebindMapId, GetPlayer()->m_homebindX, GetPlayer()->m_homebindY, GetPlayer()->m_homebindZ, GetPlayer()->GetOrientation());
        return;
    }

    // battleground state prepare (in case join to BG), at relogin/tele player not invited
    // only add to bg group and object, if the player was invited (else he entered through command)
    if (_player->InBattleground())
    {
        // cleanup setting if outdated
        if (!mEntry->IsBattlegroundOrArena())
        {
            // We're not in BG
            _player->SetBattlegroundId(0, BATTLEGROUND_TYPE_NONE);
            // reset destination bg team
            _player->SetBGTeam(0);
        }
        // join to bg case
        else if (Battleground* bg = _player->GetBattleground())
        {
            if (_player->IsInvitedForBattlegroundInstance(_player->GetBattlegroundId()))
                bg->AddPlayer(_player);
        }
    }

    GetPlayer()->SendInitialPacketsAfterAddToMap();

    // flight fast teleport case
    if (GetPlayer()->GetMotionMaster()->GetCurrentMovementGeneratorType() == FLIGHT_MOTION_TYPE)
    {
        if (!_player->InBattleground())
        {
            // short preparations to continue flight
            FlightPathMovementGenerator* flight = (FlightPathMovementGenerator*)(GetPlayer()->GetMotionMaster()->top());
            flight->Initialize(GetPlayer());
            return;
        }

        // battleground state prepare, stop flight
        GetPlayer()->GetMotionMaster()->MovementExpired();
        GetPlayer()->CleanupAfterTaxiFlight();
    }

    // resurrect character at enter into instance where his corpse exist after add to map
    Corpse* corpse = GetPlayer()->GetCorpse();
    if (corpse && corpse->GetType() != CORPSE_BONES && corpse->GetMapId() == GetPlayer()->GetMapId())
    {
        if (mEntry->IsDungeon())
        {
            GetPlayer()->ResurrectPlayer(0.5f, false);
            GetPlayer()->SpawnCorpseBones();
        }
    }

    bool allowMount = !mEntry->IsDungeon() || mEntry->IsBattlegroundOrArena();
    if (mInstance)
    {
        Difficulty diff = GetPlayer()->GetDifficulty(mEntry->IsRaid());
        if (MapDifficulty const* mapDiff = GetMapDifficultyData(mEntry->MapID, diff))
        {
            if (mapDiff->resetTime)
            {
                if (time_t timeReset = sInstanceSaveMgr->GetResetTimeFor(mEntry->MapID, diff))
                {
                    uint32 timeleft = uint32(timeReset - time(NULL));
                    GetPlayer()->SendInstanceResetWarning(mEntry->MapID, diff, timeleft);
                }
            }
        }
        allowMount = mInstance->AllowMount;
    }

    // mount allow check
    if (!allowMount)
        _player->RemoveAurasByType(SPELL_AURA_MOUNTED);

    // update zone immediately, otherwise leave channel will cause crash in mtmap
    uint32 newzone, newarea;
    GetPlayer()->GetZoneAndAreaId(newzone, newarea);
    GetPlayer()->UpdateZone(newzone, newarea);

    // honorless target
    if (GetPlayer()->pvpInfo.IsHostile)
        GetPlayer()->CastSpell(GetPlayer(), 2479, true);

    // in friendly area
    else if (GetPlayer()->IsPvP() && !GetPlayer()->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
        GetPlayer()->UpdatePvP(false, false);

    // resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    //lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMoveTeleportAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "MSG_MOVE_TELEPORT_ACK");
    uint64 guid;

    recvData.readPackGUID(guid);

    uint32 flags, time;
    recvData >> flags >> time;
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "Guid " UI64FMTD, guid);
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "Flags %u, time %u", flags, time/IN_MILLISECONDS);

    Player* plMover = _player->m_mover->ToPlayer();

    if (!plMover || !plMover->IsBeingTeleportedNear())
        return;

    if (guid != plMover->GetGUID())
        return;

    plMover->SetSemaphoreTeleportNear(false);

    uint32 old_zone = plMover->GetZoneId();

    WorldLocation const& dest = plMover->GetTeleportDest();

    plMover->UpdatePosition(dest, true);

    uint32 newzone, newarea;
    plMover->GetZoneAndAreaId(newzone, newarea);
    plMover->UpdateZone(newzone, newarea);

    // new zone
    if (old_zone != newzone)
    {
        // honorless target
        if (plMover->pvpInfo.IsHostile)
            plMover->CastSpell(plMover, 2479, true);

        // in friendly area
        else if (plMover->IsPvP() && !plMover->HasFlag(PLAYER_FLAGS, PLAYER_FLAGS_IN_PVP))
            plMover->UpdatePvP(false, false);
    }

    // resummon pet
    GetPlayer()->ResummonPetTemporaryUnSummonedIfAny();

    //lets process all delayed operations on successful teleport
    GetPlayer()->ProcessDelayedOperations();
}

void WorldSession::HandleMovementOpcodes(WorldPacket& recvData)
{
    uint16 opcode = recvData.GetOpcode();

    Unit* mover = _player->m_mover;

    ASSERT(mover != NULL);                      // there must always be a mover

    Player* plrMover = mover->ToPlayer();

    // ignore, waiting processing in WorldSession::HandleMoveWorldportAckOpcode and WorldSession::HandleMoveTeleportAck
    if (plrMover && plrMover->IsBeingTeleported())
    {
        recvData.rfinish();                     // prevent warnings spam
        return;
    }

    /* extract packet */
    uint64 guid;

    recvData.readPackGUID(guid);

    MovementInfo movementInfo;
    movementInfo.guid = guid;
    ReadMovementInfo(recvData, &movementInfo);

    recvData.rfinish();                         // prevent warnings spam

    // prevent tampered movement data
    if (guid != mover->GetGUID())
        return;

    if (!movementInfo.pos.IsPositionValid())
    {
        recvData.rfinish();                     // prevent warnings spam
        return;
    }

    if (movementInfo.pos.GetPositionZ() > 200000.0f || movementInfo.pos.GetPositionZ() < -200000.0f)
    {
        sLog->outError(LOG_FILTER_UNITS, "Player (%s) sent invalid [x: %f y: %f z: %f]. A kurva anyjat ez hackel.", GetPlayer()->GetName().c_str(), movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), movementInfo.pos.GetPositionZ());
        return;
    }

    /* handle special cases */
    if (movementInfo.flags & MOVEMENTFLAG_ONTRANSPORT)
    {
        // transports size limited
        // (also received at zeppelin leave by some reason with t_* as absolute in continent coordinates, can be safely skipped)
        if (movementInfo.transport.pos.GetPositionX() > 50 || movementInfo.transport.pos.GetPositionY() > 50 || movementInfo.transport.pos.GetPositionZ() > 50)
        {
            recvData.rfinish();                 // prevent warnings spam
            return;
        }

        if (!Trinity::IsValidMapCoord(movementInfo.pos.GetPositionX() + movementInfo.transport.pos.GetPositionX(), movementInfo.pos.GetPositionY() + movementInfo.transport.pos.GetPositionY(),
            movementInfo.pos.GetPositionZ() + movementInfo.transport.pos.GetPositionZ(), movementInfo.pos.GetOrientation() + movementInfo.transport.pos.GetOrientation()))
        {
            recvData.rfinish();                 // prevent warnings spam
            return;
        }

        // if we boarded a transport, add us to it
        if (plrMover)
        {
            if (!plrMover->GetTransport())
            {
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                {
                    plrMover->m_transport = transport;
                    transport->AddPassenger(plrMover);
                }
            }
            else if (plrMover->GetTransport()->GetGUID() != movementInfo.transport.guid)
            {
                bool foundNewTransport = false;
                plrMover->m_transport->RemovePassenger(plrMover);
                plrMover->SetMAntiDelay(1);
                if (Transport* transport = plrMover->GetMap()->GetTransport(movementInfo.transport.guid))
                {
                    foundNewTransport = true;
                    plrMover->m_transport = transport;
                    transport->AddPassenger(plrMover);
                }

                if (!foundNewTransport)
                {
                    plrMover->m_transport = NULL;
                    movementInfo.transport.Reset();
                }
            }
        }

        if (!mover->GetTransport() && !mover->GetVehicle())
        {
            GameObject* go = mover->GetMap()->GetGameObject(movementInfo.transport.guid);
            if (!go || go->GetGoType() != GAMEOBJECT_TYPE_TRANSPORT)
                movementInfo.flags &= ~MOVEMENTFLAG_ONTRANSPORT;
        }
    }
    else if (plrMover && plrMover->GetTransport())                // if we were on a transport, leave
    {
        plrMover->m_transport->RemovePassenger(plrMover);
        plrMover->m_transport = NULL;
        movementInfo.transport.Reset();
    }

    // fall damage generation (ignore in flight case that can be triggered also at lags in moment teleportation to another map).
    if (opcode == MSG_MOVE_FALL_LAND && plrMover && !plrMover->isInFlight())
    {
        plrMover->m_anti_JumpCount = 0;
        plrMover->m_anti_JumpBaseZ = 0;
        plrMover->HandleFall(movementInfo);
    }

    if (plrMover && ((movementInfo.flags & MOVEMENTFLAG_SWIMMING) != 0) != plrMover->IsInWater())
    {
        // now client not include swimming flag in case jumping under water
        plrMover->SetInWater(!plrMover->IsInWater() || plrMover->GetBaseMap()->IsUnderWater(movementInfo.pos.GetPositionX(), movementInfo.pos.GetPositionY(), movementInfo.pos.GetPositionZ()));
    }

    bool check_passed = true;

    if (plrMover && plrMover->m_anti_disable_delay <= time(NULL) && !plrMover->isInFlight())
        check_passed = MovementAnticheat(movementInfo, plrMover, mover, recvData);

    /*----------------------*/
    
    /* process position-change */
    if (check_passed)
    {
        WorldPacket data(opcode, recvData.size());
        movementInfo.time = getMSTime();
        movementInfo.guid = mover->GetGUID();
        WriteMovementInfo(&data, &movementInfo);
        mover->SendMessageToSet(&data, _player);
        mover->m_movementInfo = movementInfo;

        // this is almost never true (not sure why it is sometimes, but it is), normally use mover->IsVehicle()
        if (mover->GetVehicle())
        {
            mover->SetOrientation(movementInfo.pos.GetOrientation());
            return;
        }

        mover->UpdatePosition(movementInfo.pos);

        if (plrMover)                                           // nothing is charmed, or player charmed
        {
            plrMover->m_anti_last_opcode = movementInfo.GetMovementFlags();

            if (plrMover->m_anti_AlarmCount > 0 && !plrMover->GetTransport())
            {
                if (((opcode == MSG_MOVE_STOP_SWIM && plrMover->m_anti_AlarmCount > 5) || plrMover->m_anti_AlarmCount > 50) /*&& plMover->GetMapId() != 550*/)
                {
                    if (opcode == MSG_MOVE_STOP_SWIM)
                        sLog->outError(LOG_FILTER_UNITS, "Air Swim Hack!: Original_mover-%s Mover-%s produce %d anticheat alarms kicked original_mover",_player->GetName().c_str(),plrMover->GetName().c_str(),plrMover->m_anti_AlarmCount);
                    else
                        sLog->outError(LOG_FILTER_UNITS, "Original_mover-%s Mover-%s produce %d anticheat alarms kicked original_mover",_player->GetName().c_str(),plrMover->GetName().c_str(),plrMover->m_anti_AlarmCount);
                    KickPlayer();
                }

                if (plrMover->m_anti_AlarmCount > 1)
                    sLog->outError(LOG_FILTER_UNITS, "MA-%s produce %d anticheat alarms %u",plrMover->GetName().c_str(),plrMover->m_anti_AlarmCount);

                if (opcode != MSG_MOVE_STOP_SWIM)
                    plrMover->m_anti_AlarmCount = 0;
            }

            if (plrMover->m_lastFallTime >= movementInfo.fallTime || plrMover->m_lastFallZ <= movementInfo.pos.GetPositionZ() || opcode == MSG_MOVE_FALL_LAND)
            {
                plrMover->SetFallInformation(movementInfo.fallTime, movementInfo.pos.GetPositionZ());
                plrMover->m_anti_JustFalling = true;
            }
            else
                plrMover->m_anti_JustFalling = false;

            float underMapValueZ;   
            switch (plrMover->GetMapId())    
            {
                case 617: underMapValueZ = 3.0f;    break; // Dalaran Sewers
                case 618: underMapValueZ = 28.0f;   break; // Ring of Valor
                case 562: underMapValueZ = -10.0f;  break; // Blade Edge Arena
                case 559: underMapValueZ = -18.0f;  break; // Nagrand arena
                case 572: underMapValueZ = 28.0f;   break; // Lordearon
                case 571: underMapValueZ = -400.0f; break; // Northrend
                default:  underMapValueZ = -500.0f; break;
             }

            if (movementInfo.pos.GetPositionZ() < underMapValueZ)
            {
                if (underMapValueZ != -500)
                {
                    if (plrMover->GetMapId() == 572) // Lordaeron Arena 
                        plrMover->TeleportTo(572, 1286.14868f, 1667.32f, 41.0f, 1.6f);
             
                    if (plrMover->GetMapId() == 559) // Nagrand Arena 
                         plrMover->TeleportTo(559, 4052.79868f, 2926.32f, 16.0f, 1.6f); 
       
                    if (plrMover->GetMapId() == 562) // Blade Edge arena 
                         plrMover->TeleportTo(562, 6237.79768f, 261.142f, 2.0f, 4.0f); 
                
                    if (plrMover->GetMapId() == 617) // Dalaran Arena 
                         plrMover->TeleportTo(617, 1292.34868f, 790.40f, 8.5f, 1.6f); 
       
                }
                else if (!(plrMover->GetBattleground() && plrMover->GetBattleground()->HandlePlayerUnderMap(_player)))
                {
                    // NOTE: this is actually called many times while falling
                    // even after the player has been teleported away
                    // TODO: discard movement packets after the player is rooted
                    if (plrMover->isAlive())
                    {
                        plrMover->EnvironmentalDamage(DAMAGE_FALL_TO_VOID, GetPlayer()->GetMaxHealth());
                        // player can be alive if GM/etc
                        // change the death state to CORPSE to prevent the death timer from
                        // starting in the next player update
                        if (!plrMover->isAlive())
                            plrMover->KillPlayer();
                    }
                }
            }
        }
    }
    else if (plrMover)
    {
        ++plrMover->m_anti_AlarmCount;

        if (plrMover == mover)
        {
            WorldPacket data;
            plrMover->SetUnitMovementFlags(0);
            plrMover->SendTeleportAckPacket();
            plrMover->BuildHeartBeatMsg(&data);
            plrMover->SendMessageToSet(&data, true);
        }
        else
        {
            mover->Relocate(mover->GetPositionX(), mover->GetPositionY(), mover->GetPositionZ(), mover->GetOrientation());
            mover->StopMoving();
        }

        if (GetSecurity() > 2)
            SendNotification("AntiCheat Alarm %u", plrMover->m_anti_AlarmCount );

        if (plrMover->m_anti_AlarmCount > 50 && plrMover->GetMapId() != 550 && !plrMover->GetTransport())
        {
            sLog->outError(LOG_FILTER_UNITS, "Incorrect move: Original_mover-%s Mover-%s produce %d anticheat alarms kicked original_mover",_player->GetName().c_str(),plrMover->GetName().c_str(),plrMover->m_anti_AlarmCount);
            //KickPlayer();
        }
    }
}

void WorldSession::HandleForceSpeedChangeAck(WorldPacket &recvData)
{
    uint32 opcode = recvData.GetOpcode();
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "WORLD: Recvd %s (%u, 0x%X) opcode", LookupOpcodeName(opcode), opcode, opcode);

    /* extract packet */
    uint64 guid;
    uint32 unk1;
    float  newspeed;

    recvData.readPackGUID(guid);

    // now can skip not our packet
    if (_player->GetGUID() != guid)
    {
        recvData.rfinish();                   // prevent warnings spam
        return;
    }

    // continue parse packet

    recvData >> unk1;                                      // counter or moveEvent

    MovementInfo movementInfo;
    movementInfo.guid = guid;
    ReadMovementInfo(recvData, &movementInfo);

    recvData >> newspeed;
    /*----------------*/

    // client ACK send one packet for mounted/run case and need skip all except last from its
    // in other cases anti-cheat check can be fail in false case
    UnitMoveType move_type;
    UnitMoveType force_move_type;

    static char const* move_type_name[MAX_MOVE_TYPE] = {  "Walk", "Run", "RunBack", "Swim", "SwimBack", "TurnRate", "Flight", "FlightBack", "PitchRate" };

    switch (opcode)
    {
        case CMSG_FORCE_WALK_SPEED_CHANGE_ACK:          move_type = MOVE_WALK;          force_move_type = MOVE_WALK;        break;
        case CMSG_FORCE_RUN_SPEED_CHANGE_ACK:           move_type = MOVE_RUN;           force_move_type = MOVE_RUN;         break;
        case CMSG_FORCE_RUN_BACK_SPEED_CHANGE_ACK:      move_type = MOVE_RUN_BACK;      force_move_type = MOVE_RUN_BACK;    break;
        case CMSG_FORCE_SWIM_SPEED_CHANGE_ACK:          move_type = MOVE_SWIM;          force_move_type = MOVE_SWIM;        break;
        case CMSG_FORCE_SWIM_BACK_SPEED_CHANGE_ACK:     move_type = MOVE_SWIM_BACK;     force_move_type = MOVE_SWIM_BACK;   break;
        case CMSG_FORCE_TURN_RATE_CHANGE_ACK:           move_type = MOVE_TURN_RATE;     force_move_type = MOVE_TURN_RATE;   break;
        case CMSG_FORCE_FLIGHT_SPEED_CHANGE_ACK:        move_type = MOVE_FLIGHT;        force_move_type = MOVE_FLIGHT;      break;
        case CMSG_FORCE_FLIGHT_BACK_SPEED_CHANGE_ACK:   move_type = MOVE_FLIGHT_BACK;   force_move_type = MOVE_FLIGHT_BACK; break;
        case CMSG_FORCE_PITCH_RATE_CHANGE_ACK:          move_type = MOVE_PITCH_RATE;    force_move_type = MOVE_PITCH_RATE;  break;
        default:
            TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "WorldSession::HandleForceSpeedChangeAck: Unknown move type opcode: %u", opcode);
            return;
    }

    // skip all forced speed changes except last and unexpected
    // in run/mounted case used one ACK and it must be skipped.m_forced_speed_changes[MOVE_RUN} store both.
    if (_player->m_forced_speed_changes[force_move_type] > 0)
    {
        --_player->m_forced_speed_changes[force_move_type];
        if (_player->m_forced_speed_changes[force_move_type] > 0)
            return;
    }

    if (!_player->GetTransport() && fabs(_player->GetSpeed(move_type) - newspeed) > 0.01f)
    {
        if (_player->GetSpeed(move_type) > newspeed)         // must be greater - just correct
        {
            TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "%sSpeedChange player %s is NOT correct (must be %f instead %f), force set to correct value",
                move_type_name[move_type], _player->GetName().c_str(), _player->GetSpeed(move_type), newspeed);
            _player->SetSpeed(move_type, _player->GetSpeedRate(move_type), true);
        }
        else                                                // must be lesser - cheating
        {
            TC_LOG_DEBUG(LOG_FILTER_GENERAL, "Player %s from account id %u kicked for incorrect speed (must be %f instead %f)",
                _player->GetName().c_str(), _player->GetSession()->GetAccountId(), _player->GetSpeed(move_type), newspeed);
            _player->GetSession()->KickPlayer();
        }
    }
}

void WorldSession::HandleSetActiveMoverOpcode(WorldPacket &recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "WORLD: Recvd CMSG_SET_ACTIVE_MOVER");

    uint64 guid;
    recvData >> guid;

    if (GetPlayer()->IsInWorld())
    {
        if (_player->m_mover->GetGUID() != guid)
            TC_LOG_ERROR(LOG_FILTER_NETWORKIO, "HandleSetActiveMoverOpcode: incorrect mover guid: mover is " UI64FMTD " (%s - Entry: %u) and should be " UI64FMTD, guid, GetLogNameForGuid(guid), GUID_ENPART(guid), _player->m_mover->GetGUID());
    }
}

void WorldSession::HandleMoveNotActiveMover(WorldPacket &recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "WORLD: Recvd CMSG_MOVE_NOT_ACTIVE_MOVER");

    uint64 old_mover_guid;
    recvData.readPackGUID(old_mover_guid);

    MovementInfo mi;
    ReadMovementInfo(recvData, &mi);

    mi.guid = old_mover_guid;

    _player->m_movementInfo = mi;
}

void WorldSession::HandleMountSpecialAnimOpcode(WorldPacket& /*recvData*/)
{
    WorldPacket data(SMSG_MOUNTSPECIAL_ANIM, 8);
    data << uint64(GetPlayer()->GetGUID());

    GetPlayer()->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveKnockBackAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "CMSG_MOVE_KNOCK_BACK_ACK");

    uint64 guid;
    recvData.readPackGUID(guid);

    if (_player->m_mover->GetGUID() != guid)
        return;

    recvData.read_skip<uint32>();                          // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);

    _player->m_movementInfo = movementInfo;

    WorldPacket data(MSG_MOVE_KNOCK_BACK, 66);
    data.appendPackGUID(guid);
    _player->BuildMovementPacket(&data);

    // knockback specific info
    data << movementInfo.jump.sinAngle;
    data << movementInfo.jump.cosAngle;
    data << movementInfo.jump.xyspeed;
    data << movementInfo.jump.zspeed;

    _player->SendMessageToSet(&data, false);
}

void WorldSession::HandleMoveHoverAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "CMSG_MOVE_HOVER_ACK");

    uint64 guid;                                            // guid - unused
    recvData.readPackGUID(guid);

    recvData.read_skip<uint32>();                          // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);

    // Save movement flags
    _player->SetUnitMovementFlags(movementInfo.flags);
    _player->m_movementInfo = movementInfo;

    recvData.read_skip<uint32>();                          // unk2
}

void WorldSession::HandleMoveWaterWalkAck(WorldPacket& recvData)
{
    TC_LOG_DEBUG(LOG_FILTER_NETWORKIO, "CMSG_MOVE_WATER_WALK_ACK");

    uint64 guid;                                            // guid - unused
    recvData.readPackGUID(guid);

    recvData.read_skip<uint32>();                          // unk

    MovementInfo movementInfo;
    ReadMovementInfo(recvData, &movementInfo);

    recvData.read_skip<uint32>();                          // unk2
}

void WorldSession::HandleSummonResponseOpcode(WorldPacket& recvData)
{
    if (!_player->isAlive() || _player->isInCombat())
        return;

    uint64 summoner_guid;
    bool agree;
    recvData >> summoner_guid;
    recvData >> agree;

    _player->SummonIfPossible(agree);
}

bool WorldSession::MovementAnticheat(MovementInfo movementInfo, Player *plMover, Unit *mover, WorldPacket& recvData)
{
    bool IsValidFlight;
    bool check_passed = true;
    int32 cClientTimeDelta;
    int32 cClientTimeDelta_speed;
    int32 gmd;
    int32 sync_time;
    uint32 cServerTime;
    uint32 cServerTimeDelta;
    uint32 curDest;
    uint32 Tolerate_delta;
    float allowed_delta;
    float failure_rate;

    float delta_x;
    float delta_y;
    float delta_z;
    float real_delta;
    float tg_z;
    float current_speed;
    float time_delta;
    float mountian_hack_time_delta;
    float Last_Vspeed;
    float calc_vlast;

    if (!sWorld->getBoolConfig(CONFIG_NEW_AC))
        return true;

    uint32 opcode = recvData.GetOpcode();

    cClientTimeDelta = 1500;
    cClientTimeDelta_speed = 1500;
    if (plMover->m_anti_LastClientTime !=0)
    {
        cClientTimeDelta = movementInfo.time - plMover->m_anti_LastClientTime;
        if (cClientTimeDelta > 0) cClientTimeDelta_speed = cClientTimeDelta ;
        else cClientTimeDelta_speed = cClientTimeDelta * (-1);
        plMover->m_anti_DeltaClientTime += cClientTimeDelta;
        plMover->m_anti_LastClientTime = movementInfo.time;
    }
    else
        plMover->m_anti_LastClientTime = movementInfo.time;

    cServerTime = getMSTime();
    cServerTimeDelta = 1500;
    if (plMover->m_anti_LastServerTime != 0)
    {
        cServerTimeDelta = getMSTimeDiff(plMover->m_anti_LastServerTime, cServerTime);
        plMover->m_anti_DeltaServerTime += cServerTimeDelta;
        plMover->m_anti_LastServerTime = cServerTime;
    }
    else
        plMover->m_anti_LastServerTime = cServerTime;

    gmd = World::GetMistimingDelta();

    if (plMover->m_anti_DeltaServerTime < 15000 && plMover->m_anti_DeltaClientTime < 15000)
        plMover->m_anti_DeltaClientTime = plMover->m_anti_DeltaServerTime;

    sync_time = plMover->m_anti_DeltaClientTime - plMover->m_anti_DeltaServerTime;

    curDest = plMover->m_taxi.GetTaxiDestination();
    if (!plMover->GetTransport() && !curDest)
    {
        UnitMoveType move_type;

        if (movementInfo.HasMovementFlag(MOVEMENTFLAG_CAN_FLY))
            move_type = /*movementInfo.HasMovementFlag(MOVEMENTFLAG_BACKWARD) ? MOVE_FLIGHT_BACK : */MOVE_FLIGHT;
        else if (movementInfo.HasMovementFlag(MOVEMENTFLAG_SWIMMING))
            move_type = /*movementInfo.HasMovementFlag(MOVEMENTFLAG_BACKWARD) ? MOVE_SWIM_BACK : */MOVE_SWIM;
        else if (movementInfo.HasMovementFlag(MOVEMENTFLAG_WALKING))
            move_type = MOVE_WALK;
        else
            move_type = /*movementInfo.HasMovementFlag(MOVEMENTFLAG_BACKWARD) ? MOVE_SWIM_BACK : */MOVE_RUN;

        current_speed = plMover->GetSpeed(move_type) * 1.1f;

        if (movementInfo.HasMovementFlag(MovementFlags(MOVEMENTFLAG_PENDING_STOP | MOVEMENTFLAG_FALLING_FAR | MOVEMENTFLAG_FALLING)) && current_speed < 9.81f)
            current_speed = 9.81f;

        allowed_delta = 0.0f;
        failure_rate = 0.0f;

        /*if (plMover == mover)
            IsValidFlight = plMover->HasMtState(VALID_FLIGHT_STATE);
        else*/
        IsValidFlight = mover->HasAuraType(SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED) || mover->HasAuraType(SPELL_AURA_FLY);

        if (plMover->m_lastFallTime >= movementInfo.fallTime || plMover->m_lastFallZ <= movementInfo.pos.GetPositionZ() || opcode == MSG_MOVE_FALL_LAND || IsValidFlight)
        {
        }
        else
            plMover->m_anti_JustFalling = true;

        delta_x = mover->GetPositionX() - movementInfo.pos.GetPositionX();
        delta_y = mover->GetPositionY() - movementInfo.pos.GetPositionY();
        delta_z = 0.f;
        if (!movementInfo.HasMovementFlag(MOVEMENTFLAG_FALLING))
            delta_z = mover->GetPositionZ() - movementInfo.pos.GetPositionZ();
        real_delta = delta_x * delta_x + delta_y * delta_y;
        tg_z = 0;

        if (cClientTimeDelta < 0)
            cClientTimeDelta = 0;

        Tolerate_delta = 0;

        if (real_delta >= 1.0f)
            tg_z = ((delta_z * delta_z) / real_delta);
        else
            tg_z = (delta_z * delta_z);

        if (tg_z > 6.0f)
            current_speed = plMover->GetSpeed(move_type) + 0.5f;

        if (current_speed < plMover->m_anti_Last_HSpeed)
        {
            Tolerate_delta = (current_speed > 6) ? ((plMover->m_anti_Last_HSpeed < 11) ? 10 : 40) : 0;
            allowed_delta = plMover->m_anti_Last_HSpeed;
            cClientTimeDelta += Tolerate_delta;
            if (plMover->m_anti_LastSpeedChangeTime == 0)
                plMover->m_anti_LastSpeedChangeTime = movementInfo.time + (uint32)floor(((plMover->m_anti_Last_HSpeed / current_speed) * 1500)) + 100;
        }
        else
            allowed_delta = current_speed;

        time_delta = (cClientTimeDelta < 1500 || opcode == MSG_MOVE_HEARTBEAT) ? (float)cClientTimeDelta*0.001f : 0.5f;
        allowed_delta = allowed_delta * time_delta;
        allowed_delta = allowed_delta * allowed_delta + 2;

        mountian_hack_time_delta = time_delta;
        if (mountian_hack_time_delta > 0.5f) mountian_hack_time_delta = 0.5f;
        Last_Vspeed = 0.0f;
        calc_vlast = 0.0f;
        if (opcode == MSG_MOVE_STOP)
            Last_Vspeed = ((real_delta / tg_z) / (GetPlayer()->m_anti_Last_VSpeed)) - 0.7f;
        else
            Last_Vspeed = ((real_delta / tg_z) / (GetPlayer()->m_anti_Last_VSpeed)) - 1.06f;

        if (!plMover->isGameMaster() &&
            !IsValidFlight &&
            !plMover->IsInWater()
            )
        {
            if ((tg_z >= 13.5f) || (real_delta == 0.0f && tg_z >= 5))
            {
              real_delta += tg_z;
              if ((opcode == MSG_MOVE_HEARTBEAT || opcode == MSG_MOVE_FALL_LAND) && delta_z > 7.0f)
                  allowed_delta += (delta_z*delta_z);
            }
            else
                allowed_delta += (delta_z*delta_z)/1.8f;

            if ((delta_z < Last_Vspeed) && (plMover->m_anti_JumpCount == 0) && (tg_z > 0.0f))
            {
                if (!IsValidFlight && (plMover->m_anti_last_opcode & (MOVEMENTFLAG_PENDING_STOP | MOVEMENTFLAG_FALLING_FAR | MOVEMENTFLAG_FALLING)) == 0) //Example allow fly in tempest keep and magister kealthas fight
                {
                    if (plMover->m_anti_AlarmCount > 0)
                    {
                        sLog->outError(LOG_FILTER_UNITS, "mr_z %f TangesZ calc %f real_delta %f allowed_delta %f m_anti_Last_VSpeed %f Last_Vspeed %f [%f %f %f] %d %s %s",plMover->GetPositionZ(),tg_z, real_delta,allowed_delta,plMover->m_anti_Last_VSpeed,Last_Vspeed,delta_x,delta_y,delta_z,plMover->GetMapId(),plMover->GetName().c_str(),LookupOpcodeName(opcode));
                        check_passed = false;
                    }
                }
            }
        }
        else
            allowed_delta += (delta_z*delta_z)/1.56f;

        if (real_delta > allowed_delta)
            failure_rate = (allowed_delta / real_delta) * 100;

        //if (opcode == CMSG_MOVE_CHNG_TRANSPORT)
        //    allowed_delta *= 10.f;

        if (plMover == mover && check_passed && (real_delta > allowed_delta)
            && failure_rate <= 98.0f
            && plMover->m_anti_active != 2
            && !plMover->m_anti_JustFalling
            && !((plMover->m_anti_JustFalling || (plMover->m_anti_last_opcode & (MOVEMENTFLAG_PENDING_STOP | MOVEMENTFLAG_FALLING_FAR | MOVEMENTFLAG_FALLING)) != 0) && movementInfo.time != 357 && (delta_z < -6.5f || opcode == MSG_MOVE_SET_FACING || delta_z > 4.5f))) // && (delta_z < 0))
        {
            if (plMover->m_anti_AlarmCount == 0)
                sLog->outError(LOG_FILTER_UNITS, "MA-%s, speed exception | cDelta=%f aDelta=%f | cSpeed=%f lSpeed=%f cheatspeed=OFF | deltaTime=%f,World::m_Latency*2=%d,Tolerate_delta=%d, tg_z =%f | DeltaZ %f",
                                plMover->GetName().c_str(), real_delta, allowed_delta, current_speed, plMover->m_anti_Last_HSpeed/*,cheatspeed*/,time_delta,0,Tolerate_delta,tg_z,delta_z);

            if (plMover->GetVehicle())
                check_passed = true;
            else
                check_passed = false;
        }
        else if (check_passed && real_delta > 4900.0f) //teleport hack checks
        {
            if (plMover->m_anti_AlarmCount == 0)
                sLog->outError(LOG_FILTER_UNITS, "MA-%s, is teleport exception | cDelta=%f aDelta=%f | cSpeed=%f lSpeed=%f deltaToime=%f",
                            plMover->GetName().c_str(),real_delta, allowed_delta, current_speed, plMover->m_anti_Last_HSpeed,time_delta);

            check_passed = false;
        }

        //multi jump checks
        if (check_passed && opcode == MSG_MOVE_JUMP && !plMover->IsInWater())
        {
            if (plMover->m_anti_JumpCount >= 1)
            {
                sLog->outError(LOG_FILTER_UNITS, "MA-%s, multijump exception. ", plMover->GetName().c_str());
                check_passed = false;
            }
            else
            {
                plMover->m_anti_JumpCount += 1;
                plMover->m_anti_JumpBaseZ = movementInfo.pos.GetPositionZ();
            }
        }
        else if (plMover->IsInWater())
             plMover->m_anti_JumpCount = 0;

        /*if (movementInfo.HasMovementFlag(MOVEMENTFLAG_DISABLE_GRAVITY) && !plMover->GetVehicle() && !plMover->GetCharm() && m_lastFlyHackWarningTime <= time(NULL))
        {
            sLog.outCheat("[AntiCheat]: [1] Player(%s - %s) MoveFlag: %u MoveFlag2: %u MapId: %d SelectedTarget: %s", plMover->GetName(), plMover->GetObjectGuid().GetString().c_str(), movementInfo->GetMovementFlags(), movementInfo->GetMovementFlags2(), plMover->GetMapId(), plMover->GetSelectionGuid().GetString().c_str());
            std::ostringstream auras;
            Unit::SpellAuraHolderMap &plMoverAuras = plMover->GetSpellAuraHolderMap();
            for (Unit::SpellAuraHolderMap::iterator itr = plMoverAuras.begin(); itr != plMoverAuras.end(); ++itr)
            {
                if (!itr->second->IsPassive())
                    auras << itr->second->GetId() << " ";
            }

            sLog.outCheat("[AntiCheat]: [2] Auras: %s", auras.str().c_str());
            check_passed = false;
            WorldPacket data(SMSG_SPLINE_MOVE_UNSET_LEVITATING, 8);
            data << mover->GetPackGUID();
            GetPlayer()->SendDirectMessage(&data);
            m_lastFlyHackWarningTime = time(NULL) + 1;
            HackType = 2;
        }*/

        //Fly hack checks
        if (check_passed && (movementInfo.HasMovementFlag(MovementFlags(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING)))
              && !plMover->isGameMaster() && !IsValidFlight && plMover->m_lastFlyHackWarningTime <= time(NULL))
        {
            if (plMover->m_anti_AlarmCount > 0)
            {
                sLog->outError(LOG_FILTER_UNITS, "MA-%s, flight exception. {SPELL_AURA_FLY=[%X]} {SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED=[%X]} {SPELL_AURA_MOD_SPEED_FLIGHT=[%X]} {SPELL_AURA_MOD_FLIGHT_SPEED_ALWAYS=[%X]} {SPELL_AURA_MOD_FLIGHT_SPEED_NOT_STACK=[%X]} MAPID %d",
                   plMover->GetName().c_str(),
                   plMover->HasAuraType(SPELL_AURA_FLY), plMover->HasAuraType(SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED),
                   plMover->HasAuraType(SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED), plMover->HasAuraType(SPELL_AURA_MOD_MOUNTED_FLIGHT_SPEED_ALWAYS),
                   plMover->HasAuraType(SPELL_AURA_MOD_FLIGHT_SPEED_NOT_STACK),
                   plMover->GetMapId()
                   );

                if (movementInfo.HasMovementFlag(MOVEMENTFLAG_SWIMMING))
                    sLog->outError(LOG_FILTER_UNITS, "MA-%s, flight exception. MOVEFLAG_SWIMMING flight cheat check bug! ",plMover->GetName().c_str());

                check_passed = false;
            }
            
            plMover->SendMovementSetCanFly(true);
            plMover->SendMovementSetCanFly(false);
            plMover->m_lastFlyHackWarningTime = time(NULL) + 1;
        }

        // WaterWalking check
        /*if (//!plMover->IsInWater() && !IsWaterWalkValid &&
           movementInfo.HasMovementFlag(MOVEMENTFLAG_WATERWALKING)
              && !(plMover->HasAuraType(SPELL_AURA_WATER_WALK) || plMover->HasAuraType(SPELL_AURA_GHOST)))
        {
            if (plMover->m_anti_AlarmCount == 0)
            sLog->outError(LOG_FILTER_UNITS, "MA-%s, water-walk exception. [%X]{SPELL_AURA_WATER_WALK=[%X]}{plMover->IsInWater[%d],{WATERWALK FLAG[%d]}}",
                plMover->GetName().c_str(), movementInfo.GetMovementFlags(), plMover->HasAuraType(SPELL_AURA_WATER_WALK), plMover->IsInWater()
                ,movementInfo.HasMovementFlag(MOVEMENTFLAG_WATERWALKING));

            plMover->SendMovementSetWaterWalking(false);
            check_passed = false;
        }*/

        if (movementInfo.time > plMover->m_anti_LastSpeedChangeTime)
        {
            plMover->m_anti_Last_HSpeed = current_speed;
            plMover->m_anti_Last_VSpeed = -2.3f;
            if (plMover->m_anti_LastSpeedChangeTime != 0)
                plMover->m_anti_LastSpeedChangeTime = 0;
        }

        //AntiGravitation
        float JumpHeight = plMover->m_anti_JumpBaseZ - movementInfo.pos.GetPositionZ();
        if (!IsValidFlight && (plMover->m_anti_JumpCount && plMover->m_anti_JumpBaseZ != 0.f)
            && !(movementInfo.HasMovementFlag(MovementFlags(MOVEMENTFLAG_SWIMMING | MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING | MOVEMENTFLAG_SWIMMING | MOVEMENTFLAG_FALLING)))
                && (JumpHeight < plMover->m_anti_Last_VSpeed))
        {
            sLog->outError(LOG_FILTER_UNITS, "MA-%s, GraviJump exception. JumpHeight = %f, Allowed Veritcal Speed = %f",
                            plMover->GetName().c_str(), JumpHeight, plMover->m_anti_Last_VSpeed);

            check_passed = false;
        }

        if (plMover->m_anti_active == 2 && plMover->m_anti_disable_delay <= time(NULL))
            plMover->m_anti_active = 0;
    }
    /*else if (IsInTransport)
    {
        //antiwrap checks
        if (plMover->GetTransport())
        {
            float trans_rad = sqrt(movementInfo.transport.pos.GetPositionX() * movementInfo.transport.pos.GetPositionX() + movementInfo.transport.pos.GetPositionY() * movementInfo.transport.pos.GetPositionY() + movementInfo.transport.pos.GetPositionZ() * movementInfo.transport.pos.GetPositionZ());
            if (plMover->GetTransportHandle() && !plMover->GetTransport()->IsInTransport(plMover) && trans_rad > 100.0f)
            {
                check_passed = false;
                //#ifdef MOVEMENT_ANTICHEAT_DEBUG
                sLog->outError(LOG_FILTER_UNITS, "MA-%s, leave transport.", plMover->GetName().c_str());
                //#endif
            }
        }
        else if (plMover->GetMap())
        {
            if (plMover->GetTransport())
            {
                if (GameObject* transport = plMover->GetMap()->GetGameObject(plMover->m_anti_TransportGUID))
                {
                    if (transport->GetGoType() == GAMEOBJECT_TYPE_TRAPDOOR) // Trapdoor not have transportkit
                        check_passed = true;
                    else
                    {
                        //assert(transport->GetTransportKit()); //milyen transport
                        float delta_gox = transport->GetTransportKit()->GetPositionX() - movementInfo->GetPos()->x;
                        float delta_goy = transport->GetTransportKit()->GetPositionY() - movementInfo->GetPos()->y;
                        //float delta_goz = go_data->posZ - movementInfo->GetPos()->z; //unused
                        int mapid = transport->GetMapId();
                        #ifdef MOVEMENT_ANTICHEAT_DEBUG
                        DEBUG_FILTER_LOG(LOG_FILTER_DUMMY, "MA-%s, transport movement. GO xyzo: %f,%f,%f",
                                        plMover->GetName(), go_data->posX,go_data->posY,go_data->posZ);
                        #endif
                        if (int32(plMover->GetMapId()) != mapid)
                        {
                            HackType = 21;
                            check_passed = false;
                        }
                        else
                        {
                            float delta_go = delta_gox*delta_gox + delta_goy*delta_goy;
                            if (delta_go > 3600.0f)
                            {
                                HackType = 22;
                                check_passed = false;
                                sLog.outError("MA-%s, leave transport. " UI64FMTD " GO xyzo: %f,%f",
                                                plMover->GetName(), plMover->m_anti_TransportGUID.GetRawValue(), transport->GetTransportKit()->GetPositionX(), transport->GetTransportKit()->GetPositionY());
                            }
                        }
                    }

                }
                else
                {
                    sLog.outError("MA-%s, leave transport. " UI64FMTD,
                                    plMover->GetName(), plMover->m_anti_TransportGUID.GetRawValue());
                    #ifdef MOVEMENT_ANTICHEAT_DEBUG
                    DEBUG_FILTER_LOG(LOG_FILTER_DUMMY, "MA-%s, undefined transport.", plMover->GetName());
                    #endif
                    HackType = 24;
                    check_passed = false;
                }
            } 
        }

        if (!check_passed)
        {
            if (plMover->GetTransportBase() && plMover->GetTransportBase()->ToTransport())
                plMover->GetTransportBase()->ToTransport()->RemovePassenger(plMover);
            movementInfo->ClearTransportData();
            plMover->m_anti_TransportGUID.Set(0);
        }
    }*/

    return check_passed;
}