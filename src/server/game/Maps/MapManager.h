/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TRINITY_MAPMANAGER_H
#define TRINITY_MAPMANAGER_H

#include "Object.h"
#include "Map.h"
#include "GridStates.h"
#include "MapUpdater.h"
#include "Log.h"
#include "WorldPacket.h"
#include "UpdateData.h"
#include "Appender.h"
#include "Object.h"

#include <ace/Singleton.h>
#include <ace/Thread_Mutex.h>

class Transport;
class WorldPakcet;
class WorldSocket;
struct TransportCreatureProto;

class MapManager
{
    friend class ACE_Singleton<MapManager, ACE_Thread_Mutex>;
    public:
        Map* CreateBaseMap(uint32 mapId);
        Map* FindBaseNonInstanceMap(uint32 mapId) const;
        Map* CreateMap(uint32 mapId, Player* player);
        Map* FindMap(uint32 mapId, uint32 instanceId) const;

        uint16 GetAreaFlag(uint32 mapid, float x, float y, float z) const
        {
            Map const* m = const_cast<MapManager*>(this)->CreateBaseMap(mapid);
            return m->GetAreaFlag(x, y, z);
        }
        uint32 GetAreaId(uint32 mapid, float x, float y, float z) const
        {
            return Map::GetAreaIdByAreaFlag(GetAreaFlag(mapid, x, y, z), mapid);
        }
        uint32 GetZoneId(uint32 mapid, float x, float y, float z) const
        {
            return Map::GetZoneIdByAreaFlag(GetAreaFlag(mapid, x, y, z), mapid);
        }
        void GetZoneAndAreaId(uint32& zoneid, uint32& areaid, uint32 mapid, float x, float y, float z)
        {
            Map::GetZoneAndAreaIdByAreaFlag(zoneid, areaid, GetAreaFlag(mapid, x, y, z), mapid);
        }

        void Initialize(void);
        void Update(uint32);

        void SetGridCleanUpDelay(uint32 t)
        {
            if (t < MIN_GRID_DELAY)
                i_gridCleanUpDelay = MIN_GRID_DELAY;
            else
                i_gridCleanUpDelay = t;
        }

        void SetMapUpdateInterval(uint32 t)
        {
            if (t < MIN_MAP_UPDATE_DELAY)
                t = MIN_MAP_UPDATE_DELAY;

            i_timer.SetInterval(t);
            i_timer.Reset();
        }

        //void LoadGrid(int mapid, int instId, float x, float y, const WorldObject* obj, bool no_unload = false);
        void UnloadAll();

        static bool ExistMapAndVMap(uint32 mapid, float x, float y);
        static bool IsValidMAP(uint32 mapid, bool startUp);

        static bool IsValidMapCoord(uint32 mapid, float x, float y)
        {
            return IsValidMAP(mapid, false) && Trinity::IsValidMapCoord(x, y);
        }

        static bool IsValidMapCoord(uint32 mapid, float x, float y, float z)
        {
            return IsValidMAP(mapid, false) && Trinity::IsValidMapCoord(x, y, z);
        }

        static bool IsValidMapCoord(uint32 mapid, float x, float y, float z, float o)
        {
            return IsValidMAP(mapid, false) && Trinity::IsValidMapCoord(x, y, z, o);
        }

        static bool IsValidMapCoord(WorldLocation const& loc)
        {
            return IsValidMapCoord(loc.GetMapId(), loc.GetPositionX(), loc.GetPositionY(), loc.GetPositionZ(), loc.GetOrientation());
        }

        void DoDelayedMovesAndRemoves();

        bool CanPlayerEnter(uint32 mapid, Player* player, bool loginCheck = false);
        void InitializeVisibilityDistanceInfo();

        /* statistics */
        uint32 GetNumInstances();
        uint32 GetNumPlayersInInstances();

        // Instance ID management
        void InitInstanceIds();
        uint32 GenerateInstanceId();
        void RegisterInstanceId(uint32 instanceId);
        void FreeInstanceId(uint32 instanceId);

        uint32 GetNextInstanceId() const { return _nextInstanceId; };
        void SetNextInstanceId(uint32 nextInstanceId) { _nextInstanceId = nextInstanceId; };

        MapUpdater * GetMapUpdater() { return &m_updater; }

        typedef UNORDERED_MAP<uint32, IntermediatePacketCompiler*> IntermediatePacketCompilerStore;
        IntermediatePacketCompilerStore _compilerThreads;

        std::string GetCompilerStatistic();

        void StartCompilerThreads();

    private:
        typedef UNORDERED_MAP<uint32, Map*> MapMapType;
        typedef std::vector<bool> InstanceIds;

        // debugging code, should be deleted some day
        void checkAndCorrectGridStatesArray();              // just for debugging to find some memory overwrites
        GridState* i_GridStates[MAX_GRID_STATE];            // shadow entries to the global array in Map.cpp
        int i_GridStateErrorCount;

        MapManager();
        ~MapManager();

        Map* FindBaseMap(uint32 mapId) const
        {
            MapMapType::const_iterator iter = i_maps.find(mapId);
            return (iter == i_maps.end() ? NULL : iter->second);
        }

        MapManager(const MapManager &);
        MapManager& operator=(const MapManager &);

        ACE_Thread_Mutex Lock;
        uint32 i_gridCleanUpDelay;
        MapMapType i_maps;
        IntervalTimer i_timer;

        InstanceIds _instanceIds;
        uint32 _nextInstanceId;
        MapUpdater m_updater;
};

class IntermediatePacketCompiler : public ACE_Based::Runnable
{
    public:
        void stop()
        {
            m_incomingQueue.cancel();
            SetRunning(false);
        }

        void run();
        void SendPacket(WorldSocket* socket, WorldPacket* packet);
        void SendPacket(WorldSocket* socket, UpdateData* packet);
        void SendMultipleCombatLog(WorldSocket* socket, std::deque<WorldPacket*> data);

        struct BaseElement
        {
            BaseElement(WorldSocket* socket, WorldPacket* packet) : m_socket(socket), m_packet(packet) {}
            BaseElement(WorldSocket* socket) : m_socket(socket) {}
            virtual ~BaseElement() {}

            virtual bool Execute();
            WorldSocket* m_socket;
            WorldPacket* m_packet;
        };

        uint32 mapId;
        bool _running;
        void SetRunning(bool x) 
        {
            _running = x;
        }
        bool IsRunning() { return _running; }

        time_t m_lastupdateTime;
        uint32 m_latency;
        inline void ClearLatency() { m_lastupdateTime = 0; }
        inline void ResetLatency() { m_lastupdateTime = time(NULL); }
        inline time_t GetLatencyTimer() { return m_lastupdateTime; }

        ACE_Based::Thread selfThread;

    private:
        struct UpdateElement : public BaseElement
        {
            UpdateElement(WorldSocket* socket, UpdateData* data) : BaseElement(socket), m_updateData(data) {}
            virtual ~UpdateElement() {}

            virtual bool Execute();
            UpdateData* m_updateData;
        };

        struct MultipleCombatLogElement : public BaseElement
        {
            MultipleCombatLogElement(WorldSocket* socket, std::deque<WorldPacket*> data) : BaseElement(socket), m_multipleData(data) {}
            virtual ~MultipleCombatLogElement() {}

            virtual bool Execute();
            std::deque<WorldPacket*> m_multipleData;
        };

        struct CreateBlockHelperElement : public BaseElement
        {
            CreateBlockHelperElement() : BaseElement(NULL) {}
            virtual ~CreateBlockHelperElement() {}

            virtual bool Execute();
        };

        ACE_Based::ActivationQueue<BaseElement*> m_incomingQueue;
};


#define sMapMgr ACE_Singleton<MapManager, ACE_Thread_Mutex>::instance()
#endif
