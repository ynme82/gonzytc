/*
 * Copyright (C) 2010-2013 GonzyWoW <http://www.gonzywow.net/>
 *               Author: blaymist.
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "GuildHouseMgr.h"
#include "Player.h"
#include "Chat.h"

GuildHouseMgr::GuildHouseMgr()
{
}

GuildHouseMgr::~GuildHouseMgr()
{
}

void GuildHouseMgr::AddObject(Player* player, uint32 enty, bool npc)
{
}

void GuildHouseMgr::RemoveObject(Player* player, uint64 guid, bool npc)
{
}

void GuildHouseMgr::ClearGuildHouse(uint32 guild)
{
}

bool GuildHouseMgr::IsInGuildHouseArea(Player* player)
{
    if (player)
    {
        Creature* dummy = player->FindNearestCreature(NPC_GHOUSE_DUMMY, 100.0f);
        if (dummy && dummy->IsInWorld())
        {
            QueryResult result = WorldDatabase.PQuery("SELECT `x`, `y`, `z` FROM `guildhouses` WHERE `guildId` = '%u'", player->GetGuildId());
            if (result)
            {
                Field* field = result->Fetch();
                float x = field[0].GetDouble();
                float y = field[1].GetDouble();
                float z = field[2].GetDouble();
                if (player->IsInDist(x, y, z, 100.0f))
                    return true;
            }
        }
    }
    return false;
}

void GuildHouseMgr::KickPlayerFromGuildHouse(Player* kicker, Player* kicked)
{
    if (kicker && kicked)
    {
        if (IsInGuildHouseArea(kicker))
        {
            if ((kicker->GetGuildId() != kicked->GetGuildId()) && (kicker->GetGuildId() != 0 && kicked->GetGuildId() != 0))
            {

            }
            else
                ChatHandler(kicker->GetSession()).PSendSysMessage("Nem rughatsz ki guild tarsakat!");
        }
        else
            ChatHandler(kicker->GetSession()).PSendSysMessage("Nem tartozkodsz a Guild Hazad teruleten!");
    }
}

void GuildHouseMgr::TeleportToGuildHouse(Player* player)
{
}
