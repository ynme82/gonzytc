/*
 * Copyright (C) 2010-2013 GonzyWoW <http://www.gonzywow.net/>
 *               Author: blaymist.
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef _GUILD_HOUSE_MGR_H
#define _GUILD_HOUSE_MGR_H

#include <ace/Singleton.h>
#include "Common.h"

enum GHouseEntry
{
    NPC_GHOUSE_DUMMY = 0,
};

class Player;

class GuildHouseMgr
{
    friend class ACE_Singleton<GuildHouseMgr, ACE_Null_Mutex>;

    private:
        GuildHouseMgr();
        ~GuildHouseMgr();

    public:
        void AddObject(Player* player, uint32 entry, bool npc);
        void RemoveObject(Player* player, uint64 guid, bool npc);
        void ClearGuildHouse(uint32 guild);

        bool IsInGuildHouseArea(Player* player);
        void KickPlayerFromGuildHouse(Player* kicker, Player* kicked);
        void TeleportToGuildHouse(Player* player);
};

#define sGuildHouseMgr ACE_Singleton<GuildHouseMgr, ACE_Null_Mutex>::instance()

#endif