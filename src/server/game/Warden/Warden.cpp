/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2011 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Common.h"
#include "WorldPacket.h"
#include "WorldSession.h"
#include "Log.h"
#include "Opcodes.h"
#include "ByteBuffer.h"
#include <openssl/md5.h>
#include <openssl/sha.h>
#include "World.h"
#include "Player.h"
#include "Util.h"
#include "Warden.h"
#include "AccountMgr.h"

Warden::Warden() : _inputCrypto(16), _outputCrypto(16), _checkTimer(10000/*10 sec*/), _clientResponseTimer(0), _dataSent(false), _initialized(false)
{
}

Warden::~Warden()
{
    delete[] _module->CompressedData;
    delete _module;
    _module = NULL;
    _initialized = false;
}

void Warden::SendModuleToClient()
{
    TC_LOG_DEBUG(LOG_FILTER_WARDEN, "Send module to client");

    // Create packet structure
    WardenModuleTransfer packet;

    uint32 sizeLeft = _module->CompressedSize;
    uint32 pos = 0;
    uint16 burstSize;
    while (sizeLeft > 0)
    {
        burstSize = sizeLeft < 500 ? sizeLeft : 500;
        packet.Command = WARDEN_SMSG_MODULE_CACHE;
        packet.DataSize = burstSize;
        memcpy(packet.Data, &_module->CompressedData[pos], burstSize);
        sizeLeft -= burstSize;
        pos += burstSize;

        EncryptData((uint8*)&packet, burstSize + 3);
        WorldPacket pkt1(SMSG_WARDEN_DATA, burstSize + 3);
        pkt1.append((uint8*)&packet, burstSize + 3);
        _session->SendPacket(&pkt1);
    }
}

void Warden::RequestModule()
{
    TC_LOG_DEBUG(LOG_FILTER_WARDEN, "Request module");

    // Create packet structure
    WardenModuleUse request;
    request.Command = WARDEN_SMSG_MODULE_USE;

    memcpy(request.ModuleId, _module->Id, 16);
    memcpy(request.ModuleKey, _module->Key, 16);
    request.Size = _module->CompressedSize;

    // Encrypt with warden RC4 key.
    EncryptData((uint8*)&request, sizeof(WardenModuleUse));

    WorldPacket pkt(SMSG_WARDEN_DATA, sizeof(WardenModuleUse));
    pkt.append((uint8*)&request, sizeof(WardenModuleUse));
    _session->SendPacket(&pkt);
}

void Warden::Update()
{
    if (_initialized)
    {
        uint32 currentTimestamp = getMSTime();
        uint32 diff = currentTimestamp - _previousTimestamp;
        _previousTimestamp = currentTimestamp;

        if (_dataSent)
        {
            uint32 maxClientResponseDelay = sWorld->getIntConfig(CONFIG_WARDEN_CLIENT_RESPONSE_DELAY);

            if (maxClientResponseDelay > 0)
            {
                // Kick player if client response delays more than set in config
                if (_clientResponseTimer > maxClientResponseDelay * IN_MILLISECONDS)
                {
                    TC_LOG_WARN(LOG_FILTER_WARDEN, "%s (latency: %u, IP: %s) exceeded Warden module response delay for more than %s - disconnecting client",
                                   _session->GetPlayerInfo().c_str(), _session->GetLatency(), _session->GetRemoteAddress().c_str(), secsToTimeString(maxClientResponseDelay, true).c_str());
                    _session->KickPlayer();
                }
                else
                    _clientResponseTimer += diff;
            }
        }
        else
        {
            if (diff >= _checkTimer)
            {
                RequestData();
            }
            else
                _checkTimer -= diff;
        }
    }
}

void Warden::DecryptData(uint8* buffer, uint32 length)
{
    if (buffer)
        _inputCrypto.UpdateData(length, buffer);
}

void Warden::EncryptData(uint8* buffer, uint32 length)
{
    _outputCrypto.UpdateData(length, buffer);
}

bool Warden::IsValidCheckSum(uint32 checksum, const uint8* data, const uint16 length)
{
    uint32 newChecksum = BuildChecksum(data, length);

    if (checksum != newChecksum)
    {
        TC_LOG_DEBUG(LOG_FILTER_WARDEN, "CHECKSUM IS NOT VALID");
        return false;
    }
    else
    {
        TC_LOG_DEBUG(LOG_FILTER_WARDEN, "CHECKSUM IS VALID");
        return true;
    }
}

struct keyData {
    union
    {
        struct
        {
            uint8 bytes[20];
        } bytes;

        struct
        {
            uint32 ints[5];
        } ints;
    };
};

uint32 Warden::BuildChecksum(const uint8* data, uint32 length)
{
    keyData hash;
    SHA1(data, length, hash.bytes.bytes);
    uint32 checkSum = 0;
    for (uint8 i = 0; i < 5; ++i)
        checkSum = checkSum ^ hash.ints.ints[i];

    return checkSum;
}

std::string Warden::Penalty(WardenCheck* check /*= NULL*/)
{
    WardenActions action;

    if (check)
        action = check->Action;
    else
        action = WardenActions(sWorld->getIntConfig(CONFIG_WARDEN_CLIENT_FAIL_ACTION));

    switch (action)
    {
        case WARDEN_ACTION_LOG:
            return "None";
            break;
        case WARDEN_ACTION_KICK:
            {
                if (check)
                {
                    uint32 checkId = check->CheckId;
                    // EmuHacker Injection, Multi-Jump Hack, WPE Pro, Collision M2 Special, Collision M2 Regular, Collision WMD
                    if (checkId == 209 || checkId == 785 || checkId == 786 || checkId == 782 || checkId == 783 || checkId == 784)
                    {
                        std::string comment = check->Comment;
                        uint32 duration = 0;

                        QueryResult result = LoginDatabase.PQuery("SELECT `timesBanned` FROM `gonzy_auth`.`account` WHERE `id` = '%u'", _session->GetAccountId());
                        if (result)
                        {
                            Field* f = result->Fetch();
                            uint32 timesBanned = f[0].GetUInt32();
                            ++timesBanned;

                            switch (timesBanned)
                            {
                                case 0: duration =  0;        break;
                                case 1: duration =  2 * HOUR; break;
                                case 2: duration = 12 * HOUR; break;
                                case 3: duration =  2 * DAY;  break;
                                case 4: duration =  7 * DAY;  break;
                                default:
                                    duration = 14 * DAY;
                                    break;
                            }
                            LoginDatabase.PExecute("UPDATE `gonzy_auth`.`account` SET `timesBanned` = `timesBanned` + 1 WHERE `id` = '%u'", _session->GetAccountId());
                        
                            std::string accountName;
                            std::string ipAddress;
                            std::stringstream durationString;
                            std::stringstream banReason;
                            std::stringstream report;

                            AccountMgr::GetName(_session->GetAccountId(), accountName);
                            ipAddress = _session->GetRemoteAddress();
                            durationString << duration << "s";
                            banReason << comment << " (Ban level: " << timesBanned << ")";
                            report << "Ban (Level: " << timesBanned << ")";

                            sWorld->BanAccount(BAN_ACCOUNT, accountName, durationString.str(), banReason.str(), "System");
                            sWorld->BanAccount(BAN_IP, ipAddress, durationString.str(), banReason.str(), "System");
                            
                            return report.str();
                        }
                    }
                    else
                        _session->KickPlayer();
                }
                else
                    _session->KickPlayer();
                return "Kick";
            } break;
        case WARDEN_ACTION_BAN:
            {
                std::stringstream duration;
                duration << sWorld->getIntConfig(CONFIG_WARDEN_CLIENT_BAN_DURATION) << "s";
                std::string accountName;
                AccountMgr::GetName(_session->GetAccountId(), accountName);
                std::stringstream banReason;
                banReason << "Warden Anticheat Violation";
                // Check can be NULL, for example if the client sent a wrong signature in the warden packet (CHECKSUM FAIL)
                if (check)
                    banReason << ": " << check->Comment << " (CheckId: " << check->CheckId << ")";

                sWorld->BanAccount(BAN_ACCOUNT, accountName, duration.str(), banReason.str(),"Server");

                return "Ban";
            }
        default:
            break;
    }
    return "Undefined";
}

void WorldSession::HandleWardenDataOpcode(WorldPacket& recvData)
{
    if (_warden)
        _warden->DecryptData(recvData.contents(), recvData.size());
    uint8 opcode;
    recvData >> opcode;
    TC_LOG_DEBUG(LOG_FILTER_WARDEN, "Got packet, opcode %02X, size %u", opcode, uint32(recvData.size()));
    recvData.hexlike();

    switch (opcode)
    {
        case WARDEN_CMSG_MODULE_MISSING:
            if (_warden)
                _warden->SendModuleToClient();
            break;
        case WARDEN_CMSG_MODULE_OK:
            if (_warden)
                _warden->RequestHash();
            break;
        case WARDEN_CMSG_CHEAT_CHECKS_RESULT:
            if (_warden)
                _warden->HandleData(recvData);
            break;
        case WARDEN_CMSG_MEM_CHECKS_RESULT:
            TC_LOG_DEBUG(LOG_FILTER_WARDEN, "NYI WARDEN_CMSG_MEM_CHECKS_RESULT received!");
            break;
        case WARDEN_CMSG_HASH_RESULT:
            if (_warden)
            {
                _warden->HandleHashResult(recvData);
                _warden->InitializeModule();
            }
            break;
        case WARDEN_CMSG_MODULE_FAILED:
            TC_LOG_DEBUG(LOG_FILTER_WARDEN, "NYI WARDEN_CMSG_MODULE_FAILED received!");
            break;
        default:
            TC_LOG_DEBUG(LOG_FILTER_WARDEN, "Got unknown warden opcode %02X of size %u.", opcode, uint32(recvData.size() - 1));
            break;
    }
}
