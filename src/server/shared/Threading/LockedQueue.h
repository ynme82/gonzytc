/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2008 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOCKEDQUEUE_H
#define LOCKEDQUEUE_H

#include <ace/Guard_T.h>
#include <ace/Thread_Mutex.h>
#include <ace/Condition_Recursive_Thread_Mutex.h>
#include <ace/Condition_Thread_Mutex.h>
#include <ace/Condition_T.h>
#include <deque>
#include <assert.h>
#include "Debugging/Errors.h"

namespace ACE_Based
{
    template <class T, class LockType, typename StorageType=std::deque<T> >
        class LockedQueue
    {
        //! Cancellation flag.
        volatile bool _canceled;

        public:
            //! Storage backing the queue.
            StorageType _queue;

            //! Lock access to the queue.
            LockType _lock;

            //! Create a LockedQueue.
            LockedQueue()
                : _canceled(false)
            {
            }

            //! Destroy a LockedQueue.
            virtual ~LockedQueue()
            {
            }

            //! Adds an item to the queue.
            void add(const T& item)
            {
                lock();

                //ASSERT(!this->_canceled);
                // throw Cancellation_Exception();

                _queue.push_back(item);

                unlock();
            }

            inline void clear()
            {
                lock();
                _queue.clear();
                unlock();
            }

            //! Gets the next result in the queue, if any.
            bool next(T& result)
            {
               // ACE_Guard<LockType> g(this->_lock);
                ACE_GUARD_RETURN (LockType, g, this->_lock, false);

                if (_queue.empty())
                    return false;

                //ASSERT (!_queue.empty() || !this->_canceled);
                // throw Cancellation_Exception();
                result = _queue.front();
                _queue.pop_front();

                return true;
            }

            template<class Checker>
            bool next(T& result, Checker& check)
            {
                ACE_Guard<LockType> g(this->_lock);

                if (_queue.empty())
                    return false;

                result = _queue.front();
                if (!check.Process(result))
                    return false;

                _queue.pop_front();
                return true;
            }

            //! Peeks at the top of the queue. Check if the queue is empty before calling! Remember to unlock after use if autoUnlock == false.
            T& peek(bool autoUnlock = false)
            {
                lock();

                T& result = _queue.front();

                if (autoUnlock)
                    unlock();

                return result;
            }

            //! Cancels the queue.
            void cancel()
            {
                lock();

                _canceled = true;

                unlock();
            }

            //! Checks if the queue is cancelled.
            bool cancelled()
            {
                ACE_Guard<LockType> g(this->_lock);
                return _canceled;
            }

            //! Locks the queue for access.
            void lock()
            {
                this->_lock.acquire();
            }

            //! Unlocks the queue.
            void unlock()
            {
                this->_lock.release();
            }

            ///! Calls pop_front of the queue
            void pop_front()
            {
                ACE_GUARD (LockType, g, this->_lock);
                _queue.pop_front();
            }

            ///! Checks if we're empty or not with locks held
            bool empty()
            {
                ACE_GUARD_RETURN (LockType, g, this->_lock, false);
                return _queue.empty();
            }
    };

    template <class T, typename StorageType=std::deque<T> >
        class ActivationQueue
    {
        public:
            typedef typename StorageType::iterator iterator;
            typedef typename StorageType::const_iterator const_iterator;

            //! Lock access to the queue.
            mutable ACE_Thread_Mutex _lock;
            ACE_Thread_Mutex _sync_lock;
            ACE_Condition_Thread_Mutex _cond_lock;
            ACE_Time_Value m_absTime;

            //! Storage backing the queue.
            StorageType _queue;
            //! Cancellation flag.
            bool _canceled;

            //! Create a LockedQueue.
            explicit ActivationQueue()
                : _cond_lock(_sync_lock), m_absTime(), _canceled(false)
            { }

            //! Destroy a LockedQueue.
            virtual ~ActivationQueue() { }

            void SetWaitTime(time_t sec, suseconds_t usec)
            {
                m_absTime.set(sec, usec);
            }

            //! Adds an item to the queue.
            void add(const T& item)
            {
                lock();
                if (this->_canceled)
                    throw "Cancellation_Exception";
                _queue.push_back(item);
                if (_cond_lock.signal() != 0)   // dequeuer send to not empty
                    throw "sync error";
                unlock();
            }

            inline size_t size() const
            {
                lock();
                size_t res = _queue.size();
                unlock();
                return res;
            }

            inline void clear()
            {
                lock();
                _queue.clear();
                unlock();
            }

            inline bool empty() const
            {
                //ACE_Guard<LockType> g(this->_lock);
                return _queue.empty();
            }

            inline void WaitForNotEmpty()
            {
                while (empty())
                {
                    if (cancelled() || _cond_lock.wait() == -1)
                      break;
                }
            }

            //! Gets the next result in the queue, if any.
            bool next(T& result)
            {
                lock();
                do
                {
                    unlock();
                    WaitForNotEmpty();
                    lock();
                    if (cancelled() && empty())
                    {
                        unlock();
                        return false;
                    }
                } while (empty());

                result = _queue.front();
                _queue.pop_front();
                unlock();
                return true;
            }

            //! Peeks at the top of the queue. Remember to unlock after use.
            T& peek()
            {
                lock();
                T& result = _queue.front();
                unlock();
                return result;
            }

            //! Cancels the queue.
            void cancel()
            {
                lock();
                _canceled = true;
                _cond_lock.broadcast();
                unlock();
            }

            //! Checks if the queue is cancelled.
            inline bool cancelled() const
            {
                //ACE_Guard<ACE_Thread_Mutex> g(this->_lock);
                return _canceled;
            }

            //! Locks the queue for access.
            inline void lock() const
            {
                this->_lock.acquire();
            }

            //! Unlocks the queue.
            inline void unlock() const
            {
                this->_lock.release();
            }
    };
}
#endif
