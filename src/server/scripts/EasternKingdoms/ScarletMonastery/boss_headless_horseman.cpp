/*
 * Copyright (C) 2008-2013 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Boss_Headless_Horseman
SD%Complete:
SDComment:
SDCategory: Scarlet Monastery
EndScriptData */

#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "SpellMgr.h"
#include "scarlet_monastery.h"
#include "LFGMgr.h"
#include "Player.h"
#include "Group.h"
#include "SpellInfo.h"

enum Says
{
    EMOTE_LAUGH1                    = 11965,
    EMOTE_LAUGH2                    = 11975,
    EMOTE_LAUGH3                    = 11976,
};

enum Npcs
{
    NPC_HORSEMAN_MOUNTED            = 23682,
    NPC_HORSEMAN_UNHORSED           = 23800,
    NPC_HORSEMAN_HEAD               = 23775,
    NPC_PULSING_PUMPKIN             = 23694,
    NPC_PUMPKIN_FIEND               = 23545,
    NPC_HELPER                      = 23686,
    NPC_WISP_INVIS                  = 24034,
};

enum Spells
{
    SPELL_CLEAVE                    = 42587,
    SPELL_CONFLAGRATION             = 42380,
    SPELL_SUMMON_PUMPKIN            = 42394,
    SPELL_WHIRLWIND                 = 43118,
    SPELL_IMMUNE                    = 42556,
    SPELL_BODY_REGEN                = 42403,
    SPELL_CONFUSE                   = 43105,
    SPELL_HEAD_VISUAL               = 42413,
    SPELL_PUMPKIN_AURA              = 42280,
    SPELL_SQUASH_SOUL               = 42514,
    SPELL_SPROUTING                 = 42281,
    SPELL_SPROUT_BODY               = 42285,

    // Effects
    SPELL_WORLD_SHAKING             = 42909,
    SPELL_BODY_FLAME                = 42074,
    SPELL_HEAD_FLAME                = 42971,
};

struct HorsemanTexts
{
    uint32 sound;
    const char *text;
};

static HorsemanTexts HorsemanText[8] =
{
    {11961, "It is over, your search is done. Let fate choose now, the righteous one"},                 // Summon
    {12569, "Get over here, you idiot!"},                                                               // Lost Head 1
    {12568, "I am over here you fool of a body!"},                                                      // Lost Head 2
    {12567, "Here's my body, fit and pure! Now, your blackened souls I'll cure!"},                      // Rejoined
    {12573, "Harken, cur! Tis you I spurn! Now feel... the burn!"},                                     // Conflag
    {11963, "Soldiers arise, stand and fight! Bring victory at last to this fallen knight!"},           // Spawn Pumpkins
    {11964, "This end have I reached before. What new adventure lies in store?"},                       // Horseman dies
    {11962, "Your body lies beaten, battered and broken. Let my curse be your own, fate has spoken."},  // Player dies
};

const float horsemanWP[6][3] =
{
    {1766.084f, 1348.415f, 05.140f},
    {1766.650f, 1349.300f, 17.140f},
    {1796.707f, 1348.650f, 30.147f},
    {1838.088f, 1380.425f, 45.009f},
    {1788.904f, 1385.219f, 56.331f},
    {1759.694f, 1366.543f, 19.466f},    // 5: Land location
};

const char *SummonText[4] =
{
    "Horseman rise...",
    "Your time is nigh...",
    "You felt death once...",
    "Now, know demise!"
};

/*####
## boss_headless_horseman
####*/

class boss_headless_horseman : public CreatureScript
{
    public:
        boss_headless_horseman() : CreatureScript("boss_headless_horseman") { }

        struct boss_headless_horsemanAI : public ScriptedAI
        {
            boss_headless_horsemanAI(Creature *pCreature) : ScriptedAI(pCreature)
            {
                _instance = me->GetInstanceScript();
                Reset();
            }

            bool       m_bHeadDied, m_bIntroDone, m_bLanded, m_bFinishedWP;
            uint8      m_uiTextID, m_uiWP, m_uiPhase;
            uint32     m_uiTextTimer, m_uiHeadTimer, m_uiConflagTimer, m_uiCleaveTimer, m_uiLaughTimer;
            uint64     m_uiPlayerGUID, m_uiHeadGUID;
            InstanceScript* _instance;

            void SetFlying(bool x)
            {
                if (x)
                {
                    me->SetDisableGravity(true);
                    me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                    me->AddUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);
                    
                    WorldPacket data(SMSG_SPLINE_MOVE_SET_FLYING, 9);
                    data.append(me->GetPackGUID());
                    me->SendMessageToSet(&data, true);
                }
                else
                {
                    me->SetDisableGravity(false);
                    me->RemoveByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                    me->RemoveUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);

                    WorldPacket data(SMSG_SPLINE_MOVE_UNSET_FLYING, 9);
                    data.append(me->GetPackGUID());
                    me->SendMessageToSet(&data, true);
                }
                me->SendMovementCanFlyChange();
            }

            void Reset()
            {
                me->SetSpeed(MOVE_FLIGHT, 2.5f, true);
                me->SetSpeed(MOVE_WALK, 2.5f, true);
        
                SetFlying(true);

                m_bHeadDied     = false;
                m_bIntroDone    = false;
                m_bLanded       = false;
                m_bFinishedWP   = true;

                m_uiPlayerGUID  = 0;
                m_uiHeadGUID    = 0;

                m_uiWP          = 0;
                m_uiTextID      = 0;
                m_uiPhase       = 1;

                // Timers
                m_uiTextTimer       = 2 * IN_MILLISECONDS;
                m_uiLaughTimer      = urand(10, 15) * IN_MILLISECONDS;
                m_uiConflagTimer    = urand(15, 20) * IN_MILLISECONDS;
                m_uiCleaveTimer     = urand(2, 6) * IN_MILLISECONDS;

                if (!_instance)
                {
                    me->DespawnOrUnsummon();
                    return;
                }

                if (_instance->GetData(DATA_HORSEMAN_EVENT) == DONE)
                    me->DespawnOrUnsummon();
                else
                    _instance->SetData(DATA_HORSEMAN_EVENT, IN_PROGRESS);
            }

            Player* SelectRandomPlayer(float range = 0.0f, bool checkLoS = true)
            {
                Map* map = me->GetMap();
                if (!map->IsDungeon())
                    return NULL;

                Map::PlayerList const &PlayerList = map->GetPlayers();
                if (PlayerList.isEmpty())
                    return NULL;

                std::list<Player*> temp;
                for (Map::PlayerList::const_iterator i = PlayerList.begin(); i != PlayerList.end(); ++i)
                    if ((me->IsWithinLOSInMap(i->getSource()) || !checkLoS) && me->getVictim() != i->getSource() &&
                        me->IsWithinDistInMap(i->getSource(), range) && i->getSource()->isAlive())
                        temp.push_back(i->getSource());

                if (!temp.empty())
                {
                    std::list<Player*>::const_iterator j = temp.begin();
                    advance(j, rand()%temp.size());
                    return (*j);
                }
                return NULL;
            }

            void EnterCombat(Unit* who)
            {
                if (m_bLanded)
                {
                    me->SetInCombatWithZone();
                    ScriptedAI::EnterCombat(who);
                }
            }

            void AttackStart(Unit *pWho)
            {
                if (m_bLanded)
                    ScriptedAI::AttackStart(pWho);
            }

            void JustDied(Unit *pKiller)
            {
                me->MonsterYell(HorsemanText[6].text, LANG_UNIVERSAL, NULL);
                DoPlaySoundToSet(me, HorsemanText[6].sound);

                if (!_instance)
                    return;

                _instance->SetData(DATA_HORSEMAN_EVENT, DONE);

                Map* map = me->GetMap();
                if (!map)
                    return;

                Map::PlayerList const &players = map->GetPlayers();
                if (players.isEmpty())
                    return;

                for (Map::PlayerList::const_iterator i = players.begin(); i != players.end(); ++i)
                {
                    if (Player* player = i->getSource())
                        if (Group* grp = player->GetGroup())
                            if (grp->isLFGGroup())
                            {
                                sLFGMgr->FinishDungeon(grp->GetGUID(), 285);
                                return;
                            }
                }
            }

            void KilledUnit(Unit *pVictim)
            {
                if (pVictim && pVictim->ToPlayer())
                {
                    me->MonsterYell(HorsemanText[7].text, LANG_UNIVERSAL, NULL);
                    DoPlaySoundToSet(me, HorsemanText[7].sound);
                }
            }

            void SummonedBy(Unit *pSummoner)
            {
                if (pSummoner && pSummoner->ToPlayer())
                {
                    SetFlying(true);
                    m_uiPlayerGUID = pSummoner->GetGUID();
                    me->CastSpell(me, SPELL_WORLD_SHAKING, true);
                }
                else
                    me->DespawnOrUnsummon();
            }

            void MovementInform(uint32, uint32)
            {
                ++m_uiWP;
                m_bFinishedWP = true;
            }

            void StartHeadEvent()
            {
                if (m_uiHeadGUID)
                    return;

                ++m_uiPhase;
                me->CastSpell(me, SPELL_BODY_REGEN, true);
                me->CastSpell(me, SPELL_IMMUNE, true);
                me->CastSpell(me, SPELL_CONFUSE, true);

                float x, y, z;
                uint32 randYell = urand(1, 2);
                me->GetPosition(x, y, z);

                if (Creature *pHead = me->SummonCreature(NPC_HORSEMAN_HEAD, x, y, z, 0, TEMPSUMMON_CORPSE_DESPAWN, 0))
                {
                    pHead->CastSpell(pHead, SPELL_HEAD_VISUAL, true);
                    pHead->MonsterYell(HorsemanText[randYell].text, LANG_UNIVERSAL, NULL);

                    if (m_uiPhase == 4)
                        pHead->SetHealth(pHead->GetMaxHealth() * 66.6f/100.0f);

                    if (m_uiPhase == 6)
                        pHead->SetHealth(pHead->GetMaxHealth() * 33.3f/100.0f);

                    DoPlaySoundToSet(pHead, HorsemanText[randYell].sound);
                }
            }

            void JustSummoned(Creature *pSummoned)
            {
                if (pSummoned->GetEntry() == NPC_HORSEMAN_HEAD)
                    m_uiHeadGUID = pSummoned->GetGUID();
            }

            void DamageTaken(Unit* /*attacker*/, uint32& damage)
            {
                if (me->HasAura(SPELL_BODY_REGEN))
                    damage = 0;

                if (damage >= me->GetHealth())
                {
                    if (m_uiPhase < 7)
                    {
                        damage = 0;
                        StartHeadEvent();
                    }
                    else
                        me->CastSpell(me, SPELL_BODY_FLAME, true);
                }
            }

            void HeadEventDone(uint8 m_uiDone)
            {
                m_uiPhase = m_uiDone + 1;
                me->SetHealth(me->GetMaxHealth());
                me->RemoveAurasDueToSpell(SPELL_BODY_REGEN);
                me->RemoveAurasDueToSpell(SPELL_IMMUNE);
                me->RemoveAurasDueToSpell(SPELL_CONFUSE);

                if (Creature *pHead = me->GetMap()->GetCreature(m_uiHeadGUID))
                    if (pHead->isAlive())
                        pHead->DespawnOrUnsummon();

                m_uiHeadGUID = 0;
                if (m_uiPhase == 5)
                {
                    me->CastSpell(me, SPELL_SUMMON_PUMPKIN, true);
                    me->MonsterYell(HorsemanText[4].text, LANG_UNIVERSAL, NULL);
                    DoPlaySoundToSet(me, HorsemanText[4].sound);
                }
                else
                {
                    me->MonsterYell(HorsemanText[3].text, LANG_UNIVERSAL, NULL);
                    DoPlaySoundToSet(me, HorsemanText[3].sound);
                }
            }

            void DoIntro(uint32 uiDiff)                 // Player INTRO
            {
                if (m_uiTextTimer < uiDiff)
                {
                    if (m_uiTextID < 4)
                    {
                        if (Player *pPlayer = SelectRandomPlayer(100.0f, false))//(m_uiPlayerGUID))
                            pPlayer->Say(SummonText[m_uiTextID], LANG_UNIVERSAL);

                        ++m_uiTextID;
                        m_uiTextTimer = 3 * IN_MILLISECONDS;

                        if (Creature* trigger = me->SummonCreature(WORLD_TRIGGER, 1765.454224f, 1347.577148f, 18.718361f, 1.86f, TEMPSUMMON_TIMED_DESPAWN, 3000))
                            trigger->CastSpell(trigger, 65982, false);
                    }
                    else
                    {
                        if (Creature* trigger = me->SummonCreature(WORLD_TRIGGER, 1765.454224f, 1347.577148f, 18.718361f, 1.86f, TEMPSUMMON_TIMED_DESPAWN, 3000))
                            trigger->CastSpell(trigger, 65982, false);

                        m_bIntroDone = true;
                        me->GetMotionMaster()->MovePoint(m_uiWP, horsemanWP[0][0], horsemanWP[0][1], horsemanWP[0][2], false);
                    }
                }
                else
                    m_uiTextTimer -= uiDiff;
            }

            void DoMoveToLand()                         // Fly Around & Land
            {
                if (m_bFinishedWP)
                {
                    if (m_uiWP == 1)
                    {
                        me->MonsterYell(HorsemanText[0].text, LANG_UNIVERSAL, NULL);
                        DoPlaySoundToSet(me, HorsemanText[0].sound);
                    }

                    if (m_uiWP <= 5)
                    {
                        me->GetMotionMaster()->MovementExpired();
                        me->GetMotionMaster()->MovePoint(m_uiWP, horsemanWP[m_uiWP][0], horsemanWP[m_uiWP][1], horsemanWP[m_uiWP][2], false);
                        m_bFinishedWP = false;
                    }
                    else
                    {
                        m_bLanded = true;
                        me->GetMotionMaster()->Clear(true);
                        me->SetSpeed(MOVE_WALK, 1.0f, true);
                        SetFlying(false);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_ATTACKABLE_1);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PACIFIED);
                        me->SetInCombatWithZone();
                    }
                }
            }

            void UpdateAI(uint32 uiDiff)
            {
                if (!m_bIntroDone)
                {
                    DoIntro(uiDiff);
                    return;
                }

                if (!m_bLanded)
                {
                    DoMoveToLand();
                    return;
                }

                if (!UpdateVictim())
                    return;

                switch (m_uiPhase)
                {
                    case 2:
                    case 4:
                    case 6:
                    {
                        if (me->GetHealthPct() == 100.0f)
                            HeadEventDone(/*((m_uiPhase < 6) ? */m_uiPhase/* : 4)*/);

                        break;
                    }
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    {
                        if (m_uiLaughTimer < uiDiff)
                        {
                            DoPlaySoundToSet(me, RAND((uint32)EMOTE_LAUGH1, (uint32)EMOTE_LAUGH2, (uint32)EMOTE_LAUGH3));
                            m_uiLaughTimer      = urand(10, 15) * IN_MILLISECONDS;
                        }
                        else
                            m_uiLaughTimer -= uiDiff;

                        if (m_uiCleaveTimer < uiDiff)
                        {
                            if (me->IsWithinDist(me->getVictim(), MELEE_RANGE))
                                me->CastSpell(me->getVictim(), SPELL_CLEAVE, true);

                            m_uiCleaveTimer = urand(2, 6) * IN_MILLISECONDS;
                        }
                        else
                            m_uiCleaveTimer -= uiDiff;

                        if (m_uiPhase == 3)
                        {
                            if (m_uiConflagTimer < uiDiff)
                            {
                                if (Unit *pVictim = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                    me->CastSpell(pVictim, SPELL_CONFLAGRATION, true);

                                m_uiConflagTimer = urand(15, 20) * IN_MILLISECONDS;
                            }
                            else
                                m_uiConflagTimer -= uiDiff;
                        }

                        DoMeleeAttackIfReady();
                        break;
                    }
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_headless_horsemanAI(creature);
        }
};

/*####
## npc_horseman_head
####*/

class npc_horseman_head : public CreatureScript
{
    public:
        npc_horseman_head() : CreatureScript("npc_horseman_head") { }

        struct npc_horseman_headAI : public ScriptedAI
        {
            npc_horseman_headAI(Creature *pCreature) : ScriptedAI(pCreature) { Reset(); }

            uint8      m_uiPhase;
            uint32     m_uiFleeTimer;
            uint64     m_uiHorsemanGUID;
            bool       m_bFlee;

            void Reset()
            {
                m_uiHorsemanGUID    = 0;
                m_uiPhase           = 0;
                m_uiFleeTimer       = 1000;
                me->CastSpell(me, SPELL_HEAD_VISUAL, true);
            }

            void SummonedBy(Unit *pSummoner)
            {
                if (pSummoner && pSummoner->ToCreature())
                {
                    m_uiHorsemanGUID = pSummoner->GetGUID();
                    if (boss_headless_horseman::boss_headless_horsemanAI* pHorsemanAI = dynamic_cast<boss_headless_horseman::boss_headless_horsemanAI*>(((Creature*)pSummoner)->AI()))
                        m_uiPhase = pHorsemanAI->m_uiPhase;
                }
                else
                    me->DespawnOrUnsummon();
            }

            void DamageTaken(Unit *done_by, uint32 &damage)
            {
                float healthPCT = me->GetHealthPct();
                float health = me->GetHealth();

                switch (m_uiPhase)
                {
                    case 2:
                    {
                        if (healthPCT <= 66.6f)
                        {
                            if (Creature *pHorseman = me->GetMap()->GetCreature(m_uiHorsemanGUID))
                                CAST_AI(boss_headless_horseman::boss_headless_horsemanAI, pHorseman->AI())->HeadEventDone(m_uiPhase);

                            damage = 0;
                        }
                        break;
                    }
                    case 4:
                    {
                        if (healthPCT <= 33.3f)
                        {
                            if (Creature *pHorseman = me->GetMap()->GetCreature(m_uiHorsemanGUID))
                                CAST_AI(boss_headless_horseman::boss_headless_horsemanAI, pHorseman->AI())->HeadEventDone(m_uiPhase);

                            damage = 0;
                        }
                        break;
                    }
                    case 6:
                    {
                        if (damage >= health)
                        {
                            if (Creature *pHorseman = me->GetMap()->GetCreature(m_uiHorsemanGUID))
                                CAST_AI(boss_headless_horseman::boss_headless_horsemanAI, pHorseman->AI())->HeadEventDone(m_uiPhase);

                        }
                        break;
                    }
                }
            }

            void UpdateAI(uint32 uiDiff)
            {
                if (m_uiFleeTimer < uiDiff)
                {
                    if (me->GetMotionMaster()->GetCurrentMovementGeneratorType() != FLEEING_MOTION_TYPE)
                    {
                        me->GetMotionMaster()->Clear(false);
                        me->GetMotionMaster()->MoveFleeing(me->getVictim());
                    }

                    DoCast(me, SPELL_WHIRLWIND, true);
                    m_uiFleeTimer = 1000;
                }
                else
                    m_uiFleeTimer -= uiDiff;
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_horseman_headAI(creature);
        }
};

/*####
## npc_pulsing_pumpkin
####*/

class npc_pulsing_pumpkin : public CreatureScript
{
    public:
        npc_pulsing_pumpkin() : CreatureScript("npc_pulsing_pumpkin") { }

        struct npc_pulsing_pumpkinAI : public ScriptedAI
        {
            npc_pulsing_pumpkinAI(Creature *pCreature) : ScriptedAI(pCreature) { Reset(); }

            uint32  m_uiTimer;
            bool    m_bSprouted;

            void Reset()
            {
                me->CastSpell(me, SPELL_PUMPKIN_AURA, true);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
                m_bSprouted     = false;
                m_uiTimer       = 5 * IN_MILLISECONDS;
            }

            void UpdateAI(uint32 uiDiff)
            {
                if (!m_bSprouted)
                {
                    if (m_uiTimer < uiDiff)
                    {
                        me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_STUNNED);
                        me->RemoveAurasDueToSpell(SPELL_PUMPKIN_AURA);
                        me->CastSpell(me, SPELL_SPROUT_BODY, true);
                        m_bSprouted = true;
                        me->SetInCombatWithZone();
                    }
                    else
                        m_uiTimer -= uiDiff;

                    return;
                }

                if (!UpdateVictim())
                    return;

                DoMeleeAttackIfReady();
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_pulsing_pumpkinAI(creature);
        }
};

class go_loosely_turned_soil : public GameObjectScript
{
public:
    go_loosely_turned_soil() : GameObjectScript("go_loosely_turned_soil") { }

    bool OnQuestReward(Player* player, GameObject* go, Quest const* quest, uint32 opt)
    {
        InstanceScript* instance = player->GetInstanceScript();
        if (instance)
        {
            if (instance->GetData(DATA_HORSEMAN_EVENT) != NOT_STARTED)
                return true;
            instance->SetData(DATA_HORSEMAN_EVENT, IN_PROGRESS);
        }

        player->SummonCreature(NPC_HORSEMAN_MOUNTED, horsemanWP[0][0], horsemanWP[0][1], horsemanWP[0][2], 0, TEMPSUMMON_DEAD_DESPAWN, 30 * MINUTE * IN_MILLISECONDS);
        return true;
    }
    /*
    bool OnGossipHello(Player* player, GameObject* soil)
    {
        InstanceScript* instance = player->GetInstanceScript();
        if (instance)
        {
            if (instance->GetData(DATA_HORSEMAN_EVENT) != NOT_STARTED)
                return true;
            instance->SetData(DATA_HORSEMAN_EVENT, IN_PROGRESS);
        }

        player->AreaExploredOrEventHappens(11405);
        player->SummonCreature(NPC_HORSEMAN_MOUNTED, horsemanWP[0][0], horsemanWP[0][1], horsemanWP[0][2], 0, TEMPSUMMON_DEAD_DESPAWN, 30 * MINUTE * IN_MILLISECONDS);
        return true;
    }*/
};

void AddSC_boss_headless_horseman()
{
    new boss_headless_horseman();
    new npc_horseman_head();
    new npc_pulsing_pumpkin();
    new go_loosely_turned_soil();
}
