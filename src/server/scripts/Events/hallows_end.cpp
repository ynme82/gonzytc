/* Copyright (C) 2010 - 2013 GonzyWoW <http://www.gonzywow.net>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

enum eShadeVars
{
    // MISC
    POINT_LAST_POINT            = 0xFFFFFF,
    POINT_HOME                  = 0xFFFFFE,

    // GOs
    GO_FIRE_EFFIGY              = 186720,       // The puppet that is on fire

    // SPELLs
    SPELL_HORSEMAN_EVENT        = 47771,        // Event indicator
    SPELL_HORSEMAN_QUEST        = 42242,        // Quest completed :)
    SPELL_CONFLAG_TRAIL         = 42142,        // 25 yard conflag target vis
    SPELL_SUMMON_LANTERN        = 44231,        // Horseman Dies summons Lantern >> triggers 44231
    SPELL_HORSEMAN_START_FIRE   = 42132,        // This spell is casted by the horseman to spread the fire
    SPELL_HORSEMAN_FIRE         = 42074,        // Fire visual
    SPELL_HORSEMAN_CONFLAG      = 42869,        // Horseman targets 1 player / encounter & casts conflag on him/her
    SPELL_BUCKET_LANDS          = 42339,        // Spell that should decrease fire size
    SPELL_HORSEMAN_SMOKE        = 42355,        // Event victory FAILs

    // QUESTs that trigger the event
    QUEST_STOP_FIRES1           = 11131,
    QUEST_STOP_FIRES2           = 11219,
    QUEST_STOP_FIRES3           = 12135,
    QUEST_STOP_FIRES4           = 12139,

    // NPCs
    NPC_HORSEMAN_SHADE          = 23543,
    NPC_HORSEMAN_TRIGGER        = 23537,
    NPC_LANTERN_TRIGGER         = 18721
};

struct HorsemanYells
{
    uint32 sound;
    const char *text;
};

static HorsemanYells HorsemanEmote[7] =
{
    {11966, "Prepare yourselves, the bells have tolled! Shelter your weak, your young and your old! Each of you shall pay the final sum. Cry for mercy, the reckoning has come!"},
    {12570, "The sky is dark. The fire burns. You strive in vain as Fate's wheel turns."},                  // + 2min
    {12571, "The town still burns, a cleansing fire! Time is short, I'll soon retire!"},                    // + 4min 30s
    {11967, "Fire consumes! You've tried and failed. Let there be no doubt, justice prevailed!"},           // + 6min 40s
    {11968, "My flames have died, left not a spark. I shall send you myself, to the lifeless dark."},
    {11969, "So eager you are, for my blood to spill. Yet to vanquish me, 'tis my head you must kill!"},
    {12573, "Harken, cur! Tis you I spurn! Now feel... the burn!"}                                          //(Conflagration)
};

/*####
## Spawn locations
####*/

const float HorsemanSpawnLoc[6][3] =
{
    {-9376.847f,    -23.557f,  82.488f},            // Goldshire
    {-5642.718f,   -407.187f, 421.057f},            // Kharanos
    { 2152.742f,    232.691f,  83.720f},            // Brill
    {  218.845f,  -4735.754f,  48.682f},            // Razor Hill
    {-4191.565f, -12596.609f,  81.291f},            // Azure Watch
    { 9589.966f,  -6732.601f,  48.656f},            // Falconwing Square
};

/*####
## Landing locations
####*/

const float HorsemanDieLoc[6][3] =
{
    {-9465.820f,     65.407f,  55.979f},            // Goldshire
    {-5606.703f,   -480.495f, 396.979f},            // Kharanos
    { 2256.296f,    288.076f,  34.113f},            // Brill
    {  290.898f,  -4745.286f,   9.399f},            // Razor Hill
    {-4196.833f, -12496.359f,  44.362f},            // Azure Watch
    { 9509.413f,  -6824.237f,  16.491f},            // Falconwing Square
};

/*####
## Fire trigger spawn locations
####*/

const float TriggerLoc[6][15][3] =
{
    {
        {-9479.790f,     30.799f,  69.493f},        // Goldshire
        {-9479.630f,     31.132f,  64.171f},
        {-9478.000f,     35.819f,  57.480f},
        {-9476.990f,     42.336f,  71.163f},
        {-9471.450f,     40.874f,  64.311f},
        {-9467.590f,     93.881f,  69.901f},
        {-9467.230f,     41.584f,  72.673f},
        {-9466.890f,    102.424f,  66.497f},
        {-9465.500f,     84.108f,  66.848f},
        {-9459.400f,     81.301f,  70.018f},
        {-9459.330f,     42.726f,  64.319f},
        {-9457.500f,     36.870f,  70.197f},
        {-9453.170f,     94.722f,  69.582f},
        {-9452.750f,    101.178f,  66.188f},
        {-9452.380f,     83.798f,  66.208f},
    },
    {
        {-5755.760f,   -531.653f, 404.606f},        // Kharanos
        {-5608.890f,   -513.855f, 413.387f},
        {-5594.800f,   -515.264f, 413.135f},
        {-5591.300f,   -443.938f, 417.191f},
        {-5584.510f,   -460.206f, 414.311f},
        {-5584.040f,   -503.670f, 413.176f},
        {-5583.570f,   -505.873f, 404.967f},
        {-5578.980f,   -449.912f, 419.304f},
        {-5576.060f,   -512.004f, 413.286f},
        {-5571.680f,   -460.944f, 414.135f},
        {-5567.260f,   -448.799f, 414.105f},
        {    0.000f,      0.000f,   0.000f},
        {    0.000f,      0.000f,   0.000f},
        {    0.000f,      0.000f,   0.000f},
        {    0.000f,      0.000f,   0.000f},
    },
    {
        { 2230.720f,    304.619f,  44.774f},        // Brill
        { 2240.000f,    306.017f,  48.482f},
        { 2240.450f,    251.792f,  41.351f},
        { 2242.600f,    311.525f,  45.134f},
        { 2244.910f,    246.201f,  46.716f},
        { 2247.630f,    257.556f,  46.957f},
        { 2251.230f,    320.203f,  45.638f},
        { 2253.210f,    256.514f,  41.623f},
        { 2258.850f,    317.379f,  40.911f},
        { 2260.460f,    330.300f,  45.674f},
        { 2260.630f,    323.058f,  41.914f},
        { 2268.860f,    253.967f,  41.594f},
        { 2283.990f,    274.818f,  48.176f},
        { 2286.960f,    287.283f,  48.972f},
        { 2293.340f,    266.383f,  48.727f},
    },
    {
        {  269.666f,  -4715.960f,  19.276f},        // Razor Hill
        {  272.681f,  -4707.860f,  22.672f},
        {  276.522f,  -4717.920f,  15.026f},
        {  278.047f,  -4702.130f,  19.077f},
        {  283.315f,  -4707.360f,  15.027f},
        {  299.440f,  -4776.400f,  19.825f},
        {  301.745f,  -4776.090f,  27.699f},
        {  312.163f,  -4774.950f,  27.699f},
        {  312.865f,  -4774.870f,  20.262f},
        {  319.154f,  -4699.210f,  15.850f},
        {  323.790f,  -4694.570f,  30.894f},
        {  327.339f,  -4700.210f,  24.237f},
        {  333.232f,  -4703.540f,  23.936f},
        {  336.281f,  -4702.340f,  30.597f},
        {  339.808f,  -4710.360f,  15.379f},
    },
    {
        {-4224.440f, -12509.200f,  48.797f},        // Azure Watch
        {-4220.440f, -12529.200f,  49.685f},
        {-4218.750f, -12517.600f,  56.822f},
        {-4218.420f, -12508.400f,  48.770f},
        {-4216.120f, -12522.000f,  52.959f},
        {-4214.940f, -12523.700f,  50.791f},
        {-4150.950f, -12473.900f,  51.494f},
        {-4146.840f, -12465.200f,  48.401f},
        {-4145.980f, -12483.100f,  56.762f},
        {-4143.330f, -12468.200f,  54.375f},
        {-4137.930f, -12488.600f,  51.709f},
        {-4134.120f, -12483.100f,  57.376f},
        {-4126.460f, -12482.500f,  47.056f},
        {    0.000f,      0.000f,   0.000f},
        {    0.000f,      0.000f,   0.000f},
    },
    {
        { 9478.220f,  -6820.040f,  23.422f},        // Falconwing Square
        { 9481.820f,  -6827.070f,  23.353f},
        { 9484.670f,  -6837.060f,  31.814f},
        { 9485.540f,  -6836.270f,  24.194f},
        { 9491.610f,  -6844.110f,  31.816f},
        { 9493.070f,  -6843.820f,  24.928f},
        { 9502.110f,  -6847.230f,  23.281f},
        { 9508.870f,  -6850.830f,  23.408f},
        { 9515.840f,  -6861.450f,  20.975f},
        { 9521.080f,  -6862.680f,  32.404f},
        { 9521.200f,  -6861.560f,  25.266f},
        { 9531.270f,  -6862.810f,  32.408f},
        { 9533.990f,  -6861.630f,  24.272f},
        { 9535.130f,  -6861.630f,  20.979f},
        {    0.000f,      0.000f,   0.000f},
    },
};

/*####
## Horseman waypoints at each location
####*/

const float HorsemanWPs[6][10][3] =
{
    {
        {-9437.977f, 56.798f, 90.187f},             // Goldshire
        {-9455.533f, 41.460f, 91.066f},
        {-9491.075f, 36.955f, 88.708f},
        {-9490.446f, 78.297f, 85.589f},
        {-9464.768f, 88.692f, 93.532f},
        {-9443.903f, 65.651f, 86.276f},
        {-9463.603f, 40.212f, 84.426f},
        {-9500.513f, 45.801f, 82.053f},
        {-9487.821f, 76.754f, 81.211f},
        {-9453.472f, 85.260f, 81.221f},
    },
    {
        {-5628.602f, -499.192f, 422.894f},          // Kharanos
        {-5583.556f, -510.701f, 431.578f},
        {-5555.722f, -485.278f, 434.896f},
        {-5565.563f, -457.724f, 434.799f},
        {-5602.450f, -453.411f, 433.044f},
        {-5627.415f, -487.350f, 428.105f},
        {-5596.355f, -513.229f, 429.067f},
        {-5563.100f, -490.104f, 427.855f},
        {-5598.742f, -452.596f, 435.171f},
        {-5620.552f, -462.616f, 428.169f},
    },
    {
        {2183.532f, 266.771f, 73.934f},             // Brill
        {2232.415f, 302.048f, 69.942f},
        {2258.648f, 353.582f, 67.414f},
        {2320.734f, 354.640f, 74.241f},
        {2282.443f, 295.279f, 81.345f},
        {2243.021f, 257.544f, 83.337f},
        {2185.065f, 258.822f, 73.911f},
        {2259.052f, 334.140f, 66.313f},
        {2286.265f, 302.165f, 72.118f},
        {2257.239f, 280.112f, 66.239f},
    },
    {
        {251.574f, -4751.235f, 47.142f},            // Razor Hill
        {302.637f, -4764.872f, 42.348f},
        {338.989f, -4747.677f, 41.642f},
        {336.440f, -4717.428f, 43.115f},
        {304.402f, -4683.980f, 36.180f},
        {255.449f, -4719.633f, 45.288f},
        {305.387f, -4765.591f, 41.167f},
        {358.004f, -4738.686f, 49.016f},
        {305.917f, -4687.545f, 46.875f},
        {279.628f, -4708.373f, 36.903f},
    },
    {
        {-4205.538f, -12562.099f, 77.547f},         // Azure Watch
        {-4236.020f, -12508.696f, 95.544f},
        {-4189.798f, -12435.624f, 94.817f},
        {-4143.858f, -12484.259f, 72.631f},
        {-4139.319f, -12545.995f, 95.653f},
        {-4189.353f, -12589.907f, 97.778f},
        {-4205.468f, -12499.001f, 96.538f},
        {-4137.032f, -12497.864f, 82.305f},
        {-4114.457f, -12556.716f, 88.509f},
        {-4154.476f, -12583.909f, 92.519f},
    },
    {
        {9534.217f, -6771.067f, 54.540f},           // Falconwing Square
        {9478.620f, -6816.648f, 39.413f},
        {9492.372f, -6833.042f, 48.134f},
        {9542.758f, -6854.495f, 30.248f},
        {9578.595f, -6814.222f, 48.195f},
        {9528.833f, -6777.280f, 58.000f},
        {9486.490f, -6822.174f, 43.019f},
        {9528.250f, -6854.367f, 30.999f},
        {9599.425f, -6825.434f, 63.748f},
        {9567.514f, -6780.270f, 54.165f},
    },
};

enum horseMEvents
{
    EVENT_CAST_FIRE = 1,
    EVENT_SPREAD,
    EVENT_INITIAL_FIRE,
    EVENT_REMOVE_SUMMONS,
    EVENT_FAILED,
    EVENT_SHOUT,
};

class CastFire : public BasicEvent
{
    public:
        explicit CastFire(Creature* owner, Creature* target)
            : _owner(owner), _target(target)
        {
        }

        bool Execute(uint64 /*time*/, uint32 /*diff*/)
        {
            _owner->CastSpell(_target, SPELL_HORSEMAN_START_FIRE, true);
            return true;
        }

    private:
        Creature* _owner;
        Creature* _target;
};

/*####
## npc_shade_of_the_horseman
####*/

class npc_shade_of_the_horseman : public CreatureScript
{
    public:
        npc_shade_of_the_horseman() : CreatureScript("npc_shade_of_the_horseman") { }

        struct npc_shade_of_the_horsemanAI : public ScriptedAI
        {
            npc_shade_of_the_horsemanAI(Creature *pCreature) : ScriptedAI(pCreature),
                BurningTrigger(pCreature), AvailableTrigger(pCreature) { Reset(); }

            uint8   m_uiLoc, m_uiText;
            uint32  m_uiWP;
            uint64  m_uiMatronGUID;
            bool    m_bWP, m_bEventSuccess, m_bEventFailed, m_bEventLanded;

            SummonList  BurningTrigger;
            SummonList  AvailableTrigger;

            EventMap _events;

            void SetFlying(bool x)
            {
                if (x)
                {
                    me->SetDisableGravity(true);
                    me->SetByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                    me->AddUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);
            
                    WorldPacket data(SMSG_SPLINE_MOVE_SET_FLYING, 9);
                    data.append(me->GetPackGUID());
                    me->SendMessageToSet(&data, true);
                }
                else
                {
                    me->SetDisableGravity(false);
                    me->RemoveByteFlag(UNIT_FIELD_BYTES_1, 3, 0x02);
                    me->RemoveUnitMovementFlag(MOVEMENTFLAG_CAN_FLY | MOVEMENTFLAG_FLYING);

                    WorldPacket data(SMSG_SPLINE_MOVE_UNSET_FLYING, 9);
                    data.append(me->GetPackGUID());
                    me->SendMessageToSet(&data, true);
                }
                me->SendMovementCanFlyChange();
            }

            void Reset()
            {
                me->SetInCombatState(true);
                me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IN_COMBAT);
                m_uiMatronGUID = 0;
                _events.Reset();
            }

            void SummonedBy(Unit *pSummoner)
            {
                if (!pSummoner)
                {
                    me->DespawnOrUnsummon();
                    return;
                }

                m_uiMatronGUID  = pSummoner->GetGUID();
                m_uiLoc         = 0;
                m_uiWP          = 0;
                m_uiText        = 1;
                m_bEventLanded  = false;
                m_bWP           = true;
                m_bEventSuccess = false;
                m_bEventFailed  = false;

                me->setActive(true);
                SetFlying(true);
                me->SetSpeed(MOVE_WALK, 3.0f);
                _events.ScheduleEvent(EVENT_INITIAL_FIRE, 15 * IN_MILLISECONDS);
                _events.ScheduleEvent(EVENT_SHOUT, 400 * IN_MILLISECONDS);
            }

            void TriggerList()
            {
                BurningTrigger.DespawnAll();
                AvailableTrigger.DespawnAll();

                for (int8 i = 0; i < 15; ++i)
                {
                    if (TriggerLoc[m_uiLoc][i][0] == 0)
                        continue;

                    if (Creature *pTrigger = me->SummonCreature(NPC_HORSEMAN_TRIGGER, TriggerLoc[m_uiLoc][i][0], TriggerLoc[m_uiLoc][i][1], TriggerLoc[m_uiLoc][i][2], 0, TEMPSUMMON_CORPSE_TIMED_DESPAWN, IN_MILLISECONDS))
                        AvailableTrigger.Summon(pTrigger);
                }
            }

            void MovementInform(uint32 type, uint32 id)
            {
                if (id == POINT_LAST_POINT)
                {
                    me->Dismount();
                    SetFlying(false);
                    SetCombatMovement(false);
                    m_bEventLanded = true;
                    return;
                }
                else if (id == POINT_HOME)
                {
                    RemoveSummons();
                    me->DespawnOrUnsummon();
                    return;
                }

                if (m_bEventSuccess || m_bEventFailed)
                    return;

                ++m_uiWP;
                if (m_uiWP >= 10)
                    m_uiWP = 0;

                m_bWP = true;
            }

            void EngageFires(uint8 location)
            {
                me->MonsterYell(HorsemanEmote[0].text, LANG_UNIVERSAL, NULL);
                DoPlaySoundToSet(me, HorsemanEmote[0].sound);
                m_uiLoc = location;
                TriggerList();
                me->GetMotionMaster()->MovementExpired();
                me->GetMotionMaster()->MovePoint(0, HorsemanWPs[m_uiLoc][m_uiWP][0], HorsemanWPs[m_uiLoc][m_uiWP][1], HorsemanWPs[m_uiLoc][m_uiWP][2], false);
            }

            void SpellHitTarget(Unit *pTarget, const SpellInfo *pSpell)
            {
                if (pSpell->Id != SPELL_HORSEMAN_START_FIRE)
                    return;

                BurningTrigger.Summon(pTarget->ToCreature());
                AvailableTrigger.Despawn(pTarget->ToCreature());
            }

            void StartInitialFire()
            {
                for (uint8 i = 0; i < 5; ++i)
                {
                    if (AvailableTrigger.empty())
                        continue;

                    SummonList::iterator itr = AvailableTrigger.begin();
                    std::advance(itr, urand(0, AvailableTrigger.size()-1));

                    if (Creature *pTemp = me->GetMap()->GetCreature(*itr))
                        me->m_Events.AddEvent(new CastFire(me, pTemp), me->m_Events.CalculateTime(1200 * i));
                }

                _events.ScheduleEvent(EVENT_SHOUT, 2 * MINUTE * IN_MILLISECONDS);
                _events.ScheduleEvent(EVENT_FAILED, 400 * IN_MILLISECONDS);
                _events.ScheduleEvent(EVENT_SPREAD, urand(15, 20) * IN_MILLISECONDS);
            }

            void SpreadFire()
            {
                if (AvailableTrigger.empty())
                    return;

                SummonList::iterator itr = AvailableTrigger.begin();
                std::advance(itr, urand(0, AvailableTrigger.size() - 1));

                if (Creature *pTemp = me->GetMap()->GetCreature(*itr))
                    DoCast(pTemp, SPELL_HORSEMAN_START_FIRE, true);

                _events.ScheduleEvent(EVENT_SPREAD, urand(15, 20) * IN_MILLISECONDS);
            }

            void FireOut(Creature* crea)
            {
                if (m_bEventSuccess)
                    return;

                BurningTrigger.Despawn(crea);
                AvailableTrigger.Summon(crea);

                if (m_bEventSuccess)
                    return;

                if (BurningTrigger.size() == 0)
                {
                    m_bEventSuccess = true;
                    me->m_Events.KillAllEvents(true);
                    if (Creature *pTemp = me->GetMap()->GetCreature(m_uiMatronGUID))
                        pTemp->CastSpell(pTemp, SPELL_HORSEMAN_QUEST, true);
                    else
                        DoCast(me, SPELL_HORSEMAN_QUEST, true);

                    me->MonsterYell(HorsemanEmote[4].text, LANG_UNIVERSAL, NULL);
                    DoPlaySoundToSet(me, HorsemanEmote[4].sound);

                    me->GetMotionMaster()->MovementExpired();
                    me->GetMotionMaster()->MovePoint(POINT_LAST_POINT, HorsemanDieLoc[m_uiLoc][0], HorsemanDieLoc[m_uiLoc][1], HorsemanDieLoc[m_uiLoc][2], false);
                    me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NON_ATTACKABLE | UNIT_FLAG_NOT_ATTACKABLE_1 | UNIT_FLAG_IMMUNE_TO_PC);
                }
            }

            void RemoveSummons()
            {
                if (!BurningTrigger.empty())
                    for (SummonList::iterator itr = BurningTrigger.begin(); itr != BurningTrigger.end(); ++itr)
                        if (Creature *pTemp = me->GetMap()->GetCreature(*itr))
                        {
                            pTemp->RemoveAllAuras();
                            pTemp->DespawnOrUnsummon(20000);
                            pTemp->CastSpell(pTemp, SPELL_HORSEMAN_SMOKE, true);
                        }

                AvailableTrigger.DespawnAll();

                if (Creature *pTemp = me->GetMap()->GetCreature(m_uiMatronGUID))
                    pTemp->CastSpell(pTemp, SPELL_HORSEMAN_EVENT, true);
            }

            void JustDied(Unit*/*pKiller*/)
            {
                RemoveSummons();
            }

            void DamageTaken(Unit *done_by, uint32 &damage)
            {
                if (damage < me->GetHealth())
                    return;

                me->MonsterYell(HorsemanEmote[5].text, LANG_UNIVERSAL, NULL);
                DoPlaySoundToSet(me, HorsemanEmote[5].sound);
                float x = HorsemanDieLoc[m_uiLoc][0];
                float y = HorsemanDieLoc[m_uiLoc][1];
                float z = HorsemanDieLoc[m_uiLoc][2];
                me->GetNearPoint2D(x, y, 5.0f, me->GetAngle(x, y));
                me->UpdateGroundPositionZ(x, y, z);

                if (Creature *pTemp = me->SummonCreature(NPC_LANTERN_TRIGGER, x, y, z, 0, TEMPSUMMON_TIMED_DESPAWN, 5000))
                    pTemp->CastSpell(pTemp, SPELL_SUMMON_LANTERN, true);
            }

            void EnterEvadeMode() {};

            void UpdateAI(uint32 uiDiff)
            {
                _events.Update(uiDiff);

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_SHOUT:
                        {
                            me->MonsterYell(HorsemanEmote[m_uiText].text, LANG_UNIVERSAL, NULL);
                            DoPlaySoundToSet(me, HorsemanEmote[m_uiText].sound);
                            ++m_uiText;

                            if (m_uiText < 3)
                                _events.ScheduleEvent(EVENT_SHOUT, 2.5 * MINUTE * IN_MILLISECONDS);
                            break;
                        }
                        case EVENT_FAILED:
                        {
                            _events.ScheduleEvent(EVENT_REMOVE_SUMMONS, 3 * IN_MILLISECONDS);
                            m_bEventFailed = true;
                            me->GetMotionMaster()->MovementExpired();
                            me->GetMotionMaster()->MovePoint(POINT_HOME, HorsemanSpawnLoc[m_uiLoc][0], HorsemanSpawnLoc[m_uiLoc][1], HorsemanSpawnLoc[m_uiLoc][2], false);
                            me->MonsterYell(HorsemanEmote[3].text, LANG_UNIVERSAL, NULL);
                            DoPlaySoundToSet(me, HorsemanEmote[3].sound);

                            break;
                        }
                        case EVENT_REMOVE_SUMMONS:
                        {
                            RemoveSummons();
                            me->DespawnOrUnsummon();
                            break;
                        }
                        case EVENT_INITIAL_FIRE:
                        {
                            StartInitialFire();
                            break;
                        }
                        case EVENT_SPREAD:
                        {
                            SpreadFire();
                            break;
                        }
                    }
                }

                if (m_bEventLanded)
                {
                    if (!UpdateVictim())
                        return;

                    DoMeleeAttackIfReady();
                    return;
                }

                if (m_bWP)
                {
                    me->GetMotionMaster()->MovementExpired();
                    me->GetMotionMaster()->MovePoint(m_uiWP, HorsemanWPs[m_uiLoc][m_uiWP][0], HorsemanWPs[m_uiLoc][m_uiWP][1], HorsemanWPs[m_uiLoc][m_uiWP][2], false);
                    m_bWP = false;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_shade_of_the_horsemanAI(creature);
        }
};

/*####
## npc_horseman_fire
####*/

class npc_horseman_fire : public CreatureScript
{
    public:
        npc_horseman_fire() : CreatureScript("npc_horseman_fire") { }

        struct npc_horseman_fireAI : public ScriptedAI
        {
            npc_horseman_fireAI(Creature *c) : ScriptedAI(c) {Reset();}

            bool    m_bEvent, m_bSmoke, m_bOnFire;
            uint32  m_uiSmokeTimer, m_uiEscalateTimer;
            uint64  m_uiOwnerGUID;
            float   m_fScale;

            void Reset()
            {
                me->setActive(true);

                m_bEvent            = false;
                m_bOnFire           = true;
                m_bSmoke            = false;
                m_uiSmokeTimer      = 10 * IN_MILLISECONDS;
                m_uiOwnerGUID       = 0;
                m_uiEscalateTimer   = 15 * IN_MILLISECONDS;
                m_fScale            = 0.4f;
                me->SetObjectScale(m_fScale);
            }

            void SummonedBy(Unit *pSummoner)
            {
                if (pSummoner && pSummoner->GetTypeId() == TYPEID_UNIT)
                {
                    m_bEvent        = true;
                    m_bOnFire       = false;
                    m_uiOwnerGUID   = pSummoner->GetGUID();
                }
            }

            void SpellHit(Unit *pCaster, const SpellInfo *pSpell)
            {
                switch (pSpell->Id)
                {
                    case SPELL_HORSEMAN_SMOKE:
                    {
                        me->RemoveAurasDueToSpell(SPELL_HORSEMAN_FIRE);
                        m_bSmoke = true;
                        m_bEvent = false;
                        break;
                    }
                    case SPELL_HORSEMAN_START_FIRE:
                    {
                        me->RemoveAurasDueToSpell(SPELL_HORSEMAN_START_FIRE);
                        me->CastSpell(me, SPELL_HORSEMAN_FIRE, true);
                        m_bOnFire = true;
                        m_bEvent  = true;
                        m_fScale = 0.8f;
                        m_uiEscalateTimer = 12 * IN_MILLISECONDS;
                        me->SetObjectScale(m_fScale);
                        break;
                    }
                    case SPELL_BUCKET_LANDS:
                    {
                        m_fScale -= 0.2f;

                        if (m_fScale <= 0.4f)
                        {
                            if (m_bEvent && m_bOnFire)
                            {
                                me->RemoveAurasDueToSpell(SPELL_HORSEMAN_FIRE);
                                m_bOnFire = false;

                                if (Creature *pHorseman = me->GetMap()->GetCreature(m_uiOwnerGUID))
                                    CAST_AI(npc_shade_of_the_horseman::npc_shade_of_the_horsemanAI, pHorseman->AI())->FireOut(me);

                                m_uiEscalateTimer = 10 * IN_MILLISECONDS;
                            }

                            m_fScale = 0.4f;
                        }

                        me->SetObjectScale(m_fScale);
                        break;
                    }
                }
            }

            void DamageTaken(Unit*, uint32 &damage) { damage = 0; }

            void UpdateAI(uint32 uiDiff)
            {
                if (m_bSmoke)
                {
                    if (m_uiSmokeTimer < uiDiff)
                        me->DespawnOrUnsummon();
                    else
                        m_uiSmokeTimer -= uiDiff;

                    return;
                }

                if (!m_bEvent && m_bOnFire)
                {
                    m_fScale = 1.0f;
                    me->CastSpell(me, SPELL_HORSEMAN_FIRE, true);
                    me->SetObjectScale(m_fScale);
                }

                if (m_bOnFire)
                {
                    if (m_uiEscalateTimer < uiDiff)
                    {
                        m_fScale += 0.2f;

                        if (m_fScale > 2.5f)
                            m_fScale = 2.5f;

                        me->SetObjectScale(m_fScale);
                        m_uiEscalateTimer = urand(10,18) * IN_MILLISECONDS;
                    }
                    else
                        m_uiEscalateTimer -= uiDiff;
                }
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_horseman_fireAI(creature);
        }
};

/*####
## npc_hallows_end_matron
####*/

class npc_hallows_end_matron : public CreatureScript
{
    public:
        npc_hallows_end_matron() : CreatureScript("npc_hallows_end_matron") { }

        struct npc_hallows_end_matronAI : public ScriptedAI
        {
            npc_hallows_end_matronAI(Creature *c) : ScriptedAI(c) {Reset();}

            bool    m_bEventStarted;
            uint32  m_uiArea;
            uint8   m_uiLoc;

            void Reset()
            {
                m_bEventStarted = false;
                float x, y, z = 0;
                me->GetPosition(x, y, z);
                m_uiArea = me->GetMap()->GetAreaId(x, y, z);

                switch (m_uiArea)
                {
                    case 87:    m_uiLoc = 0;    break;    // Goldshire
                    case 131:   m_uiLoc = 1;    break;    // Kharanos
                    case 159:   m_uiLoc = 2;    break;    // Brill
                    case 362:   m_uiLoc = 3;    break;    // Razor Hill
                    case 3576:  m_uiLoc = 4;    break;    // Azure Watch
                    case 3665:  m_uiLoc = 5;    break;    // Falconwing Square
                }
            }

            void SpellHit(Unit *pCaster, const SpellInfo *pSpell)
            {
                if (pSpell->Id == SPELL_HORSEMAN_EVENT)
                    m_bEventStarted = false;
            }

            void StartEvent()
            {
                if (m_bEventStarted)
                    return;

                m_bEventStarted = true;

                if (Creature *pHorseman = me->SummonCreature(NPC_HORSEMAN_SHADE, HorsemanSpawnLoc[m_uiLoc][0], HorsemanSpawnLoc[m_uiLoc][1], HorsemanSpawnLoc[m_uiLoc][2], 0, TEMPSUMMON_CORPSE_DESPAWN, 1000))
                    CAST_AI(npc_shade_of_the_horseman::npc_shade_of_the_horsemanAI, pHorseman->AI())->EngageFires(m_uiLoc);
            }
        };

        bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest)
        {
            switch (quest->GetQuestId())
            {
                case QUEST_STOP_FIRES1:
                case QUEST_STOP_FIRES2:
                case QUEST_STOP_FIRES3:
                case QUEST_STOP_FIRES4:
                {
                    //if (npc_hallows_end_matronAI* pMatronAI = dynamic_cast<npc_hallows_end_matronAI*>(creature->AI()))
                    //    pMatronAI->StartEvent();

                    break;
                }
            }
            return true;
        }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_hallows_end_matronAI(creature);
        }
};

/*####
## npc_catrina
####*/

#define SPELL_HONOR_THE_DEAD    65386

class npc_catrina : public CreatureScript
{
    public:
        npc_catrina() : CreatureScript("npc_catrina") { }

        struct npc_catrinaAI : public ScriptedAI
        {
            npc_catrinaAI(Creature *c) : ScriptedAI(c) {Reset();}

            void Reset()
            {
            }

            void ReceiveEmote (Player *pPlayer, uint32 emote)
            {
                if (emote == EMOTE_STATE_DANCE)
                    if (!pPlayer->HasAura(SPELL_HONOR_THE_DEAD))
                        pPlayer->CastSpell(pPlayer, SPELL_HONOR_THE_DEAD, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_catrinaAI(creature);
        }
};

/*######
## npc_black_cat
######*/

enum
{
    BAD_LUCK        = 39477,
};

class npc_black_cat : public CreatureScript
{
    public:
        npc_black_cat() : CreatureScript("npc_black_cat") { }

        struct npc_black_catAI : public ScriptedAI
        {
            npc_black_catAI(Creature *pCreature) : ScriptedAI(pCreature) {Reset();}

            void Reset() {}

            void DamageTaken(Unit *done_by, uint32 &damage)
            {
                if (damage >= me->GetHealth())
                    if (done_by->GetTypeId() == TYPEID_PLAYER)
                        done_by->CastSpell(done_by, BAD_LUCK, true);
            }
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new npc_black_catAI(creature);
        }
};

void AddSC_hallows_end()
{
    new npc_hallows_end_matron();
    new npc_horseman_fire();
    new npc_shade_of_the_horseman();
    new npc_catrina();
    new npc_black_cat();
}