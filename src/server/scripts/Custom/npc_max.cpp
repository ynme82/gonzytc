
#include "ScriptPCH.h"

class npc_max : public CreatureScript
{
public:
    npc_max() : CreatureScript("npc_max") { }
	 //v
    bool OnGossipHello(Player *player, Creature *_creature)
    {
		if(_creature->isTrainer())
			player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, GOSSIP_TEXT_TRAIN, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_TRAIN);
		player->ADD_GOSSIP_ITEM(0, "Huzd fel a fegyver skilleimet maximumra!", GOSSIP_SENDER_MAIN, 1);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
        case 1:
			player->UpdateSkillsToMaxSkillsForLevel();
			_creature->MonsterWhisper("Sikeresen maximalizáltam a skilleidet!",player->GetGUID());
			player->CLOSE_GOSSIP_MENU();
            break;
 
        case GOSSIP_ACTION_TRAIN:
			player->GetSession()->SendTrainerList(_creature->GetGUID());
            break;
 
             default:
        break;                   
    }
   }
  return true;
 }
};

void AddSC_npc_max()
{
    new npc_max();
}


