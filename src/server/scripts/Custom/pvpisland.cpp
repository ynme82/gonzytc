#include "ScriptPCH.h"
#include "AccountMgr.h"
#include "Group.h"
#include "Chat.h"
#include "Config.h"

enum PvPIsland
{
    AREA_PVP_ISLAND             = 321,
    ZONE_PVP_ISLAND             = 33,
 
    STRING_HAVE_KILLED_OWN_IP   = 3000,
    STRING_BEEN_KILLED_OWN_IP   = 3001,
    STRING_IN_COMBAT            = 3002,
 
    AURA_SHIELD                 = 13874,
    AURA_SICKNESS               = 15007,
 
    SPELL_BERSERK               = 24378,
    SPELL_FOOD                  = 23493,
    SPELL_SPIRITUAL_IMMUNITY    = 36910,
    SPELL_SUMMON_PET            = 883,
    SPELL_SPRINT                = 56354,
 
    GAMEOBJECT_BERSERKING       = 0,
    GAMEOBJECT_FOOD             = 0,
    GAMEOBJECT_ANCIENT          = 0,
};

const int MAX_PLAYER_SPAWN_POINTS = 13;
static const Position playerSpawnPoint[MAX_PLAYER_SPAWN_POINTS] = 
{
    {-916.280029f, -3490.389893f, 70.450302f, 4.685141f},
    {-918.971191f, -3550.828125f, 72.964241f, 1.492506f},
    {-891.491272f, -3524.561768f, 71.368462f, 4.431466f},
    {-882.734680f, -3512.680420f, 72.047531f, 4.736986f},
    {-910.379883f, -3512.145508f, 70.667725f, 3.272218f},
    {-985.220398f, -3576.630371f, 56.749363f, 1.578905f},
    {-1004.982910f, -3578.000488f, 56.977753f, 3.167746f},
    {-1044.136719f, -3578.670654f, 68.019302f, 0.497383f},
    {-1050.050659f, -3521.238525f, 71.120293f, 0.099971f},
    {-1006.855042f, -3480.131104f, 62.743439f, 3.873813f},
    {-1029.426147f, -3494.258057f, 61.136417f, 1.179904f},
    {-975.866455f, -3467.023926f, 57.179123f, 5.287553f},
    {-961.339111f, -3473.330566f, 54.626034f, 5.723429f},
};

class pvp_island_teleport_event : public BasicEvent
{
    public:
        pvp_island_teleport_event(Player* victim) : _victim(victim) { }
 
        bool Execute(uint64 /*time*/, uint32 /*diff*/)
        {
            if (!_victim || !_victim->IsInWorld())
                return true;

            uint32 players = 0;

            SessionMap sessions = sWorld->GetAllSessions();
			for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
				if (Player* plr = itr->second->GetPlayer())
                    if (plr->GetAreaId() == 321)
					    ++players;

            ChatHandler(_victim->GetSession()).PSendSysMessage("Udvozollek a PvP zonaban!");
            ChatHandler(_victim->GetSession()).PSendSysMessage("Jelenlevo jatekosok: %u", players);
            ChatHandler(_victim->GetSession()).PSendSysMessage("A zonat a teleport parancsok segitsegevel tudod elhagyni!");

            int32 TelePos = urand(0, MAX_PLAYER_SPAWN_POINTS - 1);
            _victim->TeleportTo(0, playerSpawnPoint[TelePos].GetPositionX(), playerSpawnPoint[TelePos].GetPositionY(), playerSpawnPoint[TelePos].GetPositionZ(), playerSpawnPoint[TelePos].GetOrientation());
            _victim->ResetAllPowers();
            _victim->RemoveArenaSpellCooldowns();
            _victim->SetHealth(_victim->GetMaxHealth());
            if (_victim->getPowerType() == POWER_MANA)
                _victim->SetPower(POWER_MANA, _victim->GetMaxPower(POWER_MANA));
            return true;
        }
 
    private:
        Player* _victim;
};

class pvp_island_resurrect_event : public BasicEvent
{
    public:
        pvp_island_resurrect_event(Player* victim) : _victim(victim) { }
 
        bool Execute(uint64 /*time*/, uint32 /*diff*/)
        {
            if (!_victim || !_victim->IsInWorld())
                return true;

            _victim->ResurrectPlayer(1.0, false);
            int32 RespawnPos = urand(0, MAX_PLAYER_SPAWN_POINTS - 1);
            _victim->TeleportTo(0, playerSpawnPoint[RespawnPos].GetPositionX(), playerSpawnPoint[RespawnPos].GetPositionY(), playerSpawnPoint[RespawnPos].GetPositionZ(), playerSpawnPoint[RespawnPos].GetOrientation());
            _victim->ResetAllPowers();
            _victim->RemoveArenaSpellCooldowns();
            _victim->SetHealth(_victim->GetMaxHealth());
            if (_victim->getPowerType() == POWER_MANA)
                _victim->SetPower(POWER_MANA, _victim->GetMaxPower(POWER_MANA));
            return true;
        }
 
    private:
        Player* _victim;
};

class pvp_island_resurrect_event_pet : public BasicEvent
{
    public:
        pvp_island_resurrect_event_pet(Player* victim) : _victim(victim) { }
 
        bool Execute(uint64 /*time*/, uint32 /*diff*/)
        {
            if (!_victim || !_victim->IsInWorld())
                return true;

            _victim->CastSpell(_victim, SPELL_SUMMON_PET, false);
            return true;
        }
 
    private:
        Player* _victim;
};

class npc_pvp_join : public CreatureScript
{
    public:
        npc_pvp_join() : CreatureScript("npc_pvp_join") { }
 
        bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
        {
            player->PlayerTalkClass->ClearMenus();
            switch (action)
            {
                case GOSSIP_ACTION_INFO_DEF+1:
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Viszlat", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
                    player->SEND_GOSSIP_MENU(9000161, creature->GetGUID());
                    break;

                case GOSSIP_ACTION_INFO_DEF+2:
                {
                    if (player->isInCombat())
                    {
                        creature->MonsterWhisper("Combatban vagy!", player->GetGUID(), true);
                        player->CLOSE_GOSSIP_MENU();
                        return false;
                    }
                    player->SaveRecallPosition();
                    player->m_Events.AddEvent(new pvp_island_teleport_event(player), player->m_Events.CalculateTime(500));
                    player->SetByteFlag(UNIT_FIELD_BYTES_2, 1, UNIT_BYTE2_FLAG_PVP);
                    player->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
                    player->CLOSE_GOSSIP_MENU();
                } break;

                case GOSSIP_ACTION_INFO_DEF+3:
                {
                    if (player->isInCombat())
                    {
                        creature->MonsterWhisper("Combatban vagy!", player->GetGUID(), true);
                        player->CLOSE_GOSSIP_MENU();
                        return false;
                    }
                    Group* group = player->GetGroup();
                    if (!group)
                    {
                        creature->MonsterWhisper("Nem vagy groupban!", player->GetGUID(), true);
                        player->CLOSE_GOSSIP_MENU();
                        return false;
                    }
                    if (group->GetMembersCount() > 2)
                    {
                        creature->MonsterWhisper("Tul sokan vannak a groupodban! Maximum 2 fo!", player->GetGUID(), true);
                        player->CLOSE_GOSSIP_MENU();
                        return false;
                    }
                    for (GroupReference* itr = group->GetFirstMember(); itr != NULL; itr = itr->next())
                    {
                        if (Player* plr = itr->getSource())
                        {
                            plr->SaveRecallPosition();
                            plr->m_Events.AddEvent(new pvp_island_teleport_event(plr), plr->m_Events.CalculateTime(500));
                            plr->SetByteFlag(UNIT_FIELD_BYTES_2, 1, UNIT_BYTE2_FLAG_PVP);
                            plr->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_PVP_ATTACKABLE);
                        }
                    }
                    player->CLOSE_GOSSIP_MENU();
                } break;

                case GOSSIP_ACTION_INFO_DEF+4:
                    player->CLOSE_GOSSIP_MENU();
                    break;
            }
            return true;
        }

        bool OnGossipHello(Player* player, Creature* creature)
        {
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Mi az a PvP Island?", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Egyedul jelentkezek", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "Csapattal jelentkezek (max. 2 fo)", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Viszlat", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);

            player->SEND_GOSSIP_MENU(9000160, creature->GetGUID());
            return true;
        }
};

class pvp_island : public PlayerScript
{
    public:
        pvp_island() : PlayerScript("pvp_island") { }
 
        uint64 killerGUID;
        uint64 victimGUID;
        char msg[500];
 
        void OnUpdateZone(Player* player, uint32 newZone, uint32 newArea)
        {
            if (Group* group = player->GetGroup())
                if (newArea != AREA_PVP_ISLAND && player->GetAreaId() == AREA_PVP_ISLAND)
                    group->Disband(true);
        }

        void OnPVPKill(Player* killer, Player* victim)
        {
            if (killer->GetAreaId() == AREA_PVP_ISLAND)
            {
                if (victim->GetAreaId() == AREA_PVP_ISLAND)
                {
                    killerGUID = killer->GetGUID();
                    victimGUID = victim->GetGUID();
 
                    if (killerGUID == victimGUID)
                    {
                        victim->m_Events.AddEvent(new pvp_island_resurrect_event(victim), victim->m_Events.CalculateTime(200));
 
                        if (victim->getClass() == CLASS_HUNTER || victim->getClass() == CLASS_WARLOCK)
                            victim->m_Events.AddEvent(new pvp_island_resurrect_event_pet(victim), victim->m_Events.CalculateTime(3000));
                        return;
                    }

                    std::string killerIp = killer->GetSession()->GetRemoteAddress();
                    std::string victimIp = victim->GetSession()->GetRemoteAddress();

                    if (killerGUID != victimGUID)
                    {
                        if (strcmp(killerIp.c_str(), victimIp.c_str()) != 0)
                        {
                            uint8 token = sWorld->getIntConfig(CONFIG_PVP_TOKEN_COUNT);
                            if (ConfigMgr::GetIntDefault("PvPIsland.On", 0) == 1)
                            {
                                killer->AddItem(29434, token);
                                ChatHandler(killer->GetSession()).PSendSysMessage("You have been awarded a token for slaying another player.");
                            }
                        }
                    }

                    victim->m_Events.AddEvent(new pvp_island_resurrect_event(victim), victim->m_Events.CalculateTime(200));
 
                    if (victim->getClass() == CLASS_HUNTER || victim->getClass() == CLASS_WARLOCK)
                        victim->m_Events.AddEvent(new pvp_island_resurrect_event_pet(victim), victim->m_Events.CalculateTime(3000));
                   return;
                }
            }
        }
};

void AddSC_pvp_island()
{
    new npc_pvp_join();
    new pvp_island();
}
