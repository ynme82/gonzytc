/* 
SCRIPT DATA
NAME: VIP SYSTEM
CATEGORY: NPC/DATABASE
COMPLETE: 95%
TODO: IMPORT SETTINGS TO CONFIG FILE
AUTHOR: Bluer�k
RELEASE DATE: 2012.02.01
*/

// SETTINGS //
// ..._AR - Price of the thing/item
// for example: CUSTOMIZE_AR 40
// Char customize will cost 40 credits
// ***********************************
// VIP
// 0 - Disabled
// 1 - Enabled
// KREDIT_VIP
// 0 - Disabled
// 1 - Enabled
#define VIP 1
#define KREDIT_VIP 1
#define KREDIT_VIP_AR 1500
#define REPAIR_AR 0
#define WHISP_AR 100
#define CUSTOMIZE_AR 250
#define FACTION_AR 500
#define RACE_AR 550
#define RENAME_AR 550
#define SAVE_AR 0

// Credit Item IDs
#define ITEM_1_ENTRY 33079
#define ITEM_2_ENTRY 46802
#define ITEM_3_ENTRY 44984
#define ITEM_4_ENTRY 10394
#define ITEM_5_ENTRY 32768
#define ITEM_6_ENTRY 8494
#define ITEM_7_ENTRY 13584
#define ITEM_8_ENTRY 49284
#define ITEM_9_ENTRY 35504
#define ITEM_10_ENTRY 20371
// Credit Item Names
#define ITEM_1 "Murloc Costume"
#define ITEM_2 "Heavy Murloc Egg"
#define ITEM_3 "Ammen Vale Lashling"
#define ITEM_4 "Prairie Dog Whistle"
#define ITEM_5 "Reins of the Raven Lord"
#define ITEM_6 "Hyacinth Macaw"
#define ITEM_7 "Diablo Stone"
#define ITEM_8 "Reins of the Swift Spectral Tiger"
#define ITEM_9 "Phoenix Hatchling"
#define ITEM_10 "Blue Murloc Egg"
// Credit Item Prices
#define ITEM_1_PRICE 50
#define ITEM_2_PRICE 50
#define ITEM_3_PRICE 20
#define ITEM_4_PRICE 20
#define ITEM_5_PRICE 50
#define ITEM_6_PRICE 20
#define ITEM_7_PRICE 20
#define ITEM_8_PRICE 20
#define ITEM_9_PRICE 25
#define ITEM_10_PRICE 20


// GOSSIP MENUS //
// MAIN MENUS //
#define GOSSIP_ITEM_VIP "VIP Menu"
#define GOSSIP_ITEM_WANNA_VIP "I'm not a VIP member, but I want to be"
// ALT MENUS //
#define GOSSIP_ITEM "Repair"
#define GOSSIP_ITEM_1 "Whisp On/Off"
#define GOSSIP_ITEM_2 "Customize"
#define GOSSIP_ITEM_3 "Faction change"
#define GOSSIP_ITEM_4 "Race change"
#define GOSSIP_ITEM_5 "Rename"
#define GOSSIP_ITEM_7 "Character Save"
#define GOSSIP_ITEM_8 "Buy VIP for credit"
#define GOSSIP_ITEM_9 "Buy VIP via PayPal"
#define GOSSIP_ITEM_10 "The price of the VIP is 'XXX' credit, I'm ready to pay"
#define GOSSIP_ITEM_11 "I haven't got enough money, I will come back later"
#define GOSSIP_ITEM_12 "Buying items for credit"


class npc_vip : public CreatureScript
{
    public:
        npc_vip() : CreatureScript("npc_vip") { }
		uint32 Kredit;


	bool OnGossipSelect(Player *pPlayer, Creature *pCreature, uint32 /*sender*/, uint32 action )
	{
		pPlayer->PlayerTalkClass->ClearMenus();
		Kredit = LoginDatabase.PQuery("SELECT `kreditek` FROM `account` WHERE `account`='%s';", pPlayer->GetSession()->GetAccountId());

		switch(action)
		{
			case GOSSIP_ACTION_INFO_DEF:
			if (pPlayer->GetSession()->GetSecurity() >= SEC_MODERATOR)
			{
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+200);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+201);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+202);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_3, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+203);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_4, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+204);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_5, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+205);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_7, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+206);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_12, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+207);
				pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());
			}
			else
			{
				pCreature->MonsterWhisper("Sorry, You are not a VIP member, by the way, clicking on the 'I'm not a VIP member, but I want to be' menu will show You have to become a vip!", pPlayer->GetGUID(), false);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_WANNA_VIP, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
				pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());
			}
			break;
				

			case GOSSIP_ACTION_INFO_DEF+200:
				pPlayer->DurabilityRepairAll(false, 0, false);
				break;

			case GOSSIP_ACTION_INFO_DEF+201:
				if (Kredit >= WHISP_AR)
				{
					if(pPlayer->isAcceptWhispers())
					{
						pPlayer->SetAcceptWhispers(false);
						pCreature->MonsterWhisper("Whispers off!", pPlayer->GetGUID(), false);
						LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, WHISP_AR, pPlayer->GetSession()->GetAccountId());
					}

					else 
					{
						pPlayer->SetAcceptWhispers(true);
						pCreature->MonsterWhisper("Whispers on!", pPlayer->GetGUID(), false);
						LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, WHISP_AR, pPlayer->GetSession()->GetAccountId());
					}
				} else
					pCreature->MonsterWhisper("You haven't got enough credit, sorry!", pPlayer->GetGUID(), true);
				break;
			case GOSSIP_ACTION_INFO_DEF+202:
				pPlayer->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
				pCreature->MonsterWhisper("Customize Done!", pPlayer->GetGUID(), false);
				LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, CUSTOMIZE_AR, pPlayer->GetSession()->GetAccountId());
				break;

			case GOSSIP_ACTION_INFO_DEF+203:
				pPlayer->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
				pCreature->MonsterWhisper("Faction Change Done!", pPlayer->GetGUID(), false);
				LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, FACTION_AR, pPlayer->GetSession()->GetAccountId());
				break;

			case GOSSIP_ACTION_INFO_DEF+204:
				pPlayer->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
				pCreature->MonsterWhisper("Race Change Done!", pPlayer->GetGUID(), false);
				LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, RACE_AR, pPlayer->GetSession()->GetAccountId());
				break;

			case GOSSIP_ACTION_INFO_DEF+205:
				pPlayer->SetAtLoginFlag(AT_LOGIN_RENAME);
				pCreature->MonsterWhisper("Rename Done!", pPlayer->GetGUID(), false);
				LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, RENAME_AR, pPlayer->GetSession()->GetAccountId());
				break;

			case GOSSIP_ACTION_INFO_DEF+206:
				pPlayer->SaveToDB();
				pCreature->MonsterWhisper("Saved!", pPlayer->GetGUID(), false);
				LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, SAVE_AR, pPlayer->GetSession()->GetAccountId());
				break;
			
			case GOSSIP_ACTION_INFO_DEF+1:
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_8, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_9, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
				pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());
				break;

			case GOSSIP_ACTION_INFO_DEF+2:
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_10, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
				pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_11, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
				pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());
				break;

			case GOSSIP_ACTION_INFO_DEF+3:
				pCreature->MonsterWhisper("Visit our website to donate via PayPal!", pPlayer->GetGUID(), false);
				break;

			case GOSSIP_ACTION_INFO_DEF+4:
				if (KREDIT_VIP == 1)
				{
					if (Kredit >= KREDIT_VIP_AR)
					{
						pPlayer->GetSession()->SetSecurity(SEC_MODERATOR);
						pCreature->MonsterWhisper("Congratulations, now You are a VIP member! Relog for privileges!", pPlayer->GetGUID(), true);
						LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, KREDIT_VIP_AR, pPlayer->GetSession()->GetAccountId());
					} else
						pCreature->MonsterWhisper("Sorry, You haven't got enough credit!", pPlayer->GetGUID(), true);
				}
				else
				{
					pCreature->MonsterWhisper("Sorry, buying VIP for credit is not avaliable on this server.", pPlayer->GetGUID(), true);
				}
				break;

			case GOSSIP_ACTION_INFO_DEF+207:
				// Credit Part
				if (Kredit <= 0)
				{
					pCreature->MonsterWhisper("Sorry, You haven't got any credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+100);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+101);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_3, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+102);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_4, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+103);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_5, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+104);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_6, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+105);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_7, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+106);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_8, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+107);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_9, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+108);
					pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, ITEM_10, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+109);
					pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());
				}
				break;

				// KREDIT ITEMS //
			case GOSSIP_ACTION_INFO_DEF+100:
				if (Kredit < ITEM_1_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_1_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_1_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+101:
				if (Kredit < ITEM_2_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_2_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_2_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+102:
				if (Kredit < ITEM_3_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_3_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_3_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+103:
				if (Kredit < ITEM_4_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_4_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_4_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+104:
				if (Kredit < ITEM_5_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_5_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_5_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+105:
				if (Kredit < ITEM_6_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_6_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_6_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+106:
				if (Kredit < ITEM_7_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_7_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_7_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+107:
				if (Kredit < ITEM_8_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_8_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_8_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+108:
				if (Kredit < ITEM_9_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_9_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_9_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				case GOSSIP_ACTION_INFO_DEF+109:
				if (Kredit < ITEM_10_PRICE)
				{
					pPlayer->CLOSE_GOSSIP_MENU();
					pCreature->MonsterWhisper("Sorry, You haven't got enough credits!", pPlayer->GetGUID(), true);
				}
				else
				{
					LoginDatabase.PQuery("UPDATE `account` SET `kreditek`='%s-%d' WHERE `account`='%s';", Kredit, ITEM_10_PRICE, pPlayer->GetSession()->GetAccountId());
					pPlayer->AddItem(ITEM_10_ENTRY, 1);
					pCreature->MonsterWhisper("Congratulations, You get the item! Check your backpack!", pPlayer->GetGUID(), true);
				}
				break;

				
		}
		return true;
	}

	bool OnGossipHello(Player * pPlayer, Creature * pCreature)
	{
		if (VIP == 0)
			pCreature->MonsterWhisper("VIP System is not enabled!", pPlayer->GetGUID(), true);

		if (pPlayer->isAlive() && VIP == 1)
		{
			pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_VIP, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
			pPlayer->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_ITEM_WANNA_VIP, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
			pPlayer->SEND_GOSSIP_MENU(907, pCreature->GetGUID());

		}
			return true;
	}
};

void AddSC_npc_vip()
{
	new npc_vip();
}
