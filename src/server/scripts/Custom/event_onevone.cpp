#include "ScriptPCH.h"
#include <cstring>
#include "CharacterDatabaseCleaner.h"
#include "DatabaseEnv.h"
#include "ObjectMgr.h"
#include "BattlegroundMgr.h"
using namespace std;

class npc_arena_join : public CreatureScript
{
public:
    npc_arena_join() : CreatureScript("npc_arena_join") { }
	bool OnGossipHello(Player *player, Creature *creature)
    {
		player->ADD_GOSSIP_ITEM(4, "Jelentkezes", GOSSIP_SENDER_MAIN, 1);
		player->ADD_GOSSIP_ITEM(0, "Viszlat", GOSSIP_SENDER_MAIN, 2);
        player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());         
        return true;
    }
	void JoinArena(Player *player, Creature *_Creature)
	{
		Group * grp = player->GetGroup();
		if (grp)
		{
			player->GetSession()->SendNotification("Lepj ki a groupbol!");
			player->CLOSE_GOSSIP_MENU();
			return;
		}
		else
		{
			uint64 guid = _Creature->GetGUID();

            if (player->InBattleground())
				return;

			Creature* unit = player->GetMap()->GetCreature(guid);
			if (!unit)
				return;

			uint8 arenatype = ARENA_TYPE_5v5;
			uint32 arenaRating = 0;
			uint32 matchmakerRating = 0;

			Battleground* bg = sBattlegroundMgr->GetBattlegroundTemplate(BATTLEGROUND_AA);
			if (!bg)
			{
				sLog->outError(LOG_FILTER_NETWORKIO, "Battleground: template bg (all arenas) not found");
				return;
			}

			BattlegroundTypeId bgTypeId = bg->GetTypeID();
			BattlegroundQueueTypeId bgQueueTypeId = BattlegroundMgr::BGQueueTypeId(bgTypeId, arenatype);
			PvPDifficultyEntry const* bracketEntry = GetBattlegroundBracketByLevel(bg->GetMapId(), player->getLevel());
			if (!bracketEntry)
				return;

			uint32 ateamId = 0;
			BattlegroundQueue &bgQueue = sBattlegroundMgr->GetBattlegroundQueue(bgQueueTypeId);
    
			GroupQueueInfo* ginfo = bgQueue.AddGroup(player, NULL, bgTypeId, bracketEntry, arenatype, 0, false, arenaRating, matchmakerRating, ateamId);
			uint32 avgTime = bgQueue.GetAverageQueueWaitTime(ginfo, bracketEntry->GetBracketId());
			uint32 queueSlot = player->AddBattlegroundQueueId(bgQueueTypeId);

			WorldPacket data;
			sBattlegroundMgr->BuildBattlegroundStatusPacket(&data, bg, queueSlot, STATUS_WAIT_QUEUE, avgTime, 0, arenatype, 0);

			player->GetSession()->SendPacket(&data);
			sLog->outDebug(LOG_FILTER_BATTLEGROUND, "Battleground: player joined queue for arena, skirmish, bg queue type %u bg type %u: GUID %u, NAME %s", bgQueueTypeId, bgTypeId, player->GetGUIDLow(), player->GetName().c_str());
			sBattlegroundMgr->ScheduleQueueUpdate(matchmakerRating, arenatype, bgQueueTypeId, bgTypeId, bracketEntry->GetBracketId());

			std::string announcemsg = "";
            announcemsg += "|cffff0000[1v1 Arena]|r Jelentkezett: ";
            announcemsg += "|cffff0000";
            announcemsg += player->GetName();
            announcemsg += "|r";
            SessionMap sessions = sWorld->GetAllSessions();
            for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
				if (Player* plr = itr->second->GetPlayer())
					sWorld->SendServerMessage(SERVER_MSG_STRING, announcemsg.c_str(), plr);
			player->CLOSE_GOSSIP_MENU();
		}
	}
    bool OnGossipSelect(Player *player, Creature *creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
				case 1:
					{
						if(player->isInCombat())
						{
							player->CLOSE_GOSSIP_MENU();
							creature->MonsterWhisper("Nem joinolhatsz combatban!", player->GetGUID(), true);
							return false;
						}

						if(player->InBattlegroundQueue())
						{
							player->CLOSE_GOSSIP_MENU();
							creature->MonsterWhisper("Nem joinolhatsz mikozben BattleGround -ra vagy jelentkezve!", player->GetGUID(), true);
							return false;
						}

						if (player->HasTalent(65139,player->GetActiveSpec()) || player->HasTalent(53563,player->GetActiveSpec())
                            || player->HasTalent(47540,player->GetActiveSpec()) || player->HasTalent(47788,player->GetActiveSpec())
                            || player->HasTalent(61295,player->GetActiveSpec()))
						{
							player->CLOSE_GOSSIP_MENU();
							creature->MonsterWhisper("Nem joinolhatsz healerkent!", player->GetGUID(), true);
							return false;
						}
						JoinArena(player,creature);
					} break;
 
				case 2:
					{
						player->CLOSE_GOSSIP_MENU();
					} break;
				case 3:
					{
						player->CLOSE_GOSSIP_MENU();
					}break;
				 default:
					break;                   
			}
		}
		return true;
	}
};
class npc_arena_toplist1v1 : public CreatureScript
{
    public:
        npc_arena_toplist1v1() : CreatureScript("npc_arena_toplist1v1") { }
bool OnGossipHello(Player *player, Creature *_creature)
{
        QueryResult toplist = CharacterDatabase.PQuery("SELECT `name`,`event_wins` FROM characters ORDER BY `event_wins` DESC LIMIT 0,20");

        if (toplist)
        {
                Field* toplistfield = NULL;
                uint32 id = 1;
                do
                {
                        toplistfield = toplist->Fetch();
                        string charname = toplistfield[0].GetString();
                        uint32 wins = toplistfield[1].GetUInt32();
                        char msg[250];
                        snprintf(msg, 250, "%u: %s - Megnyert csatak: %u", id, charname.c_str(), wins);
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, msg, GOSSIP_SENDER_MAIN, id);
                        ++id;
                }while(toplist->NextRow());
        }
		//player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, "1v1 season nyitas: Januar 3. este", GOSSIP_SENDER_MAIN, 7001);
        player->ADD_GOSSIP_ITEM(0, "Viszlat", GOSSIP_SENDER_MAIN, 7002);
    player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
    return true;
}

bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 action )
{
   if (sender == GOSSIP_SENDER_MAIN)
       player->PlayerTalkClass->ClearMenus();

        if (action == 7001)
                {

                } else if (action == 7002)
                {
                        player->CLOSE_GOSSIP_MENU();
                } else {
                        // default
                }
                return true;
        }
};


void AddSC_npc_event_onevone()
{
    new npc_arena_join();
	new npc_arena_toplist1v1();
}
