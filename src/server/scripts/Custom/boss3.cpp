/*
* K�sz�tette: Geleta �kos (Zedin)
* Project: Ice World Fun Server 2012
* Boss P�lya Boss 3
* �llapot 100%
* T�pus: Creature Script
* Boss ID: 50003
* Trash ID: 50004
*/

#include "ScriptPCH.h"


enum Spells
{
		//Flame Breath
	SPELL_Flame_Breath          = 64021,
		// Searing_Flames 
	SPELL_Searing_Flames      	= 62661,
		// Lightning Nova
	SPELL_Lightning_Nova        = 65279,
		//Sunbeam
	SPELL_Sunbeam      			= 62872
};

enum Events
{
	EVENT_NONE,
	EVENT_Flame_Breath ,
	EVENT_Searing_Flames,
	EVENT_Lightning_Nova,
	EVENT_Sunbeam,
	EVENT_SUMMON_TEMPEST
};

const Position Spawn[4] =
{
		// koordin�t�tkat majd v�ltoztatni
    {8870.570323f, -3839.429932f, 129.787994f, 2.14f},
	{8848.02f, -3828.97f, 130.111f, 5.63f},
	{8861.0f, -3814.09f, 129.1f, 4.5f},
	{8842.041f, -3807.3776f, 129.2f, 5.4f}
	/*{-744.7544f, 3.8724f, 635.672f, 0.0f},
	{-737.616f, 21.5843f, 635.672f, 0.0f},
	{-719.885f, 29.0385f, 635.672f, 0.0f},
	{-702.2652f, 21.7784f, 635.672f, 0.0f}*/
};

enum Npcs
{
	// Summon
	NPC_trash1 = 50004,
};

enum Actions
{
	ACTION_NONE,
	ACTION_NEXT,
};

class boss3 : public CreatureScript
{
    public:
        boss3() : CreatureScript("boss3") { }

        struct boss3AI : public ScriptedAI
        {
            boss3AI(Creature* creature) : ScriptedAI(creature)
            {
            }
			
			int i;
			bool check_in;
			bool summons;

            void Reset()
            {
				
                _events.Reset();
                _events.ScheduleEvent(EVENT_Flame_Breath, urand(25000, 51000));
                _events.ScheduleEvent(EVENT_Searing_Flames, urand(11000, 26000));
                _events.ScheduleEvent(EVENT_Lightning_Nova, urand(60000, 12000));
				_events.ScheduleEvent(EVENT_Sunbeam, urand(4000, 8000));
				_events.ScheduleEvent(EVENT_SUMMON_TEMPEST, urand( 60000, 60000));
            }
			
			
			void JustSummoned(Creature* summoned)
			{
				ScriptedAI::JustSummoned(summoned);
			}

            void KilledUnit(Unit* victim)
            {

                if (urand(0, 4))
                    return;

            }

            void JustDied(Unit* /*killer*/)
            {
            }

            void EnterCombat(Unit* /*who*/)
            {
            }


            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_Searing_Flames:
                            DoCastVictim(SPELL_Searing_Flames);
                            _events.ScheduleEvent(EVENT_Searing_Flames, urand(9000, 20000));
                            break;
                        case EVENT_Lightning_Nova:
                            DoCastVictim(SPELL_Lightning_Nova);
                            _events.ScheduleEvent(EVENT_Lightning_Nova, urand(25000, 51000));
                            break;
						case EVENT_Flame_Breath:
                            DoCastVictim(SPELL_Flame_Breath);
                            _events.ScheduleEvent(EVENT_Flame_Breath, urand(14000, 29000));
                            break;
						case EVENT_Sunbeam:
                            DoCastVictim(SPELL_Sunbeam);
							_events.ScheduleEvent(EVENT_Sunbeam, urand(3000, 7000));
                            break;
						case EVENT_SUMMON_TEMPEST:
							for (uint8 i = 0; i < 4; i++)
							{
								if (Creature* pBoss3 = me->SummonCreature(NPC_trash1, Spawn[i].GetPositionX(), Spawn[i].GetPositionY(), Spawn[i].GetPositionZ(), 0, TEMPSUMMON_CORPSE_DESPAWN))
								{
									pBoss3->SetUInt64Value(UNIT_FIELD_SUMMONEDBY, me->GetGUID());
								}
							}
							_events.ScheduleEvent(EVENT_SUMMON_TEMPEST, urand( 60000, 60000));
							break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss3AI (creature);
        }
};

class trash1 : public CreatureScript
{
    public:
        trash1() : CreatureScript("trash1") { }

        struct trash1AI : public ScriptedAI
        {
            trash1AI(Creature* creature) : ScriptedAI(creature)
            {
            }
			
			int i;
			bool check_in;
			bool summons;

            void Reset()
            {
				
                _events.Reset();
                _events.ScheduleEvent(EVENT_Flame_Breath, urand(25000, 51000));
				_events.ScheduleEvent(EVENT_Sunbeam, urand(4000, 8000));
            }
			
			
			void JustSummoned(Creature* summoned)
			{
				ScriptedAI::JustSummoned(summoned);
			}

            void KilledUnit(Unit* victim)
            {

                if (urand(0, 4))
                    return;

            }

            void JustDied(Unit* /*killer*/)
            {
            }

            void EnterCombat(Unit* /*who*/)
            {
            }


            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
						case EVENT_Flame_Breath:
                            DoCastVictim(SPELL_Flame_Breath);
                            _events.ScheduleEvent(EVENT_Flame_Breath, urand(17000, 24000));
                            break;
						case EVENT_Sunbeam:
                            DoCastVictim(SPELL_Sunbeam);
							_events.ScheduleEvent(EVENT_Sunbeam, urand(5000, 10000));
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new trash1AI (creature);
        }
};

void AddSC_boss3()
{
    new boss3();
	new trash1();
}
