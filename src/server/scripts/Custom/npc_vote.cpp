#include "ScriptPCH.h"
#include <cstring>
#include "CharacterDatabaseCleaner.h"
#include "DatabaseEnv.h"
#include "ObjectMgr.h"
/*
class npc_vote : public CreatureScript
{
public:
    npc_vote() : CreatureScript("npc_vote") { }
	 //v
    bool OnGossipHello(Player *player, Creature *_creature)
    {
		QueryResult result = CharacterDatabase.PQuery("SELECT COUNT(*) as count FROM character_vote WHERE account_id='%u'", player->GetSession()->GetAccountId());
		if (result)
		{
			Field * f = NULL;
			f = result->Fetch();
			uint32 num = f[0].GetUInt32();
			if(num > 0)
				player->ADD_GOSSIP_ITEM(4, "Eredmenyek", GOSSIP_SENDER_MAIN, 3);
			else
				player->ADD_GOSSIP_ITEM(4, "Szavazni szeretnek", GOSSIP_SENDER_MAIN, 1);
		}

		player->ADD_GOSSIP_ITEM(0, "Kesobb visszanezek", GOSSIP_SENDER_MAIN, 2);
        player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());         
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
        case 1:
            player->ADD_GOSSIP_ITEM(4, "Cross-Faction Battleground", GOSSIP_SENDER_MAIN, 4); // 1
			player->ADD_GOSSIP_ITEM(4, "Spell javitasok", GOSSIP_SENDER_MAIN, 5); // 2
			player->ADD_GOSSIP_ITEM(4, "Tobb egyedi tartalom", GOSSIP_SENDER_MAIN, 6); // 3
			player->ADD_GOSSIP_ITEM(0, "Kesobb visszanezek", GOSSIP_SENDER_MAIN, 2);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());     
            break;
 
        case 2:
            player->CLOSE_GOSSIP_MENU();
            break;
 
        case 3:
			{
			Field * fields = NULL;
			Field * fields1 = NULL;
			Field * fields2 = NULL;
			QueryResult result31 = CharacterDatabase.Query("SELECT COUNT(*) as vote1 FROM character_vote WHERE vote = '1'");
			if (result31)
			{
				fields = result31->Fetch();
				uint32 vote1 = fields[0].GetUInt32();
				char msg[250];
				snprintf(msg, 250, "Cross-Faction Battleground (%u szavazat)", vote1);
				player->ADD_GOSSIP_ITEM(4, msg, GOSSIP_SENDER_MAIN, 99);
			}
			QueryResult result32 = CharacterDatabase.Query("SELECT COUNT(*) as vote2 FROM character_vote WHERE vote = '2'");
			if (result32)
			{
				fields1 = result32->Fetch();
				uint32 vote2 = fields1[0].GetUInt32();
				char msg[250];
				snprintf(msg, 250, "Spell javitasok (%u szavazat)", vote2);
				player->ADD_GOSSIP_ITEM(4, msg, GOSSIP_SENDER_MAIN, 99);
			}
			QueryResult result33 = CharacterDatabase.Query("SELECT COUNT(*) as vote3 FROM character_vote WHERE vote = '3'");
			if (result33)
			{
				fields2 = result33->Fetch();
				uint32 vote3 = fields2[0].GetUInt32();
				char msg[250];
				snprintf(msg, 250, "Tobb egyedi tartalom (%u szavazat)", vote3);
				player->ADD_GOSSIP_ITEM(4, msg, GOSSIP_SENDER_MAIN, 99);
			}
			player->ADD_GOSSIP_ITEM(0, "Kesobb visszanezek", GOSSIP_SENDER_MAIN, 2);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
			}
			break;
 
        case 4:
			{
			_creature->MonsterWhisper("K�sz�nj�k, hogy szavazt�l!",player->GetGUID());
			CharacterDatabase.PQuery("INSERT INTO character_vote (vote, account_id) VALUES ('1', '%u')", player->GetSession()->GetAccountId());
			player->ADD_GOSSIP_ITEM(4, "Koszonjuk a szavazatot!", GOSSIP_SENDER_MAIN, 2);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
			}
            break;
 
        case 5:
            {
			_creature->MonsterWhisper("K�sz�nj�k, hogy szavazt�l!",player->GetGUID());
			CharacterDatabase.PQuery("INSERT INTO character_vote (vote, account_id) VALUES ('2', '%u')", player->GetSession()->GetAccountId());
			player->ADD_GOSSIP_ITEM(4, "Koszonjuk a szavazatot!", GOSSIP_SENDER_MAIN, 2);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
			}
            break;                             
               
        case 6:
			{
			_creature->MonsterWhisper("K�sz�nj�k, hogy szavazt�l!",player->GetGUID());
			CharacterDatabase.PQuery("INSERT INTO character_vote (vote, account_id) VALUES ('3', '%u')", player->GetSession()->GetAccountId());
			player->ADD_GOSSIP_ITEM(4, "Koszonjuk a szavazatot!", GOSSIP_SENDER_MAIN, 2);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
			}
            break;
 
             default:
        break;                   
    }
   }
  return true;
 }
};

void AddSC_npc_vote()
{
    new npc_vote();
}*/
