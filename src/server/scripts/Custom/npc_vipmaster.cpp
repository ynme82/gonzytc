//This script has been written by $0undX , and fuck u if you remove my name ! :)

//CONFIG
bool bosswhispers = true;

class npc_vipmaster : public CreatureScript
{
public:
    npc_vipmaster() : CreatureScript("npc_vipmaster") { }

    bool OnGossipSelect(Player *pPlayer, Creature *pCreature, uint32 sender, uint32 action ) // ha a játekos rákattint valameik menüre
    {
        if(!pPlayer || !pCreature || !action)
            return false;

        Field *fields;
        uint32 ID;
        QueryResult result;

        switch(action)
        {
            case GOSSIP_ACTION_INFO_DEF+1:
                result = CharacterDatabase.PQuery("SELECT `ID`, `ItemID`, `Count` FROM `vip_items` WHERE `OwnerAccountID` = '%u' LIMIT 1", pPlayer->GetSession()->GetAccountId());
                
                if (result)
                {
                    fields = result->Fetch();
                    ID = fields[0].GetUInt16();
                    uint32 ItemID = fields[1].GetUInt32();
                    uint32 count = fields[2].GetUInt32();
                    ItemPosCountVec sDest;
                    uint32 noSpaceForCount = 0;

                    uint8 msg = pPlayer->CanStoreNewItem( NULL_BAG, NULL_SLOT, sDest,ItemID, count, &noSpaceForCount);
                    
                    if (msg != EQUIP_ERR_OK)
                        count -= noSpaceForCount;

                    if( count == 0 || sDest.empty()) 
                    {
                        pPlayer->SendEquipError(EQUIP_ERR_INVENTORY_FULL,0,0);
                        pPlayer->CLOSE_GOSSIP_MENU();
                        break;
                    }

                    Item* item = pPlayer->StoreNewItem(sDest, ItemID, true, Item::GenerateItemRandomPropertyId(ItemID));
                    pPlayer->SendNewItem(item,count,true,false);
                    CharacterDatabase.PExecute("DELETE FROM `vip_items` WHERE `ID` = '%u'", ID);
                    pCreature->MonsterWhisper("Itt a csomag!", pPlayer->GetGUID(), bosswhispers);
                }
                else
                    pCreature->MonsterWhisper("Nem erkezett csomagod!", pPlayer->GetGUID(), bosswhispers);

                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF+2:
                pPlayer->DurabilityRepairAll(false, 0, false);
                pCreature->MonsterWhisper("Felszereles javitva!", pPlayer->GetGUID(), bosswhispers);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF+3:
                pPlayer->SaveToDB();
                pCreature->MonsterWhisper("Mentes sikeres!", pPlayer->GetGUID(), bosswhispers);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF+4:
                pPlayer->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
                pCreature->MonsterWhisper("Kijelentkezes utan a karakter lista melletti ikonra kattintva valtoztathatod a karakteredet!", pPlayer->GetGUID(), bosswhispers);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF+5:
                for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
                {
                    Player::BoundInstancesMap &binds = pPlayer->GetBoundInstances(Difficulty(i));
                    for (Player::BoundInstancesMap::iterator itr = binds.begin(); itr != binds.end();)
                    {
                        if(itr->first != pPlayer->GetMapId())
                        {
                            pPlayer->UnbindInstance(itr, Difficulty(i));
                        }
                        else
                            ++itr;
                    }
                }
                pCreature->MonsterWhisper("Mentesek feloldva!", pPlayer->GetGUID(), bosswhispers);
                pPlayer->CLOSE_GOSSIP_MENU();
                break;
        }

        return true;
    }

    bool OnGossipHello(Player *pPlayer, Creature *pCreature)
    {
        pPlayer->ADD_GOSSIP_ITEM(0, "Kerem az itememet", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
        pPlayer->ADD_GOSSIP_ITEM(0, "Felszereles javitasa", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
        pPlayer->ADD_GOSSIP_ITEM(0, "Karakter mentese", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
        pPlayer->ADD_GOSSIP_ITEM(0, "Plasztikai sebeszet", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4);
        pPlayer->ADD_GOSSIP_ITEM(0, "Instance mentes torlese", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5);
        pPlayer->SEND_GOSSIP_MENU(1, pCreature->GetGUID());
        return true;
    }

};

void AddSC_npc_vipmaster()
{
    new npc_vipmaster();
}
