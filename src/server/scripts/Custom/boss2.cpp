/*
* K�sz�tette: Geleta �kos (Zedin)
* Project: Ice World Fun Server 2012
* Boss P�lya Boss 2
* �llapot 100%
* T�pus: Creature Script
* Boss ID: 50002
*/

#include "ScriptPCH.h"


enum Spells
{
		//Flame Breath
	SPELL_Flame_Breath          = 64021,
		// Searing_Flames 
	SPELL_Searing_Flames      	= 62661,
		// Lightning Nova
	SPELL_Lightning_Nova        = 65279,
		//Sunbeam
	SPELL_Sunbeam      			= 62872
};

enum Events
{
	EVENT_NONE,
	EVENT_Flame_Breath ,
	EVENT_Searing_Flames,
	EVENT_Lightning_Nova,
	EVENT_Sunbeam
};

class boss2 : public CreatureScript
{
    public:
        boss2() : CreatureScript("boss2") { }

        struct boss2AI : public ScriptedAI
        {
            boss2AI(Creature* creature) : ScriptedAI(creature)
            {
            }

            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_Flame_Breath, urand(25000, 51000));
                _events.ScheduleEvent(EVENT_Searing_Flames, urand(11000, 26000));
                _events.ScheduleEvent(EVENT_Lightning_Nova, urand(60000, 12000));
				_events.ScheduleEvent(EVENT_Sunbeam, urand(4000, 8000));
            }

            void KilledUnit(Unit* victim)
            {

                if (urand(0, 4))
                    return;

            }

            void JustDied(Unit* /*killer*/)
            {
            }

            void EnterCombat(Unit* /*who*/)
            {
            }


            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_Searing_Flames:
                            DoCastVictim(SPELL_Searing_Flames);
                            _events.ScheduleEvent(EVENT_Searing_Flames, urand(9000, 20000));
                            break;
                        case EVENT_Lightning_Nova:
                            DoCastVictim(SPELL_Lightning_Nova);
                            _events.ScheduleEvent(EVENT_Lightning_Nova, urand(25000, 51000));
                            break;
						case EVENT_Flame_Breath:
                            DoCastVictim(SPELL_Flame_Breath);
                            _events.ScheduleEvent(EVENT_Flame_Breath, urand(14000, 29000));
                            break;
						case EVENT_Sunbeam:
                            DoCastVictim(SPELL_Sunbeam);
                            _events.ScheduleEvent(EVENT_Sunbeam, urand(3000, 7000));
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss2AI (creature);
        }
};

void AddSC_boss2()
{
    new boss2();
}
