#include "Chat.h"
#include "ScriptPCH.h"
class gmloginann : PlayerScript
{
    public: 
        gmloginann() : PlayerScript("gmloginann") { }

        void OnPlayerLogin(Player* player)
        {
            uint8 level = player->GetSession()->GetSecurity();
            std::string msg = "";

			if (level > 1 && level < 6)
			{
				msg += "|cffff0000[GameMaster]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";    
			} else if (level > 5 && level < 8)
			{
				msg += "|cffff0000[Adminisztr�tor]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			} else if (level > 7 && level < 10)
			{
				msg += "|cffff0000[Fejleszt�]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			} else if (level == 10)
			{
				msg += "|cffff0000[F�adminisztr�tor]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			} else if (level == 11)
			{
				msg += "|cffff0000[Tulajdonos]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			} else if (level == 12)
			{
				msg += "|cffff0000[F�tulajdonos]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			} else if (level == 13)
			{
				msg += "|cffff0000[Pokoli Oper�tor]|r ";
				msg += player->GetName();
				msg += " |cff18be00 bel�pett a vil�gba!";
			}

			if(level < 2 || !msg.c_str())
				return;

			SessionMap sessions = sWorld->GetAllSessions();
			for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
				if (Player* plr = itr->second->GetPlayer())
					sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);  
        }
};

void AddSC_gmloginann()
{
    new gmloginann();
}