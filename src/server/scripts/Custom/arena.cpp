/*******************************************************************************************
*              _   _                                      __   _                           *
*              | \ | |   ___    _ __ ___    ___    ___    / _| | |_                        *
*              |  \| |  / _ \  | '_ ` _ \  / __|  / _ \  | |_  | __|                       *
*              | |\  | | (_) | | | | | | | \__ \ | (_) | |  _| | |_                        *
*              |_| \_|  \___/  |_| |_| |_| |___/  \___/  |_|    \__|                       *
*                               The policy of Nomsoftware states: Releasing our software   *
*                               or any other files are protected. You cannot re-release    * 
*                               anywhere unless you were given permission.                 *
*                           (C) Nomsoftware 2011-2012. All rights reserved.                *
********************************************************************************************/

#include "ScriptPCH.h"
using namespace std;

class npc_arena_teamTop : public CreatureScript
{
   public:
	   npc_arena_teamTop() : CreatureScript("npc_arena_teamTop") { }

	   bool OnGossipHello(Player * player, Creature * creature)
	   {
		   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Mutasd a Top 2v2 csapatokat", GOSSIP_SENDER_MAIN, 1);
		   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Mutasd a Top 3v3 csapatokat", GOSSIP_SENDER_MAIN, 2);
		   player->SEND_GOSSIP_MENU(1, creature->GetGUID());
		   return true;
	   }

	   bool OnGossipSelect(Player * player, Creature * creature, uint32 sender, uint32 actions)
	   {
		   if(sender == GOSSIP_SENDER_MAIN)
		   {
			   player->PlayerTalkClass->ClearMenus();
			   switch(actions)
			   {
			       case 1:
				   {
					   QueryResult result = CharacterDatabase.Query("SELECT name,rating FROM arena_team WHERE type='2' ORDER BY rating DESC LIMIT 10");
					   if(!result)
						   return false;

					   Field * fields = NULL;
					   //player->MonsterWhisper("|cff4169E1Here are the top 10 2v2 arena teams:|r", player->GetGUID());
					   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Top 10 2v2 csapatok:", GOSSIP_SENDER_MAIN, 10);
					   uint32 id = 100;
					   do
					   {
						   fields = result->Fetch();
						   string arena_name = fields[0].GetString();
						   uint32 rating = fields[1].GetUInt32();
						   char msg[250];
						   //snprintf(msg, 250, "Team Name: |cffFFFF00%s|r, Team Rating: %u \n", arena_name.c_str(), rating);
						   snprintf(msg, 250, "Csapatnev: |cffFFFF00%s|r, Rating: %u", arena_name.c_str(), rating);
						   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, msg, GOSSIP_SENDER_MAIN, id);
						   ++id;
					   }while(result->NextRow());
					   player->SEND_GOSSIP_MENU(2, creature->GetGUID());
				   }break;

			       case 2:
				   {
					   QueryResult result = CharacterDatabase.Query("SELECT name,rating FROM arena_team WHERE type='3' ORDER BY rating DESC LIMIT 10");
					   if(!result)
						   return false;

					   Field * fields = NULL;
					   //player->MonsterWhisper("|cff4169E1Here are the top 10 3v3 arena teams:|r", player->GetGUID());
					   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Top 10 3v3 csapatok:", GOSSIP_SENDER_MAIN, 20);
					   uint32 id = 200;					   
					   do
					   {
						   fields = result->Fetch();
						   string arena_name = fields[0].GetString();
						   uint32 rating = fields[1].GetUInt32();
						   char msg[250];
						   snprintf(msg, 250, "Csapatnev: |cffFFFF00%s|r, Rating: %u", arena_name.c_str(), rating);
						   //player->MonsterWhisper(msg, player->GetGUID());
						   player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, msg, GOSSIP_SENDER_MAIN, id);
						   ++id;
					   }while(result->NextRow());
					   player->SEND_GOSSIP_MENU(3, creature->GetGUID());
				   }break;

			   }
		   }

		   return true;
	   }
};

void AddSC_npc_arena_setup()
{
	new npc_arena_teamTop;
}
