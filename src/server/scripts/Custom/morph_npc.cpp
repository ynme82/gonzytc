#include "ScriptPCH.h"

class npc_morph : public CreatureScript
{
public:
    npc_morph() : CreatureScript("npc_morph") { }
	 //v
    bool OnGossipHello(Player *player, Creature *_creature)
    {
        if (player->isInCombat())
        {
            player->CLOSE_GOSSIP_MENU();
            _creature->MonsterWhisper("Jelenleg combat-ban vagy!", player->GetGUID());
            return true;
        }
        else
        {
			//player->ADD_GOSSIP_ITEM(4, "Illidan", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
			player->ADD_GOSSIP_ITEM(4, "Stormwind City Guard", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);//5446
			player->ADD_GOSSIP_ITEM(4, "Akama", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+3);
			player->ADD_GOSSIP_ITEM(4, "Greatfather Winter", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+4); //13369
			player->ADD_GOSSIP_ITEM(4, "Thrall", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+5); //4527
			player->ADD_GOSSIP_ITEM(4, "Undead", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+6);
			//player->ADD_GOSSIP_ITEM(4, "Mimiron", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+7);
			//player->ADD_GOSSIP_ITEM(4, "Lich King", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+8);
			player->ADD_GOSSIP_ITEM(4, "Tirion", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+9);
			player->ADD_GOSSIP_ITEM(4, "Uther Lightbringer", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+10);
			player->ADD_GOSSIP_ITEM(4, "Arthas", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+11);
			player->ADD_GOSSIP_ITEM(0, "Visszalakitas", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+12);
            
        }

        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        if (sender == GOSSIP_SENDER_MAIN)
        {
            player->PlayerTalkClass->ClearMenus();
            switch(uiAction)
            {
        /*case GOSSIP_ACTION_INFO_DEF+1:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(21135); 
            break;*/
 
        case GOSSIP_ACTION_INFO_DEF+2:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(5446);
            break;
 
        case GOSSIP_ACTION_INFO_DEF+3:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(20681);
            break;
 
        case GOSSIP_ACTION_INFO_DEF+4:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(13369);
            break;
 
        case GOSSIP_ACTION_INFO_DEF+5:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(4527);
            break;                             
               
                case GOSSIP_ACTION_INFO_DEF+6:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(2789);
            break;
 
              /*  case GOSSIP_ACTION_INFO_DEF+7:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(28578);
            break;*/
 
                case GOSSIP_ACTION_INFO_DEF+8:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(24191);
            break;
 
                case GOSSIP_ACTION_INFO_DEF+9:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(22209);
            break;
 
                case GOSSIP_ACTION_INFO_DEF+10:
            player->CLOSE_GOSSIP_MENU();
            player->SetDisplayId(23889);
            break;
 
                case GOSSIP_ACTION_INFO_DEF+11:
                        player->CLOSE_GOSSIP_MENU();
                        player->SetDisplayId(24949);
                        break;
 
                case GOSSIP_ACTION_INFO_DEF+12:
                        player->CLOSE_GOSSIP_MENU();
                        player->DeMorph();
                        break;
             default:
        break;                   
    }
   }
  return true;
 }
};

void AddSC_npc_morph()
{
    new npc_morph();
}
