#include "ScriptPCH.h"

class npc_atvalto : public CreatureScript
{
public:
    npc_atvalto() : CreatureScript("npc_atvalto") { }

    bool OnGossipHello(Player *player, Creature *_creature)
    {
        if (player->isInCombat())
        {
            player->CLOSE_GOSSIP_MENU();
            _creature->MonsterWhisper("Jelenleg combat-ban vagy!", player->GetGUID());
            return true;
        }
        else
        {
            uint32 accountid = player->GetSession()->GetAccountId();
            QueryResult acc_vp = CharacterDatabase.PQuery("SELECT `vp` FROM `pvp_wotlkweb`.`account_data` WHERE  `id`=%u",accountid);
            player->ADD_GOSSIP_ITEM( 1, "250  Token --> 250 Coin"                   , GOSSIP_SENDER_MAIN, 1);
            player->ADD_GOSSIP_ITEM( 6, "250  Coin --> 125 Token(-50%)"            , GOSSIP_SENDER_MAIN, 2);
            player->ADD_GOSSIP_ITEM( 4, "3000 Honor Points --> 5 Token"                     , GOSSIP_SENDER_MAIN, 3);
            player->ADD_GOSSIP_ITEM( 8, "100 Arena Points --> 100 Token"                     , GOSSIP_SENDER_MAIN, 4);
            player->ADD_GOSSIP_ITEM( 7, "5 Token --> 3000 Honor Points"                     , GOSSIP_SENDER_MAIN, 5);
            player->ADD_GOSSIP_ITEM( 5, "100 Token --> 100 Arena Points"                     , GOSSIP_SENDER_MAIN, 6);
            if (acc_vp)
            {
                char msg[250];
                Field* field_vp = acc_vp->Fetch();
                uint32 vps = field_vp[0].GetUInt32();
                snprintf(msg, 250, "%u Szavazo pont --> %u Token",vps,vps);
                if (vps > 0)
                    player->ADD_GOSSIP_ITEM( 9, msg , vps, 7);
            }

        }

        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
    {
        player->PlayerTalkClass->ClearMenus();
        if (uiAction == 7)
        {
            uint32 accountid = player->GetSession()->GetAccountId();
            CharacterDatabase.PQuery("UPDATE `pvp_wotlkweb`.`account_data` SET `vp`=0 WHERE `id`=%u",accountid);
            player->AddItem(29434,sender);
            _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
            OnGossipHello(player, _creature);
        }
        else if (sender == GOSSIP_SENDER_MAIN)
        {
            switch(uiAction)
            {
            case 1: //Token-Coin 
                if (player->HasItemCount( 29434, 250, false ))
                {
                    player->DestroyItemCount(29434, 250, true, false);
                    player->AddItem(24245, 250);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Tokened!", player->GetGUID(), true);
                    return false;
                }
                break;

            case 2: //Coin-Token
                if (player->HasItemCount( 24245, 250, false ))
                {
                    player->DestroyItemCount(24245, 250, true, false);
                    player->AddItem(29434, 125);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Coinod!", player->GetGUID(), true);
                    return false;
                }
                break;

            case 3: //Honor-Token
                if (player->GetHonorPoints() >= 3000)
                {
                    player->ModifyHonorPoints(-3000);
                    player->AddItem(29434, 5);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Honor Pontod!", player->GetGUID(), true);
                    return false;
                }
                break;

            case 4: //Arena-Token
                if (player->GetArenaPoints() >= 100)
                {
                    player->ModifyArenaPoints(-100);
                    player->AddItem(29434, 100);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Arena Pontod!", player->GetGUID(), true);
                    return false;
                }
                break;

            case 5: //Token-Honor
                if (player->HasItemCount( 29434, 5, false ))
                {
                    player->DestroyItemCount(29434, 5, true, false);
                    player->ModifyHonorPoints(+3000);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Tokened!", player->GetGUID(), true);
                    return false;
                }
                break;

            case 6: //Token-Arena
                if (player->HasItemCount( 29434, 100, false ))
                {
                    player->DestroyItemCount(29434, 100, true, false);
                    player->ModifyArenaPoints(+100);
                    _creature->MonsterWhisper("Sikeres volt a csere!", player->GetGUID(), true);
                }
                else
                {
                    player->CLOSE_GOSSIP_MENU();
                    _creature->MonsterWhisper("Sajnalom, de nincs eleg Tokened!", player->GetGUID(), true);
                    return false;
                }
                break;
            } 
        } 
        OnGossipHello(player, _creature);

        return true;
    } 
};

void AddSC_npc_atvalto()
{
    new npc_atvalto();
}
