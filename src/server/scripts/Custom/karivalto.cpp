#include "ScriptPCH.cpp"
using namespace std;

#define GOSSIP_SENDER_CHAR 2
#define GOSSIP_SENDER_QUESTION 3

class npc_karivalto : public CreatureScript
{
public:
    npc_karivalto() : CreatureScript("npc_karivalto") { }
    std::string GetItemName(Item* item, WorldSession* session)
    {
        std::string name = item->GetTemplate()->Name1;
        return name;
    }
    bool isInCharString (const char *str1, char *search)
    {
      for (int i = 0; i < strlen(str1); ++i)
      {
        if (strncmp (&str1[i], search, strlen(search)) == 0)
          return true;
      }
      return false;
    }

    bool OnGossipHello(Player* player, Creature* creature)
    {
        WorldSession* session = player->GetSession();
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Be szeretnem valtani a karakterem", GOSSIP_SENDER_CHAR, 2);
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Kesobb visszanezek", GOSSIP_SENDER_MAIN, 8);		
        player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());
        
        return true;
    }


    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 uiAction)
    {
        WorldSession* session = player->GetSession();
        player->PlayerTalkClass->ClearMenus();
        uint32 account_id = player->GetSession()->GetAccountId();
        uint32 player_guid = player->GetGUID();

        if(sender == GOSSIP_SENDER_CHAR)
        {
            char txt[250];
            char tokentxt[25];
            
            snprintf(txt, 250, "A kivalasztott karakter: %s (GUID: %u)", player->GetName().c_str(), player->GetGUID());
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, txt, GOSSIP_SENDER_MAIN, 0);

            uint32 token = 0;
            for (uint8 i = EQUIPMENT_SLOT_START; i < EQUIPMENT_SLOT_END; i++)
            {
                if (Bag* bag = player->GetBagByPos(i))
                {
                    for (uint32 j = 0; j < bag->GetBagSize(); j++)
                    {
                        if (Item* item = player->GetItemByPos(i, j))
                        {
                            const char* itemname = GetItemName(item,session).c_str();
                            if(isInCharString(itemname,"Wrathful"))
                                token = token + 100;
                            else if(isInCharString(itemname,"Shadowmourne"))
                                token = token + 100;
                        }
                    }
                }
            }
            snprintf(tokentxt, 250, "Karakter erteke: %u token", token);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, tokentxt, GOSSIP_SENDER_MAIN, 0);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, "Elfogadod?", GOSSIP_SENDER_MAIN, 0);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Igen", GOSSIP_SENDER_QUESTION, token);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Nem", GOSSIP_SENDER_MAIN, 8);
            player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());
        }
        else if (sender == GOSSIP_SENDER_QUESTION)
        {
            uint32 token = uiAction;
            LoginDatabase.PQuery("UPDATE `gonzy_auth`.`account` SET  `kreditek`=kreditek + %u WHERE id='%u'", token, player->GetSession()->GetAccountId());

            player->CLOSE_GOSSIP_MENU();
            player->GetSession()->KickPlayer();
            Player::DeleteFromDB(player_guid, account_id, true, true);
        }
        else
        {
            player->CLOSE_GOSSIP_MENU();
        }
        return true;
    }

};

void AddSC_npc_karivalto()
{
    new npc_karivalto();
}