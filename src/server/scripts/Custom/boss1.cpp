#include "ScriptPCH.h"
#include "Unit.h"
/*gobok:
sz�p nagy krist�ly: 181748
masik kristaly ugynaoylan csak kicsi : 181779


*/

/***********
** ERAGOS **
***********/

enum Spells
{
    SPELL_CLEAVE      	    = 15284,
    SPELL_FROSTBREATH       = 70116,
    SPELL_MORTALSTRIKE      = 39171,
    SPELL_SBVOLLEY			= 29924,
    SPELL_SPELLREFL			= 47981,
    SPELL_THUNDERCLAP		= 36706,
    SPELL_DOT1				= 65950,
};

enum Events
{
    EVENT_CLEAVE = 1,
    EVENT_FROSTBREATH,
    EVENT_MORTALSTRIKE,
    EVENT_SBVOLLEY,
    EVENT_SPELLREFL,
    EVENT_THUNDERCLAP,
    EVENT_FLYING
};

class boss_eragos : public CreatureScript
{
    public:
        boss_eragos() : CreatureScript("boss_eragos") { }

        struct boss_eragosAI : public ScriptedAI
        {
            boss_eragosAI(Creature* creature) : ScriptedAI(creature)
            {
            }
            uint32 phase;
            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_CLEAVE, urand(5000, 10000));
                _events.ScheduleEvent(EVENT_FROSTBREATH, urand(12000, 26000));
                _events.ScheduleEvent(EVENT_MORTALSTRIKE, urand(10000, 16000));
                _events.ScheduleEvent(EVENT_SPELLREFL, urand(10000, 26000));
                _events.ScheduleEvent(EVENT_SBVOLLEY, urand(12000, 17000));
                _events.ScheduleEvent(EVENT_THUNDERCLAP, urand(8000, 17000));
                _events.ScheduleEvent(EVENT_FLYING, 1000);
            }

            void KilledUnit(Unit* victim)
            {
            }

            void EnterCombat(Unit* victim)
            {
                /*me->AddUnitMovementFlag(MOVEMENTFLAG_CAN_FLY|MOVEMENTFLAG_ASCENDING|MOVEMENTFLAG_FLYING);
                me->SetSpeed(MOVE_FLIGHT, 0.75f, true);
                me->GetMotionMaster()->Clear(false);
                me->GetMotionMaster()->MovePoint(0,4455.571777f,-3181.624756f,1077.646240f);
                me->SetOrientation(4.689367f);
                SetCombatMovement(false);
                _events.ScheduleEvent(EVENT_FLYING, 1000);*/
            }

            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;
                /*
                if (me->GetHealthPct() < 65.0f && phase == 1)
                {
                    phase = 2;
                    _events.CancelEvent(EVENT_FLYING);
                    SetCombatMovement(true);
                    me->SetOrientation(4.559775f);
                    me->GetMotionMaster()->MovePoint(0,4457.946777f,-3178.925781f,1035.670532f);
                    me->RemoveUnitMovementFlag(MOVEMENTFLAG_CAN_FLY|MOVEMENTFLAG_ASCENDING|MOVEMENTFLAG_FLYING);
                    _events.ScheduleEvent(EVENT_CLEAVE, urand(5000, 10000));
                    _events.ScheduleEvent(EVENT_FROSTBREATH, urand(12000, 26000));
                    _events.ScheduleEvent(EVENT_MORTALSTRIKE, urand(10000, 16000));
                    _events.ScheduleEvent(EVENT_SPELLREFL, urand(10000, 26000));
                    _events.ScheduleEvent(EVENT_SBVOLLEY, urand(12000, 17000));
                    _events.ScheduleEvent(EVENT_THUNDERCLAP, urand(8000, 17000));
                }*/

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_CLEAVE:
                            DoCastVictim(SPELL_CLEAVE);
                            _events.ScheduleEvent(EVENT_CLEAVE, urand(8000, 16000));
                            break;
                        case EVENT_FROSTBREATH:
                            DoCastVictim(SPELL_FROSTBREATH);
                            _events.ScheduleEvent(EVENT_FROSTBREATH, urand(15000, 20000));
                            break;
                        case EVENT_MORTALSTRIKE:
                            DoCastVictim(SPELL_MORTALSTRIKE);
                            _events.ScheduleEvent(EVENT_MORTALSTRIKE, urand(20000, 30000));
                            break;
                        case EVENT_SPELLREFL:
                            DoCastVictim(SPELL_SPELLREFL);
                            _events.ScheduleEvent(EVENT_SPELLREFL, urand(10000, 17000)); 
                            break;
                        case EVENT_SBVOLLEY:
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                DoCast(target,SPELL_SBVOLLEY);
                            _events.ScheduleEvent(EVENT_SBVOLLEY, urand(8000, 15000)); 
                            break;
                        case EVENT_THUNDERCLAP:
                            DoCastVictim(SPELL_THUNDERCLAP);
                            _events.ScheduleEvent(EVENT_THUNDERCLAP, urand(9000, 13000)); 
                            break;
                        case EVENT_FLYING:
                            //for (uint8 i = 1; i < 6; ++i)
                            //	if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                            //		DoCast(target,SPELL_DOT1);
                            //_events.ScheduleEvent(EVENT_FLYING, 28000); 
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_eragosAI (creature);
        }
};


enum Spells2
{
    SPELL_STRANGULATE      	= 55334,
    SPELL_PENCOLD	        = 66013,
    SPELL_FRSLASH			= 66012,
    SPELL_GSWARM			= 64396,
    SPELL_BLADESTORM		= 65947,
    SPELL_CAMERA_SHAKE      = 36455,
};

enum Events2
{
    EVENT_STRANGULATE = 1,
    EVENT_PENCOLD,
    EVENT_FRSLASH,
    EVENT_GSWARM,
    EVENT_BLADESTORM,
    EVENT_ALLOWMOVE
};

class boss_guthock : public CreatureScript
{
    public:
        boss_guthock() : CreatureScript("boss_guthock") { }

        struct boss_guthockAI : public ScriptedAI
        {
            boss_guthockAI(Creature* creature) : ScriptedAI(creature)
            {
            }

            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_STRANGULATE, urand(5000, 10000));
                _events.ScheduleEvent(EVENT_PENCOLD, urand(12000, 17000));
                _events.ScheduleEvent(EVENT_FRSLASH, urand(5000, 7000));
                _events.ScheduleEvent(EVENT_GSWARM, urand(10000, 13000));
                _events.ScheduleEvent(EVENT_BLADESTORM, urand(20000, 28000));
            }

            void KilledUnit(Unit* victim)
            {
            }

            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_STRANGULATE:
                            for (uint8 i = 1; i < 7; ++i)
                                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                    DoCast(target,SPELL_STRANGULATE);
                            _events.ScheduleEvent(EVENT_STRANGULATE, urand(8000, 16000));
                            break;
                        case EVENT_PENCOLD:
                            for (uint8 i = 1; i < 7; ++i)
                                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                    DoCast(target,SPELL_PENCOLD);
                            _events.ScheduleEvent(EVENT_PENCOLD, urand(15000, 20000));
                            break;
                        case EVENT_FRSLASH:
                            DoCastVictim(SPELL_FRSLASH);
                            _events.ScheduleEvent(EVENT_FRSLASH, urand(20000, 30000));
                            break;
                        case EVENT_GSWARM:
                            for (uint8 i = 1; i < 5; ++i)
                                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                    DoCast(target,SPELL_GSWARM);
                            _events.ScheduleEvent(EVENT_GSWARM, urand(10000, 17000)); 
                            break;
                        case EVENT_BLADESTORM:
                            DoCastVictim(SPELL_BLADESTORM);
                            _events.ScheduleEvent(EVENT_BLADESTORM, urand(25000, 28000)); 
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_guthockAI (creature);
        }
};


enum Events3
{
    EVENT_PHASING = 1,
    EVENT_DOMINATE,
    EVENT_FSLASH,
//    EVENT_GSWARM,
//    EVENT_BLADESTORM,
};

enum Skullfraks
{
    NPC_HYJAL_GUARD     = 9099019,
    NPC_ZOLIBA          = 9099018,
    NPC_MIND_ILLUSION   = 9099020,

    SPELL_PHASE         = 50153,
    SPELL_CHAIN         = 56379,
    SPELL_ICE_LOCK      = 22856,
    SPELL_DOM_MIND      = 71289,
};

class boss_skullfraks : public CreatureScript
{
    public:
        boss_skullfraks() : CreatureScript("boss_skullfraks") { }

        struct boss_skullfraksAI : public ScriptedAI
        {
            boss_skullfraksAI(Creature* creature) : ScriptedAI(creature)
            {
                Reset();
            }

            Creature* guard1;
            Creature* guard2;
            Creature* zoliba;
            bool chained;
            UnitList phasedPlayers;

            void Reset()
            {
                for (UnitList::iterator itr = phasedPlayers.begin(); itr != phasedPlayers.end(); ++itr)
                {
                    if (!(*itr) || !(*itr)->IsInWorld())
                        continue;

                    Player* plr = (*itr)->ToPlayer();
                    if (plr && plr->IsInWorld())
                        plr->SetPhaseMask(1, false);
                }
                phasedPlayers.clear();

                chained = true;
                // X: 4291.221191 Y: -2697.842529 Z: 1104.489624 Orientation: 1.896681
                // X: 4265.299805 Y: -2694.769775 Z: 1106.413940 Orientation: 0.616482
                if (!guard1)
                    guard1 = me->SummonCreature(NPC_HYJAL_GUARD, 4291.221191f, -2697.842529f, 1104.489624f, 1.896681f);
                if (!guard2)
                    guard2 = me->SummonCreature(NPC_HYJAL_GUARD, 4265.299805f, -2694.769775f, 1106.413940f, 0.616482f);
                if (guard1 && guard2)
                {
                    //guard1->SetFacingTo(1.896681f); crashel tole
                    //guard2->SetFacingTo(0.616482f); crashel tole
                    guard1->CastSpell(me, SPELL_CHAIN);
                    guard2->CastSpell(me, SPELL_CHAIN);
                    guard1->SetUInt32Value(UNIT_NPC_EMOTESTATE, 469);
                    guard2->SetUInt32Value(UNIT_NPC_EMOTESTATE, 469);
                    //me->AddUnitState(UNIT_STATE_STUNNED);
                }
                zoliba = GetClosestCreatureWithEntry(me, NPC_ZOLIBA, 150.0f);
                if (zoliba)
                {
                    me->CastSpell(zoliba, SPELL_ICE_LOCK, false);
                    zoliba->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                }

                _events.Reset();
                _events.ScheduleEvent(EVENT_PHASING, urand(25000, 30000));
                _events.ScheduleEvent(EVENT_DOMINATE, urand(10000, 12000));
                _events.ScheduleEvent(EVENT_FSLASH, urand(5000, 7000));
                //_events.ScheduleEvent(EVENT_GSWARM, urand(10000, 13000));
                //_events.ScheduleEvent(EVENT_BLADESTORM, urand(20000, 28000));
            }

            void EnterCombat(Unit* /*victim*/)
            {
                chained = false;
                //me->ClearUnitState(UNIT_STATE_STUNNED);
                if (guard1 && guard2)
                {
                    guard1->DespawnOrUnsummon(1);
                    guard2->DespawnOrUnsummon(1);
                }
            }

            void EnterEvadeMode()
            {
                ScriptedAI::EnterEvadeMode();
            }

            void AttackStart(Unit* u)
            {
                if (chained)
                    return;
                ScriptedAI::AttackStart(u);
            }

            void KilledUnit(Unit* /*victim*/)
            {
            }

            void SummonedCreatureDies(Creature* c, Unit* /*u*/)
            {
                for (UnitList::iterator itr = phasedPlayers.begin(); itr != phasedPlayers.end(); ++itr)
                {
                    if (!(*itr) || !(*itr)->IsInWorld())
                        continue;

                    Player* plr = (*itr)->ToPlayer();
                    if (plr && plr->IsInWorld())
                        plr->SetPhaseMask(1, false);
                }
                phasedPlayers.clear();
                c->RemoveCorpse();
            }

            void JustDied(Unit* /*a*/)
            {
                if (zoliba)
                {
                    zoliba->RemoveAura(SPELL_ICE_LOCK);
                    zoliba->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                }
            }

            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_PHASING:
                            {
                                if (Creature* illusion = me->SummonCreature(NPC_MIND_ILLUSION, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 0.0f, TEMPSUMMON_TIMED_DESPAWN_OUT_OF_COMBAT, 6000))
                                {
                                    illusion->SetPhaseMask(2, false);
                                    illusion->SetReactState(REACT_AGGRESSIVE);
                                }

                                for (uint8 i = 1; i < 7; ++i)
                                    if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 60.0f, true))
                                    {
                                        if (target != me->getVictim()) // Tankot nem baszunk at masik phasebe
                                        {
                                            DoCast(target,SPELL_PHASE);
                                            target->SetPhaseMask(2, false);
                                            phasedPlayers.push_back(target);
                                        }
                                    }
                                _events.ScheduleEvent(EVENT_PHASING, urand(40000, 41000));
                            } break;
                        case EVENT_DOMINATE:
                            if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 60.0f, true))
                                DoCast(target,SPELL_DOM_MIND);
                            _events.ScheduleEvent(EVENT_DOMINATE, urand(35000, 48000));
                            break;
                        case EVENT_FSLASH:
                            DoCastVictim(SPELL_FRSLASH);
                            _events.ScheduleEvent(EVENT_FRSLASH, urand(8000, 12000));
                            break;
                        /*case EVENT_GSWARM:
                            for (uint8 i = 1; i < 5; ++i)
                                if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 30.0f, true))
                                    DoCast(target,SPELL_GSWARM);
                            _events.ScheduleEvent(EVENT_GSWARM, urand(10000, 17000)); 
                            break;
                        case EVENT_BLADESTORM:
                            DoCastVictim(SPELL_BLADESTORM);
                            _events.ScheduleEvent(EVENT_BLADESTORM, urand(25000, 28000)); 
                            break;*/
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss_skullfraksAI (creature);
        }
};

void AddSC_boss1()
{
    new boss_eragos();
    new boss_guthock();
    new boss_skullfraks();
}
