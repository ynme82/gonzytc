
#include "ScriptPCH.h"
#include "Chat.h"
 
#define FACTION_SPECIFIC 0
 
std::string GetNameLink(Player* player)
{
        std::string name = player->GetName();
        std::string color;
        switch(player->getClass())
        {
        case CLASS_DEATH_KNIGHT:
                color = "|cffC41F3B";
                break;
        case CLASS_DRUID:
                color = "|cffFF7D0A";
                break;
        case CLASS_HUNTER:
                color = "|cffABD473";
                break;
        case CLASS_MAGE:
                color = "|cff69CCF0";
                break;
        case CLASS_PALADIN:
                color = "|cffF58CBA";
                break;
        case CLASS_PRIEST:
                color = "|cffFFFFFF";
                break;
        case CLASS_ROGUE:
                color = "|cffFFF569";
                break;
        case CLASS_SHAMAN:
                color = "|cff0070DE";
                break;
        case CLASS_WARLOCK:
                color = "|cff9482C9";
                break;
        case CLASS_WARRIOR:
                color = "|cffC79C6E";
                break;
        }
        return "|Hplayer:"+name+"|h|cffFFFFFF["+color+name+"|cffFFFFFF]|h|r";
}
 
class vipcommands : public CommandScript
{
        public:
                vipcommands() : CommandScript("vipcommands"){}
 
        ChatCommand * GetCommands() const
        {
                static ChatCommand WorldChatCommandTable[] =
                {
                        {"chat",        SEC_PLAYER,             true,           &HandleWorldChatCommand,        "", NULL},
                        {NULL,          0,                              false,          NULL,                                           "", NULL}
                };
 
                return WorldChatCommandTable;
        }
 
        static bool HandleWorldChatCommand(ChatHandler * handler, const char * args)
        {
                if (!handler->GetSession()->GetPlayer()->CanSpeak())
                        return false;
                std::string temp = args;
 
                if (!args || temp.find_first_not_of(' ') == std::string::npos)
                        return false;
 
                std::string msg = "";
                Player * player = handler->GetSession()->GetPlayer();
                uint32 sec = player->GetSession()->GetSecurity();
                switch(sec)
                {
            case 1: // VIP
                msg += "|cffff0000[V.I.P|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;

            case 2: // VIP2
                msg += "|cffff0000[Deadly V.I.P|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;

            case 3: // Pr�baid�s GM
                msg += "|cffff0000[Pr�baid�s GM|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            /*case SEC_JuniorGM: // (�jonc) GM
                msg += "|cffff0000[GM|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;*/
                
            case 4: // GM
                msg += "|cffff0000[GM|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            case 5: // Vezet� GM
                msg += "|cffff0000[Vezet� GM|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            case 6: // Adminisztr�tor
                msg += "|cfffa9900[Adminisztr�tor|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            case 7: // Vezet� Adminisztr�tor
                msg += "|cfffa9900[Vezet� Adminisztr�tor|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                                
            case 8: // Fejleszt�
                msg += "|cfffa9900[Fejleszt�|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            case 9: // F�fejleszt�
                msg += "|cfffa9900[F�fejleszt�|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
                
            case 10: // F�adminisztr�tor
                msg += "|cfffa9900[F�adminisztr�tor|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
            
            case 11: // Tulajdonos
                msg += "|CFF7BBEF7[Tulajdonos|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |cffff0000";
                break;
            
            case 12: // F�tulajdonos
                 msg += "|CFF7BBEF7[F�tulajdonos|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |cffff0000";
                break;

            case 13: // Pokoli Oper�tor
                msg += "|cffff0000[Pokoli Oper�tor|r |cff18be00Chat:|r [";
                msg += player->GetName();
                msg += "]: |CFF7BBEF7";
                break;
    
                }
                       
                msg += args;
				player->MonsterWhisper(msg.c_str(), player->GetGUID());
                if (FACTION_SPECIFIC)
                {
                        SessionMap sessions = sWorld->GetAllSessions();
                        for (SessionMap::iterator itr = sessions.begin(); itr != sessions.end(); ++itr)
                                if (Player* plr = itr->second->GetPlayer())
                                        if (plr->GetTeam() == player->GetTeam())
                                                sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), plr);
                }
                else
                        sWorld->SendServerMessage(SERVER_MSG_STRING, msg.c_str(), 0);  
						
 
                return true;
        }
};
 
void AddSC_vipcommands()
{
        new vipcommands();
}
