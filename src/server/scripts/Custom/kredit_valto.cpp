// Script writed by STORMLine Hosting. (Muranyi Tamas)
// UPDATE `creature_template` SET ScriptName='kredit_valto' WHERE `entry`='550000';
#include "ScriptPCH.h"

class kredit_valto : public CreatureScript
{
public:
	kredit_valto() : CreatureScript("kredit_valto") { }

	bool OnGossipHello(Player *player, Creature *_creature)
	{
		MainMenu(player, _creature);
		return true;
	}
	
	void MainMenu(Player * const player, Creature * const creature)
    {
        player->ADD_GOSSIP_ITEM(0, "Token vasarlas",			GOSSIP_SENDER_MAIN, 2);
		player->ADD_GOSSIP_ITEM(0, "Kredit vasarlas",				GOSSIP_SENDER_MAIN, 3);
        player->ADD_GOSSIP_ITEM_EXTENDED(0, "Faction valtas (1000 kredit)", GOSSIP_SENDER_MAIN, 4, "Biztosan igenybe szeretned venni a Faction valtast 1000 kreditert?", 0, false);
        player->ADD_GOSSIP_ITEM_EXTENDED(0, "Faj valtas (1000 kredit)", GOSSIP_SENDER_MAIN, 5, "Biztosan igenybe szeretned venni a Faj valtast 1000 kreditert?", 0, false);
        player->ADD_GOSSIP_ITEM_EXTENDED(0, "Nev valtas (500 kredit)", GOSSIP_SENDER_MAIN, 7, "Biztosan igenybe szeretned venni a Nev valtast 500 kreditert?", 0, false);       
        //player->ADD_GOSSIP_ITEM(0, "Faction valtas (1000 kredit)",	GOSSIP_SENDER_MAIN, 4);
		//player->ADD_GOSSIP_ITEM(0, "Faj valtas (1000 kredit)",		GOSSIP_SENDER_MAIN, 5);
        player->ADD_GOSSIP_ITEM(0, "Instance feloldas",		        GOSSIP_SENDER_MAIN, 6);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, creature->GetGUID());
    }
	
	void GonzyTokenVasarlas(Player *player, Creature *_creature)
    {
        player->ADD_GOSSIP_ITEM(0, "100 Token (100 kredit)",			GOSSIP_SENDER_MAIN, 1000);
		player->ADD_GOSSIP_ITEM(0, "200 Token (200 kredit)",			GOSSIP_SENDER_MAIN, 1001);
		player->ADD_GOSSIP_ITEM(0, "400 Token (400 kredit)",			GOSSIP_SENDER_MAIN, 1002);
		player->ADD_GOSSIP_ITEM(0, "800 Token (800 kredit)",			GOSSIP_SENDER_MAIN, 1003);
		player->ADD_GOSSIP_ITEM(0, "1000 Token (1000 kredit)",		GOSSIP_SENDER_MAIN, 1004);
		player->ADD_GOSSIP_ITEM(0, "2500 Token (2500 kredit)",		GOSSIP_SENDER_MAIN, 1005);
		player->ADD_GOSSIP_ITEM(0, "5000 Token (5000 kredit)",		GOSSIP_SENDER_MAIN, 1006);
		player->ADD_GOSSIP_ITEM(0, "<= [Main Menu]", GOSSIP_SENDER_MAIN, 1);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
    }
	
	void GonzyTokenToKredit(Player *player, Creature *_creature)
    {
        player->ADD_GOSSIP_ITEM(0, "100 Kredit (100 Token)",			GOSSIP_SENDER_MAIN, 2000);
		player->ADD_GOSSIP_ITEM(0, "200 Kredit (200 Token)",			GOSSIP_SENDER_MAIN, 2001);
		player->ADD_GOSSIP_ITEM(0, "400 Kredit (400 Token)",			GOSSIP_SENDER_MAIN, 2002);
		player->ADD_GOSSIP_ITEM(0, "800 Kredit (800 Token)",			GOSSIP_SENDER_MAIN, 2003);
		player->ADD_GOSSIP_ITEM(0, "1000 Kredit (1000 Token)",		GOSSIP_SENDER_MAIN, 2004);
		player->ADD_GOSSIP_ITEM(0, "2500 Kredit (2500 Token)",		GOSSIP_SENDER_MAIN, 2005);
		player->ADD_GOSSIP_ITEM(0, "5000 Kredit (5000 Token)",		GOSSIP_SENDER_MAIN, 2006);
		player->ADD_GOSSIP_ITEM(0, "<= [Main Menu]", GOSSIP_SENDER_MAIN, 1);
		player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, _creature->GetGUID());         
    }

	bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 uiAction)
	{
		if (sender == GOSSIP_SENDER_MAIN)
        {
			player->PlayerTalkClass->ClearMenus();
			player->CLOSE_GOSSIP_MENU();
			
			switch(uiAction)
			{
				case 1:
					// Go to MainMenu
					MainMenu(player, _creature);
					break;
				case 2:
					// Go to GonzyTokenVasarlas Menu
					GonzyTokenVasarlas(player, _creature);
					break;
				case 3:
					// Go to GonzyTokenToKredit Menu
					GonzyTokenToKredit(player, _creature);
					break;
				case 4:
					// FactionValtas
					FactionValtas(player, _creature, 1000);
					break;
				case 5:
					// RaceValtas
					RaceValtas(player, _creature, 1000);
					break;
                case 6:
                    // Instance unlock
                    InstanceUnlock(player, _creature);
                    break;
                case 7:
                    // Nev valtas
                    NevValtas(player, _creature, 500);
                    break;
				case 1000:
					kredit_to_item(player, _creature, 100, 29434, 100); // V�g�n: kredit(100), itemid(37829), itemcount(100)
					break;
				case 1001:
					kredit_to_item(player, _creature, 200, 29434, 200);
					break;
				case 1002:
					kredit_to_item(player, _creature, 400, 29434, 400);
					break;
				case 1003:
					kredit_to_item(player, _creature, 800, 29434, 800);
					break;
				case 1004:
					kredit_to_item(player, _creature, 1000, 29434, 1000);
					break;
				case 1005:
					kredit_to_item(player, _creature, 2500, 29434, 2500);
					break;
				case 1006:
					kredit_to_item(player, _creature, 5000, 29434, 5000);
					break;
				case 2000:
					item_to_kredit(player, _creature, 100, 29434, 100); // V�g�n: kredit(200), itemid(37829), itemcount(200)
					break;
				case 2001:
					item_to_kredit(player, _creature, 200, 29434, 200);
					break;
				case 2002:
					item_to_kredit(player, _creature, 400, 29434, 400);
					break;
				case 2003:
					item_to_kredit(player, _creature, 800, 29434, 800);
					break;
				case 2004:
					item_to_kredit(player, _creature, 1000, 29434, 1000);
					break;
				case 2005:
					item_to_kredit(player, _creature, 2500, 29434, 2500);
					break;
				case 2006:
					item_to_kredit(player, _creature, 5000, 29434, 5000);
					break;
				default:
					MainMenu(player, _creature);
					break;
			}
		}
	return true;
	}

	int lekerdezes(int accountid)
	{
		int wkredit = -1;
		QueryResult result = LoginDatabase.PQuery("SELECT kreditek FROM account WHERE id='%u';", accountid);
		if (result) 
			wkredit = result->Fetch()->GetUInt32();
	return wkredit;
	}
	
	void kredit_to_item(Player *player, Creature *_creature, int kredit, int itemid, int itemcount)
	{
		int lekerdezett = lekerdezes(player->GetSession()->GetAccountId());
		if(lekerdezett < kredit) {
			_creature->MonsterWhisper("Sajnalom, de nincs eleg kredited!", player->GetGUID(), true);
		} else {
			player->AddItem(itemid, itemcount);
			// Kredit Lev�tele
			LoginDatabase.PQuery("UPDATE account SET kreditek=( kreditek - %u ) WHERE id='%u';", kredit, player->GetSession()->GetAccountId());
			_creature->MonsterWhisper("Sikeres volt a bevaltas.", player->GetGUID(), true);
		}
	}
	
	void item_to_kredit(Player *player, Creature *_creature, int kredit, int itemid, int itemcount)
	{
		if (player->HasItemCount( itemid, itemcount, false )) {
			player->DestroyItemCount(itemid, itemcount, true, false); 
			LoginDatabase.PQuery("UPDATE `account` SET `kreditek` = ( kreditek + %u ) WHERE id='%u'", kredit, player->GetSession()->GetAccountId());
            _creature->MonsterWhisper("Sikeres volt a bevaltas.", player->GetGUID(), true);
		} else {
			_creature->MonsterWhisper("Sajnalom, de nincs eleg tokened!", player->GetGUID(), true);
		}
	}
	
	void FactionValtas(Player *player, Creature *_creature, int kredit)
    {
		int lekerdezett = lekerdezes(player->GetSession()->GetAccountId());
		if(lekerdezett < kredit) {
			_creature->MonsterWhisper("Sajnalom, de nincs eleg kredited!", player->GetGUID(), true);
		} else {
			// Faction Valtas enged�lyez�se
			player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
			CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '64' WHERE guid = %u", player->GetGUIDLow());
			_creature->MonsterWhisper("Relog utan tudsz factiont valtani.", player->GetGUID(), true);
			// Kredit Lev�tele
			LoginDatabase.PQuery("UPDATE account SET kreditek=( kreditek - %u ) WHERE id='%u';", kredit, player->GetSession()->GetAccountId());
			//_creature->MonsterWhisper("Sikeres volt a bevaltas", player->GetGUID());
		}
    }
	
	void RaceValtas(Player *player, Creature *_creature, int kredit)
    {
		int lekerdezett = lekerdezes(player->GetSession()->GetAccountId());
		if(lekerdezett < kredit) {
			_creature->MonsterWhisper("Sajnalom, de nincs eleg kredited!", player->GetGUID(), true);
		} else {
			// Race V�lt�s enged�lyez�se
			player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
			CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '128' WHERE guid = %u", player->GetGUIDLow());
			_creature->MonsterWhisper("Relog utan tudsz kasztot valtani.", player->GetGUID(), true);
			// Kredit Lev�tele
			LoginDatabase.PQuery("UPDATE account SET kreditek=( kreditek - %u ) WHERE id='%u';", kredit, player->GetSession()->GetAccountId());
			//_creature->MonsterWhisper("Sikeres volt a bevaltas", player->GetGUID(), true);
		}
    }

    void NevValtas(Player *player, Creature *_creature, int kredit)
    {
		int lekerdezett = lekerdezes(player->GetSession()->GetAccountId());
		if(lekerdezett < kredit) {
			_creature->MonsterWhisper("Sajnalom, de nincs eleg kredited!", player->GetGUID(), true);
		} else {
			// N�v V�lt�s enged�lyez�se
            player->SetAtLoginFlag(AT_LOGIN_RENAME);
			CharacterDatabase.PExecute("UPDATE characters SET at_login = at_login | '1' WHERE guid = %u", player->GetGUIDLow());
			_creature->MonsterWhisper("Relog utan tudsz nevet valtani.", player->GetGUID(), true);
			// Kredit Lev�tele
			LoginDatabase.PQuery("UPDATE account SET kreditek=( kreditek - %u ) WHERE id='%u';", kredit, player->GetSession()->GetAccountId());
			//_creature->MonsterWhisper("Sikeres volt a bevaltas", player->GetGUID(), true);
		}
    }

    void InstanceUnlock(Player* player, Creature* creature)
    {
        for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
        {
            Player::BoundInstancesMap &binds = player->GetBoundInstances(Difficulty(i));
            for (Player::BoundInstancesMap::iterator itr = binds.begin(); itr != binds.end();)
            {
                if(itr->first != player->GetMapId())
                {
                    player->UnbindInstance(itr, Difficulty(i));
                }
                else
                    ++itr;
            }
        }
        creature->MonsterWhisper("Mentesek feloldva!", player->GetGUID(), true);
    }
	
};

void AddSC_kredit_valto()
{
    new kredit_valto();
}
