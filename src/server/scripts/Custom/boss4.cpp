/*
* K�sz�tette: Geleta �kos (Zedin)
* Project: Ice World Fun Server 2012
* Boss P�lya Boss 4
* �llapot 0%
* T�pus: Creature Script
* Boss ID: 50005
* Trash ID: 50006
*/

#include "ScriptPCH.h"


enum Spells
{
		//Impale
	SPELL_Impale          = 56090,
		// Slime Spray 
	SPELL_Slime_Spray      	= 54364,
		// Flame Breath
	SPELL_Flame_Breath        = 68970,
		//Wing Buffet
	SPELL_Wing_Buffet      			= 69293,
	SPELL_Flame_Breath2          = 64021,
	SPELL_Sunbeam      			= 62872
};

enum Events
{
	EVENT_NONE,
	EVENT_Impale,
	EVENT_Slime_Spray,
	EVENT_Flame_Breath,
	EVENT_Flame_Breath2,
	EVENT_Wing_Buffet,
	EVENT_Sunbeam,
	EVENT_SUMMON_TEMPEST
};

const Position Spawn[8] =
{
		// koordin�t�tkat majd v�ltoztatni
    {8870.570323f, -3839.429932f, 129.787994f, 2.14f},
	{8848.02f, -3828.97f, 130.111f, 5.63f},
	{8861.0f, -3814.09f, 129.1f, 4.5f},
	{8842.041f, -3807.3776f, 129.2f, 5.4f},
	{-744.7544f, 3.8724f, 635.672f, 0.0f},
	{-737.616f, 21.5843f, 635.672f, 0.0f},
	{-719.885f, 29.0385f, 635.672f, 0.0f},
	{-702.2652f, 21.7784f, 635.672f, 0.0f}
};

enum Npcs
{
	// Summon
	NPC_trash1 = 50006,
};

class boss4 : public CreatureScript
{
    public:
        boss4() : CreatureScript("boss4") { }

        struct boss4AI : public ScriptedAI
        {
            boss4AI(Creature* creature) : ScriptedAI(creature)
            {
            }

            void Reset()
            {
                _events.Reset();
                _events.ScheduleEvent(EVENT_Impale, urand(18000, 36000));
                _events.ScheduleEvent(EVENT_Slime_Spray, urand(9000, 18000));
                _events.ScheduleEvent(EVENT_Flame_Breath, urand(21000, 42000));
				_events.ScheduleEvent(EVENT_Wing_Buffet, urand(28000, 56000));
				_events.ScheduleEvent(EVENT_SUMMON_TEMPEST, urand( 50000, 50000));
            }

            void KilledUnit(Unit* victim)
            {

                if (urand(0, 4))
                    return;

            }

            void JustDied(Unit* /*killer*/)
            {
            }

            void EnterCombat(Unit* /*who*/)
            {
            }


            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_Impale:
                            DoCastVictim(SPELL_Impale);
                            _events.ScheduleEvent(EVENT_Impale, urand(18000, 36000));
                            break;
                        case EVENT_Slime_Spray:
                            DoCastVictim(SPELL_Slime_Spray);
                            _events.ScheduleEvent(EVENT_Slime_Spray, urand(9000, 18000));
                            break;
						case EVENT_Flame_Breath:
                            DoCastVictim(SPELL_Flame_Breath);
                            _events.ScheduleEvent(EVENT_Flame_Breath, urand(21000, 42000));
                            break;
						case EVENT_Wing_Buffet:
                            DoCastVictim(SPELL_Wing_Buffet);
                            _events.ScheduleEvent(EVENT_Wing_Buffet, urand(28000, 56000));
                            break;
						case EVENT_SUMMON_TEMPEST:
							for (uint8 i = 0; i < 8; i++)
							{
								if (Creature* pBoss3 = me->SummonCreature(NPC_trash1, Spawn[i].GetPositionX(), Spawn[i].GetPositionY(), Spawn[i].GetPositionZ(), 0, TEMPSUMMON_CORPSE_DESPAWN))
								{
									pBoss3->SetUInt64Value(UNIT_FIELD_SUMMONEDBY, me->GetGUID());
								}
							}
							_events.ScheduleEvent(EVENT_SUMMON_TEMPEST, urand( 50000, 50000));
							break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new boss4AI (creature);
        }
};

class trash2 : public CreatureScript
{
    public:
        trash2() : CreatureScript("trash2") { }

        struct trash2AI : public ScriptedAI
        {
            trash2AI(Creature* creature) : ScriptedAI(creature)
            {
            }
			
			int i;
			bool check_in;
			bool summons;

            void Reset()
            {
				
                _events.Reset();
                _events.ScheduleEvent(EVENT_Flame_Breath2, urand(15000, 30000));
				_events.ScheduleEvent(EVENT_Sunbeam, urand(4000, 8000));
            }
			
			
			void JustSummoned(Creature* summoned)
			{
				ScriptedAI::JustSummoned(summoned);
			}

            void KilledUnit(Unit* victim)
            {

                if (urand(0, 4))
                    return;

            }

            void JustDied(Unit* /*killer*/)
            {
            }

            void EnterCombat(Unit* /*who*/)
            {
            }


            void UpdateAI(const uint32 diff)
            {
                if (!UpdateVictim())
                    return;

                _events.Update(diff);

                if (me->HasUnitState(UNIT_STATE_CASTING))
                    return;

                while (uint32 eventId = _events.ExecuteEvent())
                {
                    switch (eventId)
                    {
						case EVENT_Flame_Breath2:
                            DoCastVictim(SPELL_Flame_Breath2);
                            _events.ScheduleEvent(EVENT_Flame_Breath2, urand(15000, 30000));
                            break;
						case EVENT_Sunbeam:
                            DoCastVictim(SPELL_Sunbeam);
							_events.ScheduleEvent(EVENT_Sunbeam, urand(5000, 10000));
                            break;
                        default:
                            break;
                    }
                }
                DoMeleeAttackIfReady();
            }

            private:
                EventMap _events;
        };

        CreatureAI* GetAI(Creature* creature) const
        {
            return new trash2AI (creature);
        }
};

void AddSC_boss4()
{
    new boss4();
	new trash2();
}
