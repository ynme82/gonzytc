#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "DatabaseEnv.h"
#include <cstring>
#include <stdio.h>
#include <time.h>
#include "Pet.h"
#include "Channel.h"

using namespace std;

class Reset_OnDuelEnd : public PlayerScript
{
    public:
        Reset_OnDuelEnd() : PlayerScript("Reset_OnDuelEnd") {}

    void OnDuelEnd(Player *winner, Player *looser, DuelCompleteType type)
    {
            winner->RemoveArenaSpellCooldowns(true);
            looser->RemoveArenaSpellCooldowns(true);
            winner->SetHealth(winner->GetMaxHealth());
            looser->SetHealth(looser->GetMaxHealth());
			winner->RemoveAurasDueToSpell(11196);
			looser->RemoveAurasDueToSpell(11196);
            if (winner->getPowerType() == POWER_MANA) 
                winner->SetPower(POWER_MANA, winner->GetMaxPower(POWER_MANA));
            if (looser->getPowerType() == POWER_MANA) 
                looser->SetPower(POWER_MANA, looser->GetMaxPower(POWER_MANA));
    }
};

class System_Censure : public PlayerScript
{
    public:
        System_Censure() : PlayerScript("System_Censure") {}

        void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg)
        {
            CheckMessage(player, msg, lang, NULL, NULL, NULL, NULL);
        }

        void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Player* receiver)
        {
            CheckMessage(player, msg, lang, receiver, NULL, NULL, NULL);
        }

        void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Group* group)
        {
            CheckMessage(player, msg, lang, NULL, group, NULL, NULL);
        }

        void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Guild* guild)
        {
            CheckMessage(player, msg, lang, NULL, NULL, guild, NULL);
        }

        void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Channel* channel)
        {
            CheckMessage(player, msg, lang, NULL, NULL, NULL, channel);
        }

        void CheckMessage(Player* player, std::string& msg, uint32 lang, Player* /*receiver*/, Group* /*group*/, Guild* /*guild*/, Channel* channel)
        {
            /*if (player->isGameMaster() || lang == LANG_ADDON)
                return;

            std::string lower = msg;
            std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);

            const uint8 cheksSize = 10;
            std::string checks[cheksSize];

            checks[0] ="http://";
            checks[1] =".com";
            checks[2] =".www";
            checks[3] =".net";
            checks[4] =".org";
            checks[5] =".hu";
            checks[6] ="www.";
            checks[7] ="jfa";
            checks[8] ="glob�l";
            checks[9] ="globalwow";
            for (int i = 0; i < cheksSize; ++i)
                 if (lower.find(checks[i]) != std::string::npos)
                 {
                     TC_LOG_ERROR(LOG_FILTER_UNITS, "Anti-Hirdetes: Info: %s Message: %s", player->GetSession()->GetPlayerInfo().c_str(), msg.c_str());
                     msg = "";
                     ChatHandler(player->GetSession()).PSendSysMessage("A hirdetes nem engedelyezett!");
                     return;
                 }*/
        }
};

#define OFFSET_THEME 10000
int length = 0;

class npc_gurubashi_theme : public CreatureScript
{
private:
    typedef std::list<Creature *> CRL;
    typedef std::list<GameObject *> GOL;
    CRL creaturelist;
    GOL gameobjectlist;

public:
    npc_gurubashi_theme() : CreatureScript("npc_gurubashi_theme") { }

    bool OnGossipHello(Player *player, Creature *_creature)
    {
		if (player->isGameMaster())
		{
			char msg[100];
			QueryResult result;
			result = WorldDatabase.PQuery("SELECT `id`, `name` FROM `gurubashi_themes`");
			if (result)
			{
				do
				{
					Field *fields = result->Fetch();
					player->ADD_GOSSIP_ITEM(4, fields[1].GetString(), GOSSIP_SENDER_MAIN, OFFSET_THEME + fields[0].GetInt32());
				} while (result->NextRow());
				player->ADD_GOSSIP_ITEM(4, "Gurubashi Arena", GOSSIP_SENDER_MAIN, 999);
			}
		} else {
			player->ADD_GOSSIP_ITEM(4, "Nem vagy GameMaster!", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
		}
        player->ADD_GOSSIP_ITEM(0, "Viszlat", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
        player->SEND_GOSSIP_MENU(1,_creature->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 action)
    {
        if (action > OFFSET_THEME)
        {
			QueryResult result;
			result = WorldDatabase.PQuery("SELECT `x`, `y`, `z`, `o`, `entry` FROM `gurubashi_spawns` WHERE `theme` = %u", action - OFFSET_THEME);
			GOL::iterator l;
			for (l = gameobjectlist.begin(); l != gameobjectlist.end(); ++l)
			{
				GameObject * go = *l;
				if (go)
					go->Delete();
			}
			gameobjectlist.clear();
			if (result)
			{
				do
				{
					Field *fields = result->Fetch();                        
					GameObject * gob = _creature->SummonGameObject(fields[4].GetInt32(), fields[0].GetFloat(), fields[1].GetFloat(), fields[2].GetFloat(), fields[3].GetFloat(), 0, 0, 0, 0, 2592000); 
					gameobjectlist.push_back(gob);
				} while (result->NextRow());
			}
			player->CLOSE_GOSSIP_MENU();

        }
        else
        {
            switch (action)
            {
            case GOSSIP_ACTION_INFO_DEF + 1:
                player->CLOSE_GOSSIP_MENU();
                break;
            case GOSSIP_ACTION_INFO_DEF + 2:
                OnGossipHello(player, _creature);
                break;
			case 999:
				GOL::iterator l;
                for (l = gameobjectlist.begin(); l != gameobjectlist.end(); ++l)
                {
                    GameObject * go = *l;
                    if (go)
						go->Delete();
                }
                gameobjectlist.clear();
				player->CLOSE_GOSSIP_MENU();
				break;
            }
        }
        return true;
    }
};

class npc_vp_bevalto : public CreatureScript
{
    public:
        npc_vp_bevalto() : CreatureScript("npc_vp_bevalto") { }
		uint32 accountid;
		//uint32 vps;
		bool OnGossipHello(Player *player, Creature *_creature)
		{
			accountid = player->GetSession()->GetAccountId();
			QueryResult acc_vp = CharacterDatabase.PQuery("SELECT `vp` FROM `pvp_wotlkweb`.`account_data` WHERE  `id`=%u",accountid);

			if (acc_vp)
			{
				Field* field_vp = acc_vp->Fetch();
				uint32 vps = field_vp[0].GetUInt32();
				if (vps > 0)
				{
					char msg[250];
					snprintf(msg, 250, "Szavazo pontok: %u",vps);
					player->ADD_GOSSIP_ITEM(7, msg, GOSSIP_SENDER_MAIN, 1001);
					player->ADD_GOSSIP_ITEM(4, "Be szeretnem valtani a pontokat", vps, 1003);
				}
				else
					player->ADD_GOSSIP_ITEM(7, "Szavazo pontok: 0", GOSSIP_SENDER_MAIN,1001);
			}
			player->ADD_GOSSIP_ITEM(0, "Viszlat", GOSSIP_SENDER_MAIN, 1002);
			player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
			return true;
		}

		bool OnGossipSelect(Player *player, Creature *_creature, uint32 sender, uint32 action )
		{
			player->PlayerTalkClass->ClearMenus();

			switch (action)
			{
				case 1002:
					player->CLOSE_GOSSIP_MENU();
					break;
				case 1003:
					player->ADD_GOSSIP_ITEM(0, "Mire szeretned bevaltani?", GOSSIP_SENDER_MAIN, 1001);
					char msgtoken[250];
					snprintf(msgtoken, 250, "GonzyWoW Token (%u darab)",sender);
					player->ADD_GOSSIP_ITEM(4, msgtoken, sender, 1004);
					player->SEND_GOSSIP_MENU(player->GetGossipTextId(_creature), _creature->GetGUID());
					break;
				case 1004:
					CharacterDatabase.PQuery("UPDATE `pvp_wotlkweb`.`account_data` SET `vp`=0 WHERE `id`=%u",accountid);
					player->AddItem(29434,sender);
					_creature->MonsterWhisper("Jovairtam a tokent!",player->GetGUID(),true);
					player->CLOSE_GOSSIP_MENU();
					break;
				default:
					break;
			}
			return true;
		}
};


void AddSC_custom_scripts()
{
	new Reset_OnDuelEnd;
    new npc_gurubashi_theme();
	new npc_vp_bevalto();
    new System_Censure();
}

